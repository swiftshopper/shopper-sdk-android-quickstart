package com.retailer.anyapp;

import android.text.TextUtils;

/**
 * Created by joelr on 12/16/2015.
 */
public class BasePresenter {

    /**
     * Returns true if @param val is null or empty. ""
     *
     * @param val the value to evaluate
     * @return a boolean struct of true if value is not empty.
     */
    protected boolean IsNotEmpty(String val) {
        return !TextUtils.isEmpty(val);
    }
}
