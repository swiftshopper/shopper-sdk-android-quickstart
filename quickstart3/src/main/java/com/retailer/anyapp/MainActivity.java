package com.retailer.anyapp;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.retailer.anyapp.movies.MoviesFragment;

public class MainActivity extends BaseActivity {

    private FragmentManager mFragmentManager;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragmentManager = getSupportFragmentManager();
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .add(R.id.main_container, MoviesFragment.newInstance(), MoviesFragment.TAG)
                    .commit();
        }

        progressBar.setVisibility(View.GONE);
    }
}

