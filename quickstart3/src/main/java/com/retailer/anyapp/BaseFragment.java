package com.retailer.anyapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by joelr on 11/22/2015.
 */
public class BaseFragment extends Fragment {

    /**
     * Returns the Support action bar.
     *
     * @return the current system Support Action bar.
     */
    protected ActionBar getSupportActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    /**
     * Returns true if @param versionCode equals to or is greater
     * than the current build version code.
     *
     * @param versionCode the version code used for evaluation.
     * @return
     */
    protected boolean isVersionOrNewer(int versionCode) {
        return Build.VERSION.SDK_INT >= versionCode;
    }

    /**
     * Resets Action bar color
     */
    protected void resetActionbarColor(int color) {
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(color));
    }

    /**
     * Returns true if we are in a tablet or phablet size device.
     *
     * @return a boolean based on device size.
     */
    protected boolean weAreInATabletSizeDevice() {
        return (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Sets the current action bar to its default state.
     */
    protected void resetActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);

            if (isVersionOrNewer(Build.VERSION_CODES.LOLLIPOP)) {
                setActionbarAndStatusBarColors(getResources().getColor(R.color.colorPrimary),
                        getResources().getColor(R.color.colorPrimaryDark));
            } else {
                resetActionbarColor(getResources().getColor(R.color.colorPrimary));
            }
        }
    }

    /**
     * Sets the action bar color base on the give @param actionBarColor
     * Sets the status bar color base on the given @param statusBarColor.
     *
     * @param actionBarColor The color resource value to use as the action bar color.
     * @param statusBarColor The color resource value to use as the status bar color
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void setActionbarAndStatusBarColors(int actionBarColor, int statusBarColor) {
        try {
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(actionBarColor));
            Window window = getActivity().getWindow();
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            // finally change the color
            window.setStatusBarColor(statusBarColor);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Displays a Snackbar with the desired message on the view specified.
     *
     * @param parent
     * @param msg
     */
    protected void showSnackbar(View parent, String msg) {
        Snackbar.make(parent, msg, Snackbar.LENGTH_SHORT).show();
    }

    /**
     * Detects where or not there is a network available.
     *
     * @return networkAvailability
     */
    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
