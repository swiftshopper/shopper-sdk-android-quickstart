package com.retailer.anyapp.movies;

import android.support.v7.widget.RecyclerView;

/**
 * Created by joelr on 11/29/2015.
 */
public interface MovieOnScrollChangedListener {

    void onScrollChanged(RecyclerView recyclerView, int dx, int dy);
}
