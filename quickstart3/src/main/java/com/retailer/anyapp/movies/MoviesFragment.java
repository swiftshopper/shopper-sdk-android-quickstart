package com.retailer.anyapp.movies;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.retailer.anyapp.BaseFragment;
import com.retailer.anyapp.R;
import com.retailer.anyapp.details.MovieDetailsFragment;
import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.MovieFilterList;
import com.retailer.anyapp.domain.api.ApiClient;
import com.retailer.anyapp.swiftshopper.LoginActivity;
import com.retailer.anyapp.swiftshopper.ProfileActivity;
import com.retailer.anyapp.swiftshopper.SelectMarketActivity;
import com.retailer.anyapp.util.FavoriteMoviesProvider;
import com.retailer.anyapp.util.MarginItemDecorator;
import com.retailer.anyapp.util.MovieSpanSizeLookup;
import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;

import java.util.ArrayList;
import java.util.List;


public class MoviesFragment extends BaseFragment implements MovieItemClickListener,
        MoviesContract.View, MovieOnScrollChangedListener {

    public static final String TAG = "MoviesFragment";

    private MoviesAdapter mListAdapter;
    private MoviesContract.UserActionsListener mActionsListener;

    public MoviesFragment() {
        // Required empty public constructor
    }


    public static MoviesFragment newInstance() {
        return new MoviesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mListAdapter = new MoviesAdapter(getContext(), this);
        mActionsListener = new MoviesPresenter(ApiClient.getApiClient(), this);

        if (savedInstanceState == null) {
            mActionsListener.loadMovies(MovieFilterList.NOW_PLAYING);
        } else {
            mActionsListener.setLastKnownResultPage(savedInstanceState.getInt(MoviesPresenter.TAG, 1));
            mActionsListener.setLastKnownFilter((MovieFilterList) savedInstanceState.getSerializable(MoviesPresenter.TAG_MOVIE_FILTER));
            ArrayList<Movie> movies = savedInstanceState.getParcelableArrayList(Movie.PARCELABLE_KEY);
            mListAdapter.addAll(movies);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_movies, container, false);

        MovieRecyclerView recyclerView = (MovieRecyclerView) root.findViewById(R.id.movies_recycler_view);
        recyclerView.setAdapter(mListAdapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.addOnScrollChangedListener(this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MarginItemDecorator(getContext()));

        int numColumns = getContext().getResources().getInteger(R.integer.movies_span);
        int numColumnsIfIndexEven = getContext().getResources().getInteger(R.integer.movies_span_when_even);
        int numColumnsIfIndexOdd = getContext().getResources().getInteger(R.integer.movies_span_when_odd);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), numColumns);
        layoutManager.setSpanSizeLookup(new MovieSpanSizeLookup(numColumns, numColumnsIfIndexOdd, numColumnsIfIndexEven));
        recyclerView.setLayoutManager(layoutManager);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(MoviesPresenter.TAG, mActionsListener.getResultPageNumber());
        outState.putSerializable(MoviesPresenter.TAG_MOVIE_FILTER, mActionsListener.getCurrentMovieFilter());
        outState.putParcelableArrayList(Movie.PARCELABLE_KEY, (ArrayList<? extends Parcelable>) mListAdapter.getAll());
    }

    @Override
    public void onResume() {
        resetActionBar();
        if (mActionsListener != null) {
            mActionsListener.setProgressIndicator(false);
            if (mActionsListener.getCurrentMovieFilter().equals(MovieFilterList.FAVORITES)) {
                loadFavoriteMovies(); //reload, this can improve.
            }
        }
        getActivity().invalidateOptionsMenu();
        super.onResume();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_movies, menu);
        //menu.findItem(R.id.action_shop).getIcon().setAlpha(0xd0);


        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_filter) {
            AlertDialog dialog = new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.movies_filter))
                    .setSingleChoiceItems(R.array.sort_values, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0:
                                    mActionsListener.loadMovies(MovieFilterList.FAVORITES);
                                    break;
                                case 1:
                                    mActionsListener.loadMovies(MovieFilterList.HIGHEST_RATED);
                                    break;
                                case 2:
                                    mActionsListener.loadMovies(MovieFilterList.MOST_POPULAR);
                            }
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
        } else if (id == R.id.action_login) {
            SsUser ssUser = SwiftShopperSdk.getUser();
            if (ssUser == null || ssUser.isAnonymous()) {
                startActivity(new Intent(getContext(), LoginActivity.class));
            } else {
                startActivity(new Intent(getContext(), ProfileActivity.class));
            }
        } else if (id == R.id.action_shop) {
            startActivity(new Intent(getContext(), SelectMarketActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    private <T extends View> T findViewById(int id) {
        View retVal = null;
        View contentView = getView();
        if (contentView != null) {
            retVal = contentView.findViewById(id);
        }
        return (T)retVal;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem loginItem = menu.findItem(R.id.action_login);
        SsUser ssUser = SwiftShopperSdk.getUser();
        String ssUserPhotoUrl = (ssUser == null) ? null : ssUser.getUserPhotoUrl();
        ssUserPhotoUrl = (ssUserPhotoUrl == null) ? null : ssUserPhotoUrl.trim();
        if (ssUserPhotoUrl != null && ssUserPhotoUrl.length() > 0) {
            Glide.with(this).load(ssUserPhotoUrl).asBitmap().fitCenter()
                    .atMost().into(new SimpleTarget<Bitmap>(96, 96) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    loginItem.setIcon(new BitmapDrawable(getResources(), resource));
                }
            });
        }
    }

    @Override
    public void onMovieClick(View v, int position) {
        mActionsListener.openMovieDetails(mListAdapter.get(position));
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if (getView() == null) {
            return;
        }

        ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.progress_bar);
        if (active) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void showMovieDetailUi(Bundle movieArgs) {
        //TODO: Should I use an app router and onAttach so I decouple features and else?
        if (weAreInATabletSizeDevice()) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.detail_container, MovieDetailsFragment.newInstance(movieArgs), MovieDetailsFragment.TAG)
                    .commit();
        } else {
            getFragmentManager().beginTransaction()
                    .replace(R.id.main_container, MovieDetailsFragment.newInstance(movieArgs), MovieDetailsFragment.TAG)
                    .addToBackStack(MovieDetailsFragment.TAG)
                    .commit();
        }
    }

    @Override
    public void setMovies(List<Movie> movies) {
        mListAdapter.addAll(movies);
    }

    @Override
    public void clearMovies() {
        mListAdapter.clear();
    }

    @Override
    public void showMessage(String msg, int duration) {
        if (getView() == null) {
            return;
        }

        Snackbar.make(getView(), msg, duration).show();
    }

    @Override
    public void loadFavoriteMovies() {
        // Retrieve movie records
        mListAdapter.clear();
        setProgressIndicator(true);
        List<Movie> favs = new ArrayList<>();
        FavoriteMoviesProvider provider = new FavoriteMoviesProvider(getContext());

        Cursor c = provider.query(Uri.parse(FavoriteMoviesProvider.CONTENT_URI.toString()), null, null, null, "title");
        if (c != null) {
            if (c.moveToFirst()) {
                do {
                    favs.add(new Movie(c.getInt(c.getColumnIndex(FavoriteMoviesProvider.ID)), getValueFor(c, FavoriteMoviesProvider.TITLE),
                            getValueFor(c, FavoriteMoviesProvider.OVERVIEW),
                            c.getLong(c.getColumnIndex(FavoriteMoviesProvider.RELEASE_DATE)),
                            getValueFor(c, FavoriteMoviesProvider.AVERAGE_RATING),
                            getValueFor(c, FavoriteMoviesProvider.POSTER),
                            getValueFor(c, FavoriteMoviesProvider.BACKDROP)));
                } while (c.moveToNext());
            }
            c.close();
        }

        if (favs.size() > 0) {
            mListAdapter.addAll(favs);
            mActionsListener.setProgressIndicator(false);
        } else {
            setProgressIndicator(false);
            showMessage("You do not have Favorite movies yet", 0);
        }
    }

    private String getValueFor(Cursor c, String column) {
        return c.getString(c.getColumnIndex(column));
    }

    @Override
    public void onScrollChanged(RecyclerView recyclerView, int dx, int dy) {
        if (dy > 0) //check for scroll down
        {
            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = recyclerView.getLayoutManager().getItemCount();
            int pastVisibleItem = ((GridLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

            if ((visibleItemCount + pastVisibleItem) >= totalItemCount) {
                mActionsListener.loadMovies(mActionsListener.getCurrentMovieFilter());
            }
        }
    }
}
