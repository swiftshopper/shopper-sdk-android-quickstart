package com.retailer.anyapp.movies;

import android.os.Bundle;
import androidx.annotation.NonNull;

import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.MovieFilterList;

import java.util.List;

/**
 * Created by joelr on 12/14/2015.
 * Represents the movie contracts between a Movies Fragment and a MoviesPresenter
 * This follows a MVP Design Pattern.
 */
public interface MoviesContract {

    /**
     * Represents the methods that our view (Activity or Fragment)
     * will implement.
     */
    interface View {

        void setProgressIndicator(boolean active);

        void showMovieDetailUi(Bundle movieArgs);

        void setMovies(List<Movie> movies);

        void clearMovies();

        void showMessage(String msg, int duration);

        void loadFavoriteMovies();
    }

    /**
     * Represents the method calls that our presenter will implement,
     * as the interface name implies, this are user based actions or similar that trigger
     * actions on the presenter, the presenter then will callback using it's
     * reference on a MoviesContract.View member.
     */
    interface UserActionsListener {

        void setProgressIndicator(boolean active);

        void openMovieDetails(@NonNull Movie requestedMovie);

        void loadMovies(MovieFilterList filterCriteria);

        int getResultPageNumber();

        void setLastKnownResultPage(int pageNumber);

        MovieFilterList getCurrentMovieFilter();

        void setLastKnownFilter(MovieFilterList filter);
    }
}
