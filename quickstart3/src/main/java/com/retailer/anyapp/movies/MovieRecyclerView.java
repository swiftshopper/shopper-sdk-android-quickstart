package com.retailer.anyapp.movies;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/**
 * Created by joelr on 11/29/2015.
 */
public class MovieRecyclerView extends RecyclerView {

    private MovieOnScrollChangedListener mMovieOnScrollChangedListener;

    public MovieRecyclerView(Context context) {
        super(context);
        init(context);
    }

    public MovieRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public MovieRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    /**
     * Initializes the basics component for our specialized Movie recycler view.
     *
     * @param context
     */
    private void init(Context context) {
        setHasFixedSize(true);
        setItemAnimator(new DefaultItemAnimator());
        setOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mMovieOnScrollChangedListener != null) {
                    mMovieOnScrollChangedListener.onScrollChanged(recyclerView, dx, dy);
                }
            }
        });
    }

    /**
     * Adds a custom onScrollChangeListener
     *
     * @param movieOnScrollChangedListener the listener to be used.
     */
    public void addOnScrollChangedListener(MovieOnScrollChangedListener movieOnScrollChangedListener) {
        mMovieOnScrollChangedListener = movieOnScrollChangedListener;
    }
}
