package com.retailer.anyapp.movies;

import android.view.View;

/**
 * Created by joelr on 11/12/2015.
 * A simple interface to communicate with our recycler view items,
 * <p/>
 * <p/>
 * this is a simple work around you can also use the LayoutManager built in methods to accomplish
 * similar functionality.
 * <p/>
 * Created by Joel Roman Sosa  on 8/10/2015.
 */
public interface MovieItemClickListener {
    void onMovieClick(View v, int position);
}

