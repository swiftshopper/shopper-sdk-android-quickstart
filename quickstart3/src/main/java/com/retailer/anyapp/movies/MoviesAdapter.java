package com.retailer.anyapp.movies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.retailer.anyapp.R;
import com.retailer.anyapp.domain.Genre;
import com.retailer.anyapp.domain.Image;
import com.retailer.anyapp.domain.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joelr on 11/5/2015.
 * The Movie Adapter handles the binding between the view and our Movie models.
 */
public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int NORMAL_VIEW_TYPE = 1;
    private final static int EXPANDED_VIEW_TYPE = 2;
    private Context mContext;
    private Picasso mPicasso;
    private List<Movie> mMovies;
    private MovieItemClickListener itemClickedListener;

    public MoviesAdapter(Context context, MovieItemClickListener clickListener) {
        this.mContext = context;
        this.mMovies = new ArrayList<>();
        this.mPicasso = Picasso.with(context);
        this.itemClickedListener = clickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case NORMAL_VIEW_TYPE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_movie_item, parent, false);
                return new MovieMultiRowViewHolder(v, itemClickedListener);
            case EXPANDED_VIEW_TYPE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_expanded_movie_item, parent, false);
                return new MovieSingleRowViewHolder(v, itemClickedListener);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        Movie movie = mMovies.get(position);
        if (movie != null) {
            switch (getItemViewType(position)) {
                case NORMAL_VIEW_TYPE:
                    bindNormalTypeView(holder, movie);
                    break;
                case EXPANDED_VIEW_TYPE:
                    bindExpandedViewType(holder, movie);
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        int comparator = mContext.getResources().getInteger(R.integer.movies_span) +
                mContext.getResources().getInteger(R.integer.movies_span_when_even);
        return (position % comparator == 0 ? EXPANDED_VIEW_TYPE : NORMAL_VIEW_TYPE);
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    /**
     * Clears our @member mMovies.
     */
    public void clear() {
        int size = mMovies.size();
        mMovies.clear();
        notifyItemRangeRemoved(0, size);
    }

    /**
     * Adds all Movie objects to the current @member mMovies.
     * and notifies it to the adapter.
     *
     * @param movies
     */
    public void addAll(List<Movie> movies) {
        if (mMovies.size() > 0) {
            int mSize = mMovies.size();
            mMovies.addAll(mSize, movies);
            notifyItemRangeInserted(mSize, movies.size());
        } else {
            mMovies.addAll(movies);
            notifyItemRangeInserted(0, movies.size());
        }
    }

    /**
     * Checks if our @member mMovies has items.
     *
     * @return
     */
    public boolean IsNotEmpty() {
        return mMovies.size() > 0;
    }

    /**
     * Retrieve a copy of @member mMovies
     *
     * @return mMovies List
     */
    public List<Movie> getAll() {
        return mMovies;
    }

    /**
     * Returns a Movie object for the given position within @member mMovies.
     *
     * @param position
     * @return
     */
    public Movie get(int position) {
        return mMovies.get(position);
    }

    /**
     * Binds a 'Expanded View Type' version of our Movie object.
     *
     * @param holder the inflated view
     * @param movie  The movie object.
     */
    private void bindExpandedViewType(final RecyclerView.ViewHolder holder, Movie movie) {
        mPicasso.load(Image.BASE_URL + Image.SIZE_W500 + movie.poster_path).into(((MovieSingleRowViewHolder) holder).posterImageView);
        ((MovieSingleRowViewHolder) holder).titleTextView.setText(movie.title);
        ((MovieSingleRowViewHolder) holder).voteAverageTextView.setText(String.valueOf(movie.vote_average));
        ((MovieSingleRowViewHolder) holder).releaseDateTextView.setText(android.text.format.DateFormat.format("yyyy", movie.release_date));
        if (movie.overview != null)
            if (movie.overview.length() > 100) {
                ((MovieSingleRowViewHolder) holder).overViewTextView.setText(String.valueOf(movie.overview.substring(0, 97) + "..."));//truncate
            } else {
                ((MovieSingleRowViewHolder) holder).overViewTextView.setText(movie.overview);
            }

        if (movie.genre_ids != null) {
            ((MovieSingleRowViewHolder) holder).genresTextView.setText(Genre.toCsv(movie.genre_ids));
        }
    }

    /**
     * Binds a 'Normal View Type' version of our Movie object.
     *
     * @param holder the inflated view
     * @param movie  The movie object.
     */
    private void bindNormalTypeView(final RecyclerView.ViewHolder holder, Movie movie) {
        mPicasso.load(Image.BASE_URL + Image.SIZE_W500 + movie.poster_path).into(((MovieMultiRowViewHolder) holder).posterImageView);
        ((MovieMultiRowViewHolder) holder).titleTextView.setText(movie.title);
    }

}
