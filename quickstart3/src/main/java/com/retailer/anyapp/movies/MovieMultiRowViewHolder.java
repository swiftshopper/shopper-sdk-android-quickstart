package com.retailer.anyapp.movies;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.retailer.anyapp.R;

/**
 * Created by joelr on 11/12/2015.
 */
public class MovieMultiRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private MovieItemClickListener itemClickedListener;

    public ImageView posterImageView;
    public TextView titleTextView;

    public MovieMultiRowViewHolder(View v, MovieItemClickListener itemClickedListener) {
        super(v);
        this.itemClickedListener = itemClickedListener;
        v.setOnClickListener(this);
        posterImageView = (ImageView) v.findViewById(R.id.movie_poster_image_view);
        titleTextView = (TextView) v.findViewById(R.id.title_text_view);
    }

    @Override
    public void onClick(View v) {
        if (itemClickedListener != null)
            itemClickedListener.onMovieClick(v, this.getLayoutPosition());
    }
}
