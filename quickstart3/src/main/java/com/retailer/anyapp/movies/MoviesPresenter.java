package com.retailer.anyapp.movies;

import android.os.Bundle;
import androidx.annotation.NonNull;
import android.support.design.widget.Snackbar;

import com.retailer.anyapp.domain.Language;
import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.MovieFilterList;
import com.retailer.anyapp.domain.MovieResultsPage;
import com.retailer.anyapp.domain.api.ApiClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by joelr on 12/14/2015.
 */
public class MoviesPresenter implements MoviesContract.UserActionsListener, Callback<MovieResultsPage> {

    public final static String TAG = "MoviesPresenter";
    public static final String TAG_MOVIE_FILTER = "lastknownfilter";
    private int mResultsPage = 1;
    private boolean mFetching = false;
    private ApiClient mApiClient;
    private MoviesContract.View mMoviesView;
    private MovieFilterList mCurrentListFilter = MovieFilterList.NOW_PLAYING; //default;

    public MoviesPresenter(@NonNull ApiClient apiClient, @NonNull MoviesContract.View movieView) {
        this.mApiClient = apiClient;
        this.mMoviesView = movieView;
    }

    @Override
    public int getResultPageNumber() {
        return mResultsPage;
    }

    @Override
    public void setLastKnownResultPage(int pageNumber) {
        this.mResultsPage = pageNumber;
        this.mFetching = false;
    }

    @Override
    public void setLastKnownFilter(MovieFilterList filter) {
        this.mCurrentListFilter = filter;
        this.mFetching = false;
    }

    @Override
    public void setProgressIndicator(boolean active) {
        mMoviesView.setProgressIndicator(active);
    }

    @Override
    public void openMovieDetails(@NonNull Movie requestedMovie) {
        Bundle movieArgs = new Bundle();
        movieArgs.putParcelable(Movie.PARCELABLE_KEY, requestedMovie);
        mMoviesView.showMovieDetailUi(movieArgs);
    }

    @Override
    public void loadMovies(MovieFilterList filterCriteria) {
        if (filterCriteria != mCurrentListFilter) {
            mCurrentListFilter = filterCriteria; //set new filter criteria.
            mMoviesView.clearMovies(); //Clear old movies
            mMoviesView.setProgressIndicator(true); //And show progress indicator.
            mResultsPage = 1; //Reset page count and fetch with new criteria.
            mFetching = false; //Cancel any current fetch
        }

        if (mFetching) {
            return;
        }

        mFetching = true;
        if (filterCriteria == MovieFilterList.NOW_PLAYING) {
            mApiClient.moviesService().nowPlaying(mResultsPage, Language.LANGUAGE_EN.toString(), this);
        } else if (filterCriteria == MovieFilterList.HIGHEST_RATED) {
            mApiClient.moviesService().nowPlaying(mResultsPage, Language.LANGUAGE_EN.toString(), this);
        } else if (filterCriteria == MovieFilterList.MOST_POPULAR) {
            mApiClient.moviesService().topRated(mResultsPage, Language.LANGUAGE_EN.toString(), this);
        } else if (filterCriteria == MovieFilterList.FAVORITES) {
          mMoviesView.loadFavoriteMovies();
        }
    }

    @Override
    public MovieFilterList getCurrentMovieFilter() {
        return mCurrentListFilter;
    }

    @Override
    public void success(MovieResultsPage movieResultsPage, Response response) {
        mMoviesView.setMovies(movieResultsPage.results);
        mMoviesView.setProgressIndicator(false);
        mResultsPage++;
        mFetching = false;
    }

    @Override
    public void failure(RetrofitError error) {
        error.printStackTrace();
        mMoviesView.showMessage("An error occured while fetching movies", Snackbar.LENGTH_SHORT);
    }
}