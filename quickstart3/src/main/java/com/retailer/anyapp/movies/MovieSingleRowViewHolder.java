package com.retailer.anyapp.movies;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.retailer.anyapp.R;

/**
 * Created by joelr on 11/12/2015.
 */
public class MovieSingleRowViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private MovieItemClickListener itemClickedListener;

    public ImageView posterImageView;
    public TextView overViewTextView, titleTextView, genresTextView, releaseDateTextView, voteAverageTextView;
    public Button moreInfoButton;


    public MovieSingleRowViewHolder(View view, MovieItemClickListener itemClickedListener) {
        super(view);
        this.itemClickedListener = itemClickedListener;
        itemView.setOnClickListener(this);
        overViewTextView = (TextView) view.findViewById(R.id.overview_text_view);
        posterImageView = (ImageView) view.findViewById(R.id.movie_poster_image_view);
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        genresTextView = (TextView) view.findViewById(R.id.genres_text_view);
        releaseDateTextView = (TextView) view.findViewById(R.id.release_date_text_view);
        voteAverageTextView = (TextView) view.findViewById(R.id.vote_average_text_view);
        moreInfoButton = (Button) view.findViewById(R.id.more_info_button);

    }

    @Override
    public void onClick(View v) {
        if (itemClickedListener != null)
            itemClickedListener.onMovieClick(v, this.getLayoutPosition());
    }
}
