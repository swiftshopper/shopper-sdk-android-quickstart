package com.retailer.anyapp.details;

import com.retailer.anyapp.domain.Reviews;
import com.retailer.anyapp.domain.Videos;

import java.util.List;

/**
 * Created by joelr on 12/16/2015.
 */
public interface MovieDetailsContract {

    interface View {

        void setProgressIndicator(boolean active);

        void showMessage(String msg, int duration);

        void setMoviePoster(String moviePoster);

        void setMovieBackdropImage(String backdropImage);

        void setTitle(String title);

        void setOverview(String overview);

        void setRating(String rating);

        void setReleaseDate(String releaseDate);

        void setFavoriteButtonSrc(boolean isFavorite);

        void setFavoriteButtonBackgroundColor(int color);

        void setPlayTrailerButtonBackgroundColor(int color);

        void setShareTrailerButtonBackgroundColor(int color);

        void setTrailers(List<Videos.Video> videoList);

        void setFirstTrailer(Videos.Video video);

        void setReviews(List<Reviews.Review> results);
    }

    interface UserActionsListener {

        void showMovieDetails();

        void changeFavoriteButtonSrc(boolean isFavorite);

        void changeFavoriteButtonBackgroundColor(int color);

        void changePlayTrailerButtonBackgroundColor(int color);

        void changeShareTrailerButtonBackgroundColor(int color);

        void loadTrailers();

        void loadFirstTrailer();
    }
}
