package com.retailer.anyapp.details;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import androidx.palette.graphics.Palette;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.retailer.anyapp.BaseFragment;
import com.retailer.anyapp.R;
import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.Reviews;
import com.retailer.anyapp.domain.Videos;
import com.retailer.anyapp.domain.api.ApiClient;
import com.retailer.anyapp.util.FavoriteMoviesProvider;
import com.retailer.anyapp.util.PaletteTransformation;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

// TODO: 12/20/2015 Add favorite impl and store info in db

/**
 * A Movie Details Fragment
 */
public class MovieDetailsFragment extends BaseFragment
        implements MovieDetailsContract.View, View.OnClickListener {

    public static final String TAG = "MovieDetailsFragment";
    private static final String KEY_COLOR = "PalleteColor";

    private Movie mMovie;
    private LinearLayout contentView;
    private ImageView mBackdropView, mPosterView;
    private MovieDetailsContract.UserActionsListener mUserActionListener;
    private TextView mTitleView;
    private TextView mOverviewView;
    private FloatingActionButton mFavoriteView, mPlayTrailerView, mShareTrailerView;
    private int mPalletColor;
    private TextView mRatingView;
    private TextView mReleaseDate;
    private TextView reviewsHeader;

    public MovieDetailsFragment() {
        // Required empty public constructor
    }

    public static MovieDetailsFragment newInstance(Bundle arg) {
        MovieDetailsFragment fragment = new MovieDetailsFragment();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            mMovie = (Movie) savedInstanceState.get(Movie.PARCELABLE_KEY);
            mPalletColor = savedInstanceState.getInt(MovieDetailsFragment.KEY_COLOR);
        } else {
            mMovie = getArguments().getParcelable(Movie.PARCELABLE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_movie_details, container, false);
        contentView = (LinearLayout) root.findViewById(R.id.content_view);
        reviewsHeader = (TextView) root.findViewById(R.id.reviews_header_text_view);
        mBackdropView = (ImageView) root.findViewById(R.id.movie_backdrop_image_view);
        mPosterView = (ImageView) root.findViewById(R.id.movie_poster_image_view);
        mTitleView = (TextView) root.findViewById(R.id.movie_title_text_view);
        mOverviewView = (TextView) root.findViewById(R.id.movie_overview_text_view);
        mRatingView = (TextView) root.findViewById(R.id.rating_text_view);
        mFavoriteView = (FloatingActionButton) root.findViewById(R.id.favorite_image_button);
        mReleaseDate = (TextView) root.findViewById(R.id.release_date_text_view);
        mPlayTrailerView = (FloatingActionButton) root.findViewById(R.id.play_trailer_image_button);
        mShareTrailerView = (FloatingActionButton) root.findViewById(R.id.share_trailer_image_button);

        mPlayTrailerView.setOnClickListener(this);
        mShareTrailerView.setOnClickListener(this);
        mFavoriteView.setOnClickListener(this);

        return root;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(Movie.PARCELABLE_KEY, mMovie);
        outState.putInt(MovieDetailsFragment.KEY_COLOR, mPalletColor);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mUserActionListener = new MovieDetailsPresenter(mMovie, this, ApiClient.getApiClient());
        mUserActionListener.showMovieDetails();

        if (!weAreInATabletSizeDevice()) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.movies_details_title));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setProgressIndicator(boolean active) {

    }

    @Override
    public void showMessage(String msg, int duration) {
        showSnackbar(getView(), msg);
    }

    @Override
    public void setMoviePoster(String moviePoster) {
        Picasso.with(getContext())
                .load(moviePoster)
                .into(mPosterView);
    }

    @Override
    public void setMovieBackdropImage(String backdropImage) {
        final PaletteTransformation transformation = PaletteTransformation.getInstance();
        Picasso.with(getContext())
                .load(backdropImage)
                .transform(transformation)
                .into(mBackdropView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Palette palette = transformation.extractPaletteAndRelease();
                        if (palette != null) {
                            mPalletColor = palette.getDarkVibrantColor(getResources().getColor(R.color.colorPrimaryDark));
                            setFabColors();
                            if (!weAreInATabletSizeDevice()) {
                                setActionbarAndStatusBarColors(mPalletColor, mPalletColor);
                            }
                        }
                    }

                    @Override
                    public void onError() {

                    }
                });
    }

    @Override
    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    @Override
    public void setOverview(String overview) {
        mOverviewView.setText(overview);
    }

    @Override
    public void setRating(String rating) {
        mRatingView.append(getString(R.string.movies_details_rating_placeholder) + rating);
    }

    @Override
    public void setReleaseDate(String releaseDate) {
        mReleaseDate.append(getString(R.string.movies_datail_release_date_placeholder) + releaseDate);
    }

    @Override
    public void setFavoriteButtonSrc(boolean isFavorite) {

    }

    @Override
    public void setFavoriteButtonBackgroundColor(int color) {
        mFavoriteView.setBackgroundTintList(ColorStateList.valueOf(color));
    }

    @Override
    public void setPlayTrailerButtonBackgroundColor(int color) {
        mPlayTrailerView.setBackgroundTintList(ColorStateList.valueOf(color));
    }

    @Override
    public void setShareTrailerButtonBackgroundColor(int color) {
        mShareTrailerView.setBackgroundTintList(ColorStateList.valueOf(color));
    }

    @Override
    public void setTrailers(final List<Videos.Video> videoList) {
        if (!videoList.isEmpty()) {
            int vidSize = videoList.size();
            String[] titles = new String[vidSize];
            for (int i = 0; i < vidSize; i++) {
                titles[i] = videoList.get(i).name;
            }
            AlertDialog dialog = new AlertDialog.Builder(getContext())
                    .setTitle(R.string.movies_detail_trailer_dialog_title)
                    .setSingleChoiceItems(titles, -1, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            launchTrailer(videoList.get(which).key);
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
        } else {
            showSnackbar(getView(), "No trailers found.");
        }
    }

    private void launchTrailer(String movieUrl) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Videos.YOUTUBE_BASE + movieUrl)));
    }

    @Override
    public void setFirstTrailer(Videos.Video video) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, Videos.YOUTUBE_BASE + video.key);
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.open_with)));
    }

    @Override
    public void setReviews(List<Reviews.Review> results) {
        if (results != null && results.size() > 0) {

            for (Reviews.Review review : results) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.view_detail_review, contentView, false);
                TextView author = (TextView) view.findViewById(R.id.author_text_view);
                TextView content = (TextView) view.findViewById(R.id.content_text_view);
                author.setText(review.author);
                content.setText(review.content);
                contentView.addView(view);
            }
        } else {
            reviewsHeader.append(" none yet.");
        }
    }

    @Override
    public void onResume() {
        if (mPalletColor != 0 & mUserActionListener != null) {
            setFabColors();
            if (isVersionOrNewer(Build.VERSION_CODES.LOLLIPOP)) {
                setActionbarAndStatusBarColors(mPalletColor, mPalletColor);
            }

        }
        super.onResume();
    }

    /**
     * Sets the Floating action button background tint color.
     */
    private void setFabColors() {
        mUserActionListener.changeFavoriteButtonBackgroundColor(mPalletColor);
        mUserActionListener.changePlayTrailerButtonBackgroundColor(mPalletColor);
        mUserActionListener.changeShareTrailerButtonBackgroundColor(mPalletColor);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == mPlayTrailerView.getId()) {
            loadTrailers();
        } else if (id == mShareTrailerView.getId()) {
            loadFirstTrailer();
        } else if (id == mFavoriteView.getId()) {
            setFavOrUnFav();
        }
    }

    private void setFavOrUnFav() {
        // TODO: 12/20/2015 So far it works, maybe clean and add to presenter logic
        Cursor cursor = getContext().getContentResolver().query(FavoriteMoviesProvider.CONTENT_URI,
                null, FavoriteMoviesProvider.ID + "=" + String.valueOf(mMovie.id),
                null, null);

        //If our cursor is not null and the count is 0 it's a new favorite, else removed it.
        if (cursor != null) {
            if (cursor.getCount() == 0) {

                ContentValues values = new ContentValues();
                values.put(FavoriteMoviesProvider.ID, mMovie.id);
                values.put(FavoriteMoviesProvider.TITLE, mMovie.title);
                values.put(FavoriteMoviesProvider.BACKDROP, mMovie.backdrop_path);
                values.put(FavoriteMoviesProvider.AVERAGE_RATING, String.valueOf(mMovie.vote_average));
                values.put(FavoriteMoviesProvider.RELEASE_DATE, mMovie.release_date.getTime());
                values.put(FavoriteMoviesProvider.POSTER, mMovie.poster_path);
                values.put(FavoriteMoviesProvider.OVERVIEW, mMovie.overview);

                getContext().getContentResolver().update(
                        FavoriteMoviesProvider.CONTENT_URI, values,
                        FavoriteMoviesProvider.ID + "=" + mMovie.id, null);
                showMessage(mMovie.title + " " + "added to favorites", 0);

            } else {
                getContext().getContentResolver().delete(
                        FavoriteMoviesProvider.CONTENT_URI,
                        FavoriteMoviesProvider.ID + "=" + mMovie.id, null);
                showMessage(mMovie.title + " " + "removed from favorites", 0);
            }
            cursor.close();
        }
    }

    private void loadFirstTrailer() {
        if (isNetworkAvailable()) {
            mUserActionListener.loadFirstTrailer();
            showSnackbar(getView(), "Preparing trailer for sharing");
        } else {
            showMessage("You are offline", 0);
        }
    }

    private void loadTrailers() {
        if (isNetworkAvailable()) {
            mUserActionListener.loadTrailers();
        } else {
            showMessage("You are offline", 0);
        }
    }
}
