package com.retailer.anyapp.details;

import android.support.design.widget.Snackbar;

import com.retailer.anyapp.BasePresenter;
import com.retailer.anyapp.domain.Image;
import com.retailer.anyapp.domain.Language;
import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.Reviews;
import com.retailer.anyapp.domain.Videos;
import com.retailer.anyapp.domain.api.ApiClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by joelr on 12/16/2015.
 */
public class MovieDetailsPresenter extends BasePresenter implements MovieDetailsContract.UserActionsListener, Callback<Videos> {

    private MovieDetailsContract.View mMovieDetailsView;
    private Movie mMovie;
    private ApiClient mApiClient;

    public MovieDetailsPresenter(Movie movie, MovieDetailsContract.View movieDetailsView, ApiClient apiClient) {
        this.mMovie = movie;
        this.mMovieDetailsView = movieDetailsView;
        this.mApiClient = apiClient;
    }


    @Override
    public void showMovieDetails() {
        if (IsNotEmpty(mMovie.backdrop_path)) {
            mMovieDetailsView.setMovieBackdropImage(Image.BASE_URL + Image.SIZE_W780 + mMovie.backdrop_path);
        }

        if (IsNotEmpty(mMovie.poster_path)) {
            mMovieDetailsView.setMoviePoster(Image.BASE_URL + Image.SIZE_W780 + mMovie.poster_path);
        }

        mMovieDetailsView.setTitle(mMovie.title);
        mMovieDetailsView.setOverview(mMovie.overview);
        mMovieDetailsView.setRating(String.valueOf(mMovie.vote_average));
        mMovieDetailsView.setReleaseDate((String) android.text.format.DateFormat.format("yyyy", mMovie.release_date));

        mApiClient.moviesService().reviews(mMovie.id, Language.LANGUAGE_EN.toString(), new Callback<Reviews>() {
            @Override
            public void success(Reviews reviews, Response response) {
                mMovieDetailsView.setReviews(reviews.results);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public void changeFavoriteButtonSrc(boolean isFavorite) {
        mMovieDetailsView.setFavoriteButtonSrc(isFavorite);
    }

    @Override
    public void changeFavoriteButtonBackgroundColor(int color) {
        mMovieDetailsView.setFavoriteButtonBackgroundColor(color);
    }

    @Override
    public void changePlayTrailerButtonBackgroundColor(int color) {
        mMovieDetailsView.setPlayTrailerButtonBackgroundColor(color);
    }

    @Override
    public void changeShareTrailerButtonBackgroundColor(int color) {
        mMovieDetailsView.setShareTrailerButtonBackgroundColor(color);
    }

    @Override
    public void loadTrailers() {
        mMovieDetailsView.showMessage("Loading trailers...", Snackbar.LENGTH_LONG);
        mApiClient.moviesService().videos(mMovie.id, Language.LANGUAGE_EN.toString(), this);
    }

    @Override
    public void loadFirstTrailer() {
        mApiClient.moviesService().videos(mMovie.id, Language.LANGUAGE_EN.toString(), new Callback<Videos>() {
            @Override
            public void success(Videos videos, Response response) {
                mMovieDetailsView.setFirstTrailer(videos.results.get(0));
            }

            @Override
            public void failure(RetrofitError error) {
                error.printStackTrace();
                mMovieDetailsView.showMessage(error.getLocalizedMessage(), 0);
            }
        });
    }

    @Override
    public void success(Videos videos, Response response) {
        mMovieDetailsView.setTrailers(videos.results);
    }

    @Override
    public void failure(RetrofitError error) {
        error.printStackTrace();
        mMovieDetailsView.showMessage(error.getLocalizedMessage(), 0);
    }
}
