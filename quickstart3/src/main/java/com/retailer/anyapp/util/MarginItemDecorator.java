package com.retailer.anyapp.util;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.retailer.anyapp.R;

/**
 * Created by joelr on 11/16/2015.
 */
public class MarginItemDecorator extends RecyclerView.ItemDecoration {
    private int margin;

    public MarginItemDecorator(Context context) {
        margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(margin, margin, margin, margin);
    }
}
