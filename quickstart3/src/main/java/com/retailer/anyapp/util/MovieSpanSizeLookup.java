package com.retailer.anyapp.util;

import android.support.v7.widget.GridLayoutManager;

/**
 * Created by joelr on 11/17/2015.
 * A custom SpanSizeLookup class, this helps us set the desired column spans across our RecyclerViews more easy.
 */
public class MovieSpanSizeLookup extends GridLayoutManager.SpanSizeLookup {

    private int mColumnsSpan, mSpanWhenPositionIsOdd, mSpanWhenPositionIsEven;

    public MovieSpanSizeLookup(int columnsSpan, int spanWhenRemainderZero, int spanWhenRemainderNotZero) {
        this.mColumnsSpan = columnsSpan + 1; //we must always add 1 to our column span count in order to behave correctly.
        this.mSpanWhenPositionIsOdd = spanWhenRemainderZero;
        this.mSpanWhenPositionIsEven = spanWhenRemainderNotZero;
    }

    @Override
    public int getSpanSize(int position) {
        return (position % mColumnsSpan == 0 ? mSpanWhenPositionIsOdd : mSpanWhenPositionIsEven);
    }
}
