package com.retailer.anyapp.util;

import android.graphics.Bitmap;
import androidx.core.util.Pools;
import androidx.palette.graphics.Palette;
import android.util.Log;

import com.squareup.picasso.Transformation;

/**
 * Created by joelr on 11/21/2015.
 * An extension for Transofrmation in Picasso used for palette extraction,
 * this implementation follow one of Jake's Wharton blog posts found
 * here: http://jakewharton.com/coercing-picasso-to-play-with-palette/
 */
public class PaletteTransformation implements Transformation {
    private static final Pools.Pool<PaletteTransformation> POOL = new Pools.SynchronizedPool<>(5);

    /**
     * Factory.
     *
     * @return
     */
    public static PaletteTransformation getInstance() {
        PaletteTransformation instance = POOL.acquire();
        return instance != null ? instance : new PaletteTransformation();
    }

    private Palette palette;

    private PaletteTransformation() {
    }

    /**
     * Extracts the Pallete object.
     *
     * @return
     */
    public Palette extractPaletteAndRelease() {
        Palette palette = this.palette;
        if (palette == null) {
            Log.e("TRANSFORMATION", "Transformation was not run");
            return null;
        }
        this.palette = null;
        POOL.release(this);
        return palette;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        if (palette != null) {
            throw new IllegalStateException("Instances may only be used once.");
        }
        palette = Palette.generate(source); //This is depracated, in the future favor ColorUtils instead.
        return source;
    }

    @Override
    public String key() {
        return "";
    }
}
