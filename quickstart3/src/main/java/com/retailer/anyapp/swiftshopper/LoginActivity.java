package com.retailer.anyapp.swiftshopper;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.retailer.anyapp.R;
import com.swiftshopper.sdk.ui.view.SsLoginView;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        SsLoginView ssLoginView = findViewById(R.id.login_view);
        ssLoginView.setLoginDelegate(new SsLoginView.Delegate() {
            @Override
            public void onLoginSuccess() {
                onBackPressed();
            }

            @Override
            public void onLoginFailure(Throwable throwable) {
                Snackbar.make(ssLoginView, "Error logging in", Snackbar.LENGTH_LONG).show();

            }

            @Override
            public void proceedAnonymously(Runnable runnable) {
                runnable.run();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
