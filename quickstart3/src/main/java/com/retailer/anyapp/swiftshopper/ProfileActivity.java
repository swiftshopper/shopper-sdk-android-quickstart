package com.retailer.anyapp.swiftshopper;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.retailer.anyapp.R;
import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        SsUser ssUser;
        String ssUserDisplayName;
        if ((ssUser=SwiftShopperSdk.getUser()) != null &&
                (ssUserDisplayName=ssUser.getUserDisplayName()) != null) {
            if (ssUserDisplayName.endsWith("s")) {
                setTitle(ssUserDisplayName + "' Profile");
            } else {
                setTitle(ssUserDisplayName + "'s Profile");
            }
        }

        findViewById(R.id.self_scan_shopping_button).setOnClickListener((v)->{
            ProgressBar progressBar = findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.VISIBLE);
            SwiftShopperSdk.enterStore(50, new SwiftShopperSdk.EnterStoreListener() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.INVISIBLE);
                    startActivity(new Intent(ProfileActivity.this, SelfScanShoppingActivity.class));
                }

                @Override
                public void onFailure() {
                    Toast.makeText(ProfileActivity.this, "Error checking into the store.", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            });
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {

            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
            builder.setTitle(getTitle());
            builder.setMessage(R.string.logout_confirm);
            // Reverse psychology: "Going Back" or "Cancelling" is illustrated as the positive choice
            builder.setPositiveButton(R.string.logout_confirm_no, (d, i)->{

            });
            builder.setNegativeButton(R.string.logout_confirm_yes, (d,i)->{
                SwiftShopperSdk.logout(this::finish);
            });
            builder.create().show();
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
