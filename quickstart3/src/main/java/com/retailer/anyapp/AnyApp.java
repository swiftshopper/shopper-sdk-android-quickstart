package com.retailer.anyapp;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.core.content.res.ResourcesCompat;

import com.retailer.anyapp.domain.api.ApiClient;
import com.swiftshopper.sdk.SsConfig;
import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.login.LoginListener;

/**
 * Created by joelr on 11/12/2015.
 */
public class AnyApp extends Application {

    public BroadcastReceiver mSwiftShopperBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent mainActivityIntent = new Intent(getApplicationContext(),
                    MainActivity.class);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainActivityIntent);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        ApiClient.initialize(BuildConfig.THEMOVIEDB_KEY);
        SwiftShopperSdk.initialize(new SsConfig.Builder(this)
                .setRetailerSdkKey("w9mn{dn7lPIF37Yb#>fh)By^Q|AoA3w8<SGXbd2gmPF2|")
                .setSmallLogo(R.mipmap.ic_launcher)
                .setLargeLogo(R.mipmap.ic_launcher)
                .setThemeColor(ResourcesCompat.getColor(
                        getResources(), R.color.colorPrimary, null))
                .build());

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SwiftShopperSdk.ACTION_CHECKOUT_COMPLETE);
        registerReceiver(mSwiftShopperBroadCastReceiver, intentFilter);


        SsUser ssUser = SwiftShopperSdk.getUser();
        if (ssUser == null) {
            SwiftShopperSdk.loginAnonymous(new LoginListener() {
                @Override
                public void onLoginSuccess() {
                    System.out.println("Anonymous login success");
                }

                @Override
                public void onLoginFailure(Throwable throwable) {
                    System.out.println("Anonymous login failure: " + throwable);
                }
            });
        }
    }

    @Override
    public void onTerminate() {
        unregisterReceiver(mSwiftShopperBroadCastReceiver);
        super.onTerminate();
    }
}
