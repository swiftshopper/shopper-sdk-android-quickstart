package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/9/2015.
 * Represents the Spoken Language JSON of the Movie within the JSON response.
 */
public class SpokenLanguage {
    public String iso_639_1;
    public String name;
}
