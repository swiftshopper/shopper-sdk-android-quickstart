package com.retailer.anyapp.domain;

import java.util.Date;

/**
 * Created by joelr on 11/9/2015.
 * Represents the country data information along with certification of notice.
 */
public class CountryRelease {
    public Date release_date;
    public String iso_3166_1;
    public String certification;
}
