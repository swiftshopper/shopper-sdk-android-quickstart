package com.retailer.anyapp.domain.api;

import com.retailer.anyapp.domain.AppendToResponse;
import com.retailer.anyapp.domain.Credits;
import com.retailer.anyapp.domain.Images;
import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.MovieResultsPage;
import com.retailer.anyapp.domain.Releases;
import com.retailer.anyapp.domain.Reviews;
import com.retailer.anyapp.domain.Videos;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by joelr on 11/12/2015.
 * Serves as the primary interface for Movies under the discover section,
 * by making them interfaces its make our work with Retrofit easier to implement.
 */
public interface MoviesInterface {

    /**
     * Get the basic movie information for a specific movie id.
     *
     * @param tmdbId           TMDb id.
     * @param language         <em>Optional.</em> ISO 639-1 code.
     * @param appendToResponse <em>Optional.</em> extra requests to append to the result.
     */
    @GET("/movie/{id}")
    Movie summary(
            @Path("id") int tmdbId,
            @Query("language") String language,
            @Query("append_to_response") AppendToResponse appendToResponse
    );


    /**
     * Get the cast and crew information for a specific movie id.
     *
     * @param tmdbId TMDb id.
     */
    @GET("/movie/{id}/credits")
    Credits credits(
            @Path("id") int tmdbId
    );

    /**
     * Get the images (posters and backdrops) for a specific movie id.
     *
     * @param tmdbId   TMDb id.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/{id}/images")
    void images(
            @Path("id") int tmdbId,
            @Query("language") String language,
            Callback<Images> callback
    );


    /**
     * Get the release date and certification information by country for a specific movie id.
     *
     * @param tmdbId TMDb id.
     */
    @GET("/movie/{id}/releases")
    Releases releases(
            @Path("id") int tmdbId
    );

    /**
     * Get the videos (trailers, teasers, clips, etc...) for a specific movie id.
     *
     * @param tmdbId   TMDb id.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/{id}/videos")
    void videos(
            @Path("id") int tmdbId,
            @Query("language") String language,
            Callback<Videos> videosCallback
    );

    /**
     * Get the reviews (author, content, etc...) for a specific movie id.
     *
     * @param tmdbId   TMDb id.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/{id}/reviews")
    void reviews(
            @Path("id") int tmdbId,
            @Query("language") String language,
            Callback<Reviews> videosCallback
    );

    /**
     * Get the similar movies for a specific movie id.
     *
     * @param tmdbId   TMDb id.
     * @param page     <em>Optional.</em> Minimum value is 1, expected value is an integer.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/{id}/similar")
    MovieResultsPage similar(
            @Path("id") int tmdbId,
            @Query("page") Integer page,
            @Query("language") String language
    );


    /**
     * Get the latest movie id.
     */
    @GET("/movie/latest")
    Movie latest();

    /**
     * Get the list of upcoming movies. This list refreshes every day. The maximum number of items this list will
     * include is 100.
     *
     * @param page     <em>Optional.</em> Minimum value is 1, expected value is an integer.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/upcoming")
    MovieResultsPage upcoming(
            @Query("page") Integer page,
            @Query("language") String language
    );

    /**
     * Get the list of movies playing in theaters. This list refreshes every day. The maximum number of items this list
     * will include is 100.
     *
     * @param page     <em>Optional.</em> Minimum value is 1, expected value is an integer.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/now_playing")
    void nowPlaying(
            @Query("page") Integer page,
            @Query("language") String language,
            Callback<MovieResultsPage> results
    );

    /**
     * Get the list of popular movies on The Movie Database. This list refreshes every day.
     *
     * @param page     <em>Optional.</em> Minimum value is 1, expected value is an integer.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/popular")
    void popular(
            @Query("page") Integer page,
            @Query("language") String language,
            Callback<MovieResultsPage> results
    );

    /**
     * Get the list of top rated movies. By default, this list will only include movies that have 10 or more votes. This
     * list refreshes every day.
     *
     * @param page     <em>Optional.</em> Minimum value is 1, expected value is an integer.
     * @param language <em>Optional.</em> ISO 639-1 code.
     */
    @GET("/movie/top_rated")
    void topRated(
            @Query("page") Integer page,
            @Query("language") String language,
            Callback<MovieResultsPage> results
    );

}
