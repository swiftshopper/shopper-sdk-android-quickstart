package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/9/2015.
 * Represents the Production Company JSON of the Movie within the JSON response.
 */
public class ProductionCompany {
    public Integer id;
    public String name;
}
