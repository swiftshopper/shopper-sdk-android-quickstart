package com.retailer.anyapp.domain;

import java.util.List;

/**
 * Created by joelr on 11/9/2015.
 * Represents the places where the movie was release.
 */
public class Releases {
    public int id;
    public List<CountryRelease> countries;
}
