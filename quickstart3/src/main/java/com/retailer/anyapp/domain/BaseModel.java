package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/9/2015.
 * Represents the base response of Json been handle by Retrofit.
 */
public class BaseModel {
    public Integer page;
    public Integer total_pages;
    public Integer total_results;
}
