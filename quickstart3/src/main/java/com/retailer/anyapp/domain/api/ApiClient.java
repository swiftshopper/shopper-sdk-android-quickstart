package com.retailer.anyapp.domain.api;


import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by joelr on 10/31/2015.
 * Represent the API client to be used to retrieve data from
 * The Movie DB.
 * Created for Udacity Android Nanodegree Project (This will be used for both stage 1 and 2)
 */
public class ApiClient {

    /**
     * The movie database base api endpoint.
     */
    public final static String API_BASE_ENDPOINT = "https://api.themoviedb.org/3";

    /**
     * API key query parameter name.
     */
    public static final String PARAM_API_KEY = "api_key";

    private static ApiClient mApiClient;
    private String mApiKey;
    private RestAdapter mRestAdapter;

    public ApiClient() {
    }

    public static ApiClient getApiClient() {
        return mApiClient;
    }

    /**
     * Set the The Movie Database API key.
     *
     * @param value The API key.
     */
    public ApiClient setApiKey(String value) {
        this.mApiKey = value;
        mRestAdapter = null;
        return mApiClient;
    }

    public static void initialize(String apiKey) {
        if (mApiClient == null) {
            mApiClient = new ApiClient();
        }

        mApiClient.setApiKey(apiKey);
    }

    /**
     * Create a new {@link retrofit.RestAdapter.Builder}. Override this to e.g. set your own client or executor.
     *
     * @return A {@link retrofit.RestAdapter.Builder} with no modifications.
     */
    protected RestAdapter.Builder newRestAdapterBuilder() {
        return new RestAdapter.Builder();
    }

    /**
     * Return the current {@link retrofit.RestAdapter} instance. If none exists (first call, API key changed),
     * builds a new one.
     * <p/>
     */
    protected RestAdapter getRestAdapter() {
        if (mRestAdapter == null) {
            RestAdapter.Builder builder = newRestAdapterBuilder();

            builder.setEndpoint(API_BASE_ENDPOINT);
            builder.setConverter(new GsonConverter(ApiHelper.getGsonBuilder().create()));
            builder.setRequestInterceptor(new RequestInterceptor() {
                public void intercept(RequestFacade requestFacade) {
                    requestFacade.addQueryParam(PARAM_API_KEY, mApiKey);
                }
            });
            mRestAdapter = builder.build();
        }

        return mRestAdapter;
    }


    public MoviesInterface moviesService() {
        return getRestAdapter().create(MoviesInterface.class);
    }
}
