package com.retailer.anyapp.domain;

import java.util.List;

/**
 * Created by joelr on 11/9/2015.
 * Represents the Videos information JSON object(s) from the Movie JSON response.
 */
public class Videos {
    public static final String YOUTUBE_BASE = "https://www.youtube.com/watch?v=";

    public static class Video {
        public String id;
        public String iso_639_1;
        public String key;
        public String name;
        public String site;
        public Integer size;
        public String type;
    }

    public Integer id;
    public List<Video> results;
}
