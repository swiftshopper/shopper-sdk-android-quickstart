package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/12/2015.
 */
public class Image {

    public final static String BASE_URL = "http://cf2.imgobject.com/t/p/";
    public final static String SIZE_W500 = "w500/";
    public final static String SIZE_W342 = "w342/";
    public final static String SIZE_W185 = "w185/";
    public final static String SIZE_W780 = "w780/";


    public String file_path;
    public Integer width;
    public Integer height;
    public String iso_639_1;
    public Float aspect_ratio;
    public Float vote_average;
    public Integer vote_count;
}
