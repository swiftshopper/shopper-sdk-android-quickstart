package com.retailer.anyapp.domain;

import java.util.List;

/**
 * Created by joelr on 11/12/2015.
 */
public class Images {

    public Integer id;
    public List<Image> backdrops;
    public List<Image> posters;
    public List<Image> stills;
}
