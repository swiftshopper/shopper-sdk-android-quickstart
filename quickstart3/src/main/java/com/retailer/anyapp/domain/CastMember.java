package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/9/2015.
 * Represents a CastMember object with basic information.
 */
public class CastMember extends BaseMember {
    public String character;
    public Integer order;
}
