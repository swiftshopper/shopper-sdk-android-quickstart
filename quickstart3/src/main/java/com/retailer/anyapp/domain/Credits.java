package com.retailer.anyapp.domain;

import java.util.List;

/**
 * Created by joelr on 11/9/2015.
 * Holds the Credits of the movie including cast, crew and guest starts information.
 */
public class Credits {
    public Integer id;

    public List<CastMember> cast;
    public List<CrewMember> crew;
    public List<CastMember> guest_stars;
}
