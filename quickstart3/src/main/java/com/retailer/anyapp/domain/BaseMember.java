package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/9/2015.
 * Represents the basic representation of a Movie member.
 */
public class BaseMember {
    public Integer id;
    public String credit_id;
    public String name;
    public String profile_path;
}
