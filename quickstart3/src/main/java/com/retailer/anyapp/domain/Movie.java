package com.retailer.anyapp.domain;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Created by joelr on 10/31/2015.
 */
public class Movie implements Parcelable {

    public Movie(int movieId, String title, String overview, long releaseDate, String averageRating, String poster_path, String backdrop_path) {
        this.id = movieId;
        this.title = title;
        this.overview = overview;
        this.release_date = new Date(releaseDate);
        this.vote_average = Double.parseDouble(averageRating);
        this.poster_path = poster_path;
        this.backdrop_path = backdrop_path;
    }

    public static final String PARCELABLE_KEY = "movies.key";
    public Integer id;
    public Boolean adult;
    public String title;
    public String overview;
    public Double popularity;
    public Double vote_average;
    public String poster_path;
    public String backdrop_path;
    public Date release_date;
    public Integer vote_count;
    public Boolean video;
    public List<Integer> genre_ids; //ids are now the only thing returned by a Discover call.
    public String original_title;
    public Collection belongs_to_collection;
    public List<SpokenLanguage> spoken_languages;
    public List<ProductionCompany> production_companies;
    public List<ProductionCountry> production_countries;

    // This can be retrieve when using: append_to_response
    public Videos videos;
    public Reviews reviews;
    public Releases releases;
    public Credits credits;
    public MovieResultsPage similar;

    public Movie() {

    }

    protected Movie(Parcel in) {
        title = in.readString();
        overview = in.readString();
        poster_path = in.readString();
        backdrop_path = in.readString();
        original_title = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeSerializable(adult);
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(poster_path);
        dest.writeString(backdrop_path);
        dest.writeSerializable(release_date);
        if (popularity != null)
            dest.writeDouble(popularity);

        if (vote_count != null)
            dest.writeInt(vote_count);
        if (video != null)
            dest.writeSerializable(video);

        if (genre_ids != null)
            dest.writeList(genre_ids);

        if (release_date != null)
            dest.writeString(original_title);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}

