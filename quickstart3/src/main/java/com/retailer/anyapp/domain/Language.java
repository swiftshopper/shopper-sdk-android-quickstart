package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/12/2015.
 */
public enum Language {

    LANGUAGE_EN("en");

    private final String value;

    private Language(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
