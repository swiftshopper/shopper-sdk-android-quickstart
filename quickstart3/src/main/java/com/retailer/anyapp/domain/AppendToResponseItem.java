package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/12/2015.
 * Appends any of the following to the Movie object.
 */
public enum AppendToResponseItem {

    VIDEOS("videos"),
    RELEASES("releases"),
    CREDITS("credits"),
    SIMILAR("similar"),
    IMAGES("images"),
    EXTERNAL_IDS("external_ids");

    private final String value;

    private AppendToResponseItem(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
