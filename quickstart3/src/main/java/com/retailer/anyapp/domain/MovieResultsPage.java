package com.retailer.anyapp.domain;

import java.util.List;

/**
 * Created by joelr on 11/9/2015.
 */
public class MovieResultsPage extends BaseModel {
    /**
     * Holds the Json response objects.
     */
    public List<Movie> results;
}
