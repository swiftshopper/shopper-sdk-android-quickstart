package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/19/2015.
 */
public enum MovieFilterList {
    MOST_POPULAR,
    HIGHEST_RATED,
    FAVORITES,
    NOW_PLAYING
}
