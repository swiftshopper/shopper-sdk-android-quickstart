package com.retailer.anyapp.domain;

import java.util.List;

/**
 * Created by joelr on 12/5/2015.
 */
public class Reviews {

    public static class Review {
        public String id;
        public String author;
        public String content;
        public String url;
    }

    public Integer id;
    public List<Review> results;

}
