package com.retailer.anyapp.domain;

/**
 * Created by joelr on 11/9/2015.
 * Represents the Production Country of the Movie within the JSON response.
 */
public class ProductionCountry {
    public String iso_3166_1;
    public String name;
}
