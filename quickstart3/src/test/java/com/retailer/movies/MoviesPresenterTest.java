package com.retailer.movies;

import com.retailer.anyapp.domain.Movie;
import com.retailer.anyapp.domain.api.ApiClient;
import com.retailer.anyapp.movies.MoviesContract;
import com.retailer.anyapp.movies.MoviesPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;


/**
 * Created by joelr on 12/15/2015.
 * The MoviesPresenterTest class handles all of the presenter calls that do not
 * work with network calls.
 */
public class MoviesPresenterTest {

    private static List<Movie> movieMocks = new ArrayList();
    private MoviesPresenter mMoviesPresenter;

    @Mock
    MoviesContract.View mMoviesView;

    @Before
    public void setupNotesPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mMoviesPresenter = new MoviesPresenter(ApiClient.getApiClient(), mMoviesView);
    }


    @Before
    public void setUp() {
        //Add a few movies to our {@movieMocks} collection.
        Movie movie = new Movie();
        movie.id = 12345;
        movie.title = "foo";

        Movie movie2 = new Movie();
        movie2.id = 6789;
        movie2.title = "bar";

        movieMocks.add(movie);
        movieMocks.add(movie2);
    }

    @Test
    public void movieView_showMessage() {
        //Given the following message
        mMoviesView.showMessage("foo", 1);

        //Message should has been showed.
        verify(mMoviesView).showMessage("foo", 1);
    }

    @Test
    public void movieView_setMovies() {
        //set a predefined mocked movie list.
        mMoviesView.setMovies(movieMocks);

        //Check that the movie list set is the mocked list.
        verify(mMoviesView).setMovies(movieMocks);
    }

    @Test
    public void movieView_SetProgressIndicator() {
        //Set progress indicator visibility
        mMoviesView.setProgressIndicator(true);

        //Check that it was successfully set.
        verify(mMoviesView).setProgressIndicator(true);
    }

    @Test
    public void movieView_clear() {
        //clear movie list.
        mMoviesView.clearMovies();

        //Check that it was successfully clear.
        verify(mMoviesView).clearMovies();
    }
}
