package com.swiftshopper.sdk.starterapp.view.dialogfragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.util.ActivityUtils;
import com.swiftshopper.sdk.starterapp.util.ColorUtils;
import com.swiftshopper.sdk.starterapp.util.Utility;
import com.swiftshopper.sdk.util.LoadingUtil;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.FragmentActivity;

public abstract class BaseDialogFragment<DATABINDING extends ViewDataBinding, VIEWMODEL> extends AppCompatDialogFragment {
    protected Context mContext;
    protected DATABINDING binding;
    protected VIEWMODEL viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
        LoadingUtil.setLoadingAnimation(mContext, binding);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = createViewModel();
        setViewModel(binding, viewModel);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        setStatusBarAndNavigationBar(dialog);
        return dialog;
    }

    private void setStatusBarAndNavigationBar(Dialog dialog) {
        Window window = dialog.getWindow();
        if (window != null) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ColorUtils.getStatusBarColor(SwiftShopperSdk.getThemeColor()));
            window.setNavigationBarColor(ColorUtils.getStatusBarColor(SwiftShopperSdk.getThemeColor()));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    protected abstract int getLayout();

    protected abstract VIEWMODEL createViewModel();

    protected abstract void setViewModel(DATABINDING binding, VIEWMODEL viewModel);

    public void launchActivity(Intent intent) {
        ActivityUtils.launchActivity(((FragmentActivity) mContext), intent);
    }

    public void launchActivity(Intent intent, boolean finishCurrent) {
        ActivityUtils.launchActivity(((FragmentActivity) mContext), intent, finishCurrent);
    }

    public void launchActivity(Intent intent, int requestCode) {
        ActivityUtils.launchActivity(((FragmentActivity) mContext), intent, requestCode);
    }

    protected void showToast(int message) {
        ActivityUtils.showToast(mContext, getString(message));
    }

    protected void showToast(String message) {
        ActivityUtils.showToast(mContext, message);
    }

    protected void sendActionBroadcast(Intent intent) {
        Utility.sendActionBroadcast(intent, mContext);
    }

    protected void hideKeyBoard() {
        View view = getView();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
