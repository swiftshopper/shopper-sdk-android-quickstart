package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.Toast;

import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.StarterApp;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.LoginBinding;
import com.swiftshopper.sdk.ui.view.SsLoginView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

public class LoginActivity extends BaseAppCompatActivity<LoginBinding> implements SsLoginView.Delegate {


    public static Intent getActivityIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setBackground();
        super.onCreate(savedInstanceState);
        binding.loginView.setLoginDelegate(this);
    }

    private void setBackground() {
        Window window = getWindow();
        if (window != null) {
            window.setBackgroundDrawableResource(R.drawable.bg_splash);
        }
    }

    // BEGIN: SsLoginView.Delegate Implementation
    @Override
    public void onLoginSuccess() {
        hideLoading();
        goToDashboardOrFinish();
    }

    @Override
    public void onLoginFailure(Throwable throwable) {
        if (throwable != null) {
            Toast.makeText(this, getString(R.string.toast_message_login_failed), Toast.LENGTH_SHORT).show();
        }
        hideLoading();
    }

    @Override
    public void proceedAnonymously(Runnable runMeToProceed) {

        SsUser ssUser = SwiftShopperSdk.getUser();
        // If user is already logged in anonymously, just let them skip (the user would not yet
        // be anonymous the first time that they ever arrive at the login screen, the idea is
        // that the else clause should only execute one time to nag the user about skipping
        // login)
        if (ssUser != null && ssUser.isAnonymous()) {
            runMeToProceed.run();
        } else {
            // Try to gently convince the user not to skip login
            //
            // Create an alert dialog that explains what features will be missing if the user skips
            // login and asks the user if they are sure.
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getTitle());
            builder.setMessage(R.string.login_skip_confirmation_message);
            // Reverse psychology: "Going Back" or "Cancelling" is illustrated as the positive choice
            builder.setPositiveButton(R.string.login_skip_confirmation_cancel, (d, i) -> {

            });
            builder.setNegativeButton(R.string.login_skip_confirmation_yes, (d, i) -> {
                // Proceed to skip the login since the user insists
                showLoading();
                runMeToProceed.run();
            });
            // Show the alert dialog
            builder.create().show();
        }
    }



    private boolean isForAction() {
        String action = getIntent().getAction();
        return action != null && (
                action.equals(SwiftShopperSdk.ACTION_LOGIN_FOR_PRIVATE_SHARE)
                        || action.equals(SwiftShopperSdk.ACTION_LOGIN_FOR_PUBLIC_SHARE)
                        || action.equals(StarterApp.ACTION_LOGIN_FOR_PROFILE)
                        || action.equals(SwiftShopperSdk.ACTION_LOGIN_TO_VIEW_SHARED_LIST));
    }

    private void goToDashboardOrFinish() {
        if (isForAction()) {
            finish();
        } else {
            launchActivity(DashboardActivity.getActivityIntent(LoginActivity.this, false), true);
        }
    }
}
