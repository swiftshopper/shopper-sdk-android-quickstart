package com.swiftshopper.sdk.starterapp.view.toolbar;

import android.content.Context;
import androidx.databinding.DataBindingUtil;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.swiftshopper.sdk.util.GlideUtils;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.databinding.LayoutCustomStarterAppToolbarBinding;

/**
 * Created by chintan.soni on 15/03/18.
 */

public class SampleAppToolbar extends ConstraintLayout {
    private ImageButton mIbLeft1;
    private ImageButton mIbLeft2;
    private ImageView mIvLogo;
    private TextView mTvTitle;
    private TextView mTvRight;

    private LayoutCustomStarterAppToolbarBinding layoutCustomToolbarBinding;

    public SampleAppToolbar(Context context) {
        this(context, null);
    }

    public SampleAppToolbar(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SampleAppToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    private void initialize(Context context) {
        layoutCustomToolbarBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.layout_custom_starter_app_toolbar, this, true);
        layoutCustomToolbarBinding.getRoot().setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

        mIbLeft1 = layoutCustomToolbarBinding.ibLayoutCustomToolbarLeft1;
        mIbLeft2 = layoutCustomToolbarBinding.ibLayoutCustomToolbarLeft2;
        mIvLogo = layoutCustomToolbarBinding.ivLayoutCustomToolbarLogo;
        mTvTitle = layoutCustomToolbarBinding.tvLayoutCustomToolbarTitle;
        mTvRight = layoutCustomToolbarBinding.tvLayoutCustomToolbarRight;
    }

    public SampleAppToolbar builder() {
        mIbLeft1.setVisibility(GONE);
        mIbLeft2.setVisibility(GONE);
        mIvLogo.setVisibility(GONE);
        mTvTitle.setVisibility(GONE);
        mTvRight.setVisibility(GONE);
        return this;
    }

    public SampleAppToolbar setLeft1(@DrawableRes int left1Resource, OnClickListener left1ClickListener) {
        if (left1Resource == 0) {
            mIbLeft1.setVisibility(View.GONE);
        } else {
            mIbLeft1.setVisibility(View.VISIBLE);
            mIbLeft1.setImageResource(left1Resource);
            mIbLeft1.setOnClickListener(left1ClickListener);
        }
        return this;
    }

    public SampleAppToolbar setLeft2(@DrawableRes int left2Resource, OnClickListener left2ClickListener) {
        if (left2Resource == 0) {
            mIbLeft2.setVisibility(View.GONE);
        } else {
            mIbLeft2.setVisibility(View.VISIBLE);
            mIbLeft2.setImageResource(left2Resource);
            mIbLeft2.setOnClickListener(left2ClickListener);
        }
        return this;
    }

    public SampleAppToolbar setLogo(int logo, OnClickListener logoClickListener) {
        mIvLogo.setVisibility(View.VISIBLE);
        GlideUtils.loadImageUrl(mIvLogo, logo, R.drawable.ic_logo_large);
        mIvLogo.setOnClickListener(logoClickListener);
        return this;
    }

    public SampleAppToolbar setTitle(String title, OnClickListener onClickListener) {
        if (TextUtils.isEmpty(title)) {
            mTvTitle.setVisibility(GONE);
        } else {
            mTvTitle.setVisibility(VISIBLE);
            mTvTitle.setText(title);
            mTvTitle.setOnClickListener(onClickListener);
        }
        return this;
    }

    public SampleAppToolbar setRightView(String rightResource, @DrawableRes int leftIcon, @DrawableRes int rightIcon,
                                         OnClickListener onRightClickListener) {
        if (TextUtils.isEmpty(rightResource) && (rightIcon == 0 && leftIcon == 0)) {
            mTvRight.setVisibility(GONE);
        } else {
            mTvRight.setVisibility(VISIBLE);
            mTvRight.setText(rightResource);
            mTvRight.setOnClickListener(onRightClickListener);
            mTvRight.setCompoundDrawablesWithIntrinsicBounds(leftIcon, 0, rightIcon, 0);
        }
        return this;
    }
}
