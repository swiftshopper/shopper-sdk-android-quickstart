package com.swiftshopper.sdk.starterapp.view.activity;


import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.StarterApp;
import com.swiftshopper.sdk.starterapp.base.BaseNavigationActivity;
import com.swiftshopper.sdk.starterapp.databinding.AccountSettingsAB;
import com.swiftshopper.sdk.starterapp.viewmodel.AccountSettingsVM;

import androidx.core.content.res.ResourcesCompat;

public class AccountSettingActivity extends BaseNavigationActivity<AccountSettingsAB, AccountSettingsVM> implements View.OnClickListener {

    public static final int RC_ACCOUNT_SETTINGS = 225;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, AccountSettingActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_account_settings;
    }

    @Override
    protected AccountSettingsVM createViewModel() {
        return new AccountSettingsVM();
    }

    @Override
    protected void setViewModel(AccountSettingsAB binding, AccountSettingsVM viewModel) {
        binding.setViewModel(viewModel);
        initViews();
    }

    private void initViews() {
        setToolbar();

        binding.tvAccountSettingsProfile.setOnClickListener(this);
        binding.swcScanner.setChecked(SwiftShopperSdk.isScannerGuideBarEnabled());
        binding.swcScanner.setOnCheckedChangeListener((compoundButton, enabled) ->
                SwiftShopperSdk.toggleScannerGuideBar(enabled));
        binding.swcScanner.setTypeface(ResourcesCompat.getFont(this, R.font.digital7));
    }

    private void setToolbar() {
        binding.customToolbar.builder()
                .setLeft1(R.drawable.ic_back, v -> back())
                .setLogo(R.drawable.ic_logo_small, v -> backToHome())
                .setTitle(getString(R.string.account_settings), null);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_account_settings_profile:
                if (isUserSocialLoggedIn()) {
                    launchActivity(ProfileActivity.getActivityIntent(this));
                } else {
                    ((StarterApp) getApplication()).launchLoginForAction(StarterApp.ACTION_LOGIN_FOR_PROFILE);
                }
                break;
            default:
                break;
        }
    }

    private boolean isUserSocialLoggedIn() {
        SsUser ssUser;
        return (ssUser=SwiftShopperSdk.getUser()) != null && !ssUser.isAnonymous();
    }
}
