package com.swiftshopper.sdk.starterapp.viewmodel;

import com.swiftshopper.sdk.starterapp.databinding.OrderHistoryBinding;

import androidx.databinding.BaseObservable;


public class OrderHistoryListVM extends BaseObservable {
    private OrderHistoryBinding mBinding;

    public void setBinding(OrderHistoryBinding binding) {
        this.mBinding = binding;
    }
}
