package com.swiftshopper.sdk.starterapp.base;

import androidx.databinding.ViewDataBinding;
import android.os.Bundle;

public abstract class BaseNavigationActivity<DATABINDING extends ViewDataBinding, VIEWMODEL> extends BaseAppCompatActivity<DATABINDING> {
    protected VIEWMODEL viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = createViewModel();
        setViewModel(binding, viewModel);
    }

    protected abstract VIEWMODEL createViewModel();

    protected abstract void setViewModel(DATABINDING binding, VIEWMODEL viewModel);
}
