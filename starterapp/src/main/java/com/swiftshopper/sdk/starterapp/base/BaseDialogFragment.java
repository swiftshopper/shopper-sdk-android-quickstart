package com.swiftshopper.sdk.starterapp.base;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseDialogFragment<DATABINDING extends ViewDataBinding, VIEWMODEL> extends AppCompatDialogFragment {
    protected Context context;
    protected DATABINDING binding;
    protected VIEWMODEL viewModel;
    protected BaseNavigationActivity mBaseNavigationActivity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = createViewModel();
        setViewModel(binding, viewModel);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.mBaseNavigationActivity = (BaseNavigationActivity) context;
    }

    protected abstract int getLayout();

    protected abstract VIEWMODEL createViewModel();

    protected abstract void setViewModel(DATABINDING binding, VIEWMODEL viewModel);

    public void launchActivity(Intent intent) {
        mBaseNavigationActivity.launchActivity(intent);
    }

    public void launchActivity(Intent intent, boolean finishCurrent) {
        mBaseNavigationActivity.launchActivity(intent, finishCurrent);
    }

    public void launchActivity(Intent intent, int requestCode) {
        mBaseNavigationActivity.launchActivity(intent, requestCode);
    }

    protected void showToast(String message) {
        mBaseNavigationActivity.showToast(message);
    }

    protected void finishWithResultOk() {
        mBaseNavigationActivity.finishWithResultOk();
    }

    protected void showDialogFragment(AppCompatDialogFragment targetAppCompatDialogFragment, AppCompatDialogFragment appCompatDialogFragment) {
        mBaseNavigationActivity.showDialogSubFragment(targetAppCompatDialogFragment, appCompatDialogFragment, mBaseNavigationActivity.DEFAULT_REQUEST_CODE);
    }
}
