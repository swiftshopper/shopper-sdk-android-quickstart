package com.swiftshopper.sdk.starterapp.view.fragment;

import android.view.View;

import com.swiftshopper.sdk.SsStoreDetail;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseFragment;
import com.swiftshopper.sdk.starterapp.databinding.DashboardFragmentBinding;
import com.swiftshopper.sdk.starterapp.view.activity.PlanActivity;
import com.swiftshopper.sdk.starterapp.view.activity.SelectMarketActivity;
import com.swiftshopper.sdk.starterapp.view.activity.ShopActivity;
import com.swiftshopper.sdk.starterapp.viewmodel.DashboardFragmentViewModel;

public class DashboardFragment extends BaseFragment<DashboardFragmentBinding, DashboardFragmentViewModel> implements View.OnClickListener {

    @Override
    protected int getLayout() {
        return R.layout.fragment_dashboard;
    }

    @Override
    protected DashboardFragmentViewModel createViewModel() {
        return new DashboardFragmentViewModel();
    }

    @Override
    protected void setViewModel(DashboardFragmentBinding binding, DashboardFragmentViewModel viewModel) {
        binding.setViewModel(viewModel);
    }

    @Override
    public void onResume() {
        super.onResume();
        initViews();
    }

    private void initViews() {
        SsStoreDetail ssStore = SwiftShopperSdk.getStore();
        binding.cvDashboardPlan.setOnClickListener(this);
        binding.cvDashboardShop.setOnClickListener(this);
        binding.tvDashboardPlan.setText(ssStore == null
                ? R.string.plan
                : R.string.shop_from_list
        );
        binding.tvDashboardShop.setText(ssStore == null
                ? R.string.shop
                : R.string.shop_on_the_fly
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cv_dashboard_plan:
                launchActivity(PlanActivity.getActivityIntent(context));
                break;
            case R.id.cv_dashboard_shop:
                if (SwiftShopperSdk.getStore() == null) {
                    launchActivity(SelectMarketActivity.getActivityIntent(context));
                } else {
                    launchActivity(ShopActivity.getActivityIntent(context));
                }
                break;
            default:
                break;
        }
    }
}
