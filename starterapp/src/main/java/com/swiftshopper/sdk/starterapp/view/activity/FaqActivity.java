package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;

import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseNavigationActivity;
import com.swiftshopper.sdk.starterapp.databinding.FaqAB;
import com.swiftshopper.sdk.starterapp.viewmodel.FaqVM;

/**
 * Created by dipak.vyas on 20/03/18.
 */

public class FaqActivity extends BaseNavigationActivity<FaqAB, FaqVM> {

    public static final int RC_FAQ = 223;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, FaqActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_faq;
    }

    @Override
    protected FaqVM createViewModel() {
        return new FaqVM();
    }

    @Override
    protected void setViewModel(FaqAB binding, FaqVM viewModel) {
        binding.setViewModel(viewModel);
        setToolbar();
    }

    private void setToolbar() {
        binding.customToolbar.builder()
                .setLeft1(R.drawable.ic_back, v -> back())
                .setLogo(R.drawable.ic_logo_small, v -> backToHome())
                .setTitle(getString(R.string.faq), null);
    }
}
