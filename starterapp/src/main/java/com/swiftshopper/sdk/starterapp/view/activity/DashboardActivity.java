package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.DashboardActivityBinding;
import com.swiftshopper.sdk.starterapp.util.ActivityUtils;
import com.swiftshopper.sdk.starterapp.util.DialogUtils;
import com.swiftshopper.sdk.starterapp.view.dialogfragment.ExitStoreDialogFragment;

public class DashboardActivity extends BaseAppCompatActivity<DashboardActivityBinding> implements AdapterView.OnItemClickListener, View.OnClickListener {

    public static final String EXTRA_SHARED_LIST_IMPORTED =
            "com.swiftshopper.sdk.starterapp.view.activity.DashboardActivity." +
                    "EXTRA_SHARED_LIST_IMPORTED";

    public static Intent getActivityIntent(Context context, boolean sharedListImported) {
        Intent intent = new Intent(context, DashboardActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (sharedListImported) {
            intent.putExtra(EXTRA_SHARED_LIST_IMPORTED, true);
        }
        return intent;
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_dashboard;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    private void initViews() {
        setupDrawer();
        checkIfDeepLinkAvailable();
    }

    private void checkIfDeepLinkAvailable() {
        Intent intentThatStartedThisActivity;
        if ((intentThatStartedThisActivity=getIntent()) != null &&
                intentThatStartedThisActivity.getBooleanExtra(
                        EXTRA_SHARED_LIST_IMPORTED, false)) {
            intentThatStartedThisActivity.removeExtra(EXTRA_SHARED_LIST_IMPORTED);
            setIntent(intentThatStartedThisActivity);
            launchActivity(PlanActivity.getActivityIntent(this));
        }
    }

    private void setupDrawer() {
        binding.ivDashboardDrawer.setOnClickListener(v -> openDrawer());
        binding.lvDashboard.setAdapter(new ArrayAdapter<>(this,
                R.layout.layout_list_item_side_menu, R.id.tv_list_item_side_menu,
                getResources().getStringArray(R.array.items_side_menu)));
        binding.lvDashboard.setOnItemClickListener(this);
        binding.ibSideMenuClose.setOnClickListener(this);
        binding.esvDashboard.setOnClickListener(this);
        binding.esvDashboard.setVisibility(
                SwiftShopperSdk.getStore() == null ? View.INVISIBLE : View.VISIBLE);
        int colorPrimary = ContextCompat.getColor(DashboardActivity.this, R.color.colorPrimary);
        binding.clSideMenu.setBackgroundColor(Color.argb(243,
                Color.red(colorPrimary), Color.green(colorPrimary), Color.blue(colorPrimary)));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position) {
            case 0:
                launchActivity(AccountSettingActivity.getActivityIntent(this), AccountSettingActivity.RC_ACCOUNT_SETTINGS);
                break;
            case 1:
                launchActivity(OrderHistoryActivity.getActivityIntent(this), OrderHistoryActivity.RC_ORDER_HISTORY);
                break;
            case 2:
                launchActivity(FaqActivity.getActivityIntent(this), FaqActivity.RC_FAQ);
                break;
            case 3:
                launchActivity(ContactUsActivity.getActivityIntent(this), ContactUsActivity.RC_CONTACT_US);
                break;
            case 4:
                launchActivity(AboutActivity.getActivityIntent(this), AboutActivity.RC_ABOUT);
                break;
            case 5:
                showRateThisAppDialog();
                break;
            default:
        }
        closeDrawer();
    }

    private void showRateThisAppDialog() {
        DialogUtils.confirm(this,
                R.string.rate_this_app,
                R.string.if_you_enjoy,
                R.string.rate_now,
                (dialog, which) -> ActivityUtils.openAppInPlayStore(this),
                R.string.later,
                (dialog, which) -> {

                }
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ib_side_menu_close:
                closeDrawer();
                break;
            case R.id.esv_dashboard:
                cancelCartClick();
                break;
            default:
        }
    }

    private void closeDrawer() {
        binding.drawerLayout.closeDrawer(Gravity.START);
    }

    private void openDrawer() {
        binding.drawerLayout.openDrawer(Gravity.START);
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(Gravity.START)) {
            binding.drawerLayout.closeDrawers();
        } else {
            if (SwiftShopperSdk.getStore() == null) {
                super.onBackPressed();
            } else {
                cancelCartClick();
            }
        }
    }

    public void cancelCartClick() {
        showDialogFragment(ExitStoreDialogFragment.getDialogFragment());
    }
}
