package com.swiftshopper.sdk.starterapp.base;

import android.content.Context;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public abstract class BasePagerAdapter<T> extends PagerAdapter {
    private static final String TAG = BasePagerAdapter.class.getSimpleName();
    private Context mContext;
    private List<T> mList = new ArrayList<>();

    public BasePagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void setItems(List<T> list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(mContext).inflate(getViewType(position), container, false);
        return bindDataAndReturnView(container, view, getItemAtPosition(position), getViewType(position), position);
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    public abstract int getViewType(int position);

    public abstract View bindDataAndReturnView(ViewGroup container, View view, T model, int viewType, int position);

    public T getItemAtPosition(int position) {
        return mList.get(position);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public List<T> getList() {
        return mList;
    }
}
