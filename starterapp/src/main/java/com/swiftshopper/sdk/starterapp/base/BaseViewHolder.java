package com.swiftshopper.sdk.starterapp.base;

import android.content.Intent;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.swiftshopper.sdk.starterapp.BR;


public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    public ViewDataBinding binding;
    protected Object viewModel;
    protected Object model;
    protected BaseNavigationActivity baseNavigationActivity;

    public BaseViewHolder(ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
        baseNavigationActivity = (BaseNavigationActivity) this.binding.getRoot().getContext();
    }

    public Object getViewModel() {
        return viewModel;
    }

    public Object getModel() {
        return model;
    }

    public void bind(int position, Object viewModel) {
        bind(position, viewModel, null);
    }

    public void bind(int position, Object viewModel, Object obj) {
        this.viewModel = viewModel;
        binding.setVariable(BR.viewModel, viewModel);
        if (obj != null) {
            this.model = obj;
            binding.setVariable(BR.obj, obj);
        }
        onBeforeBind(viewModel);
        binding.executePendingBindings();
        onBind(position, viewModel, obj);
    }

    protected void onBeforeBind(Object viewModel) {

    }

    protected void launchActivity(Intent intent) {
        baseNavigationActivity.launchActivity(intent);
    }

    protected void showDialogFragment(AppCompatDialogFragment appCompatDialogFragment) {
        baseNavigationActivity.showDialogFragment(appCompatDialogFragment);
    }

    protected void showDialogFragment(Fragment fragment, AppCompatDialogFragment appCompatDialogFragment) {
        baseNavigationActivity.showDialogFragment(fragment, appCompatDialogFragment, baseNavigationActivity.DEFAULT_REQUEST_CODE);
    }

    protected void showDialogFragment(AppCompatDialogFragment targetAppCompatDialogFragment,
                                      AppCompatDialogFragment appCompatDialogFragment) {
        baseNavigationActivity.showDialogFragment(targetAppCompatDialogFragment,
                appCompatDialogFragment, baseNavigationActivity.DEFAULT_REQUEST_CODE);
    }

    protected Fragment getFragmentFromActivity(Class t) {
        return baseNavigationActivity.getSupportFragmentManager().findFragmentByTag(t.getSimpleName());
    }

    protected abstract void onBind(int position, Object viewModel, Object obj);
}
