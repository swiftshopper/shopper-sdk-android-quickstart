package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;

import com.swiftshopper.sdk.base.BaseView;
import com.swiftshopper.sdk.ui.view.SsDealsView;
import com.swiftshopper.sdk.ui.view.SsShopOnTheFlyView;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.ShopActivityBinding;

public class ShopActivity extends BaseAppCompatActivity<ShopActivityBinding> implements View.OnClickListener {
    private BaseView mBaseView;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, ShopActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_shop;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initializeBottomTabs();
        loadShopOnFlyView();
    }

    private void initToolbar() {
        binding.ivLeft.setOnClickListener(this);
        binding.ivLogo.setOnClickListener(this);
        binding.cbvShop.setOnClickListener(this);
    }

    private void initializeBottomTabs() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_deals:
                    loadDealsView();
                    return true;
                case R.id.navigation_go_shop:
                    loadShopOnFlyView();
                    return true;
                default:
                    return false;
            }
        });
        setBottomTabsForPlan();
    }

    public void setBottomTabsForPlan() {
        binding.bottomNavigation.setSelectedItemId(R.id.navigation_go_shop);
        binding.bottomNavigation.getMenu().removeItem(R.id.navigation_lists);
        binding.bottomNavigation.getMenu().findItem(R.id.navigation_deals).setTitle(R.string.tab_in_store_deals);
    }

    private void loadDealsView() {
        binding.container.removeAllViews();
        mBaseView = new SsDealsView(this);
        binding.container.addView(mBaseView);
    }

    private void loadShopOnFlyView() {
        binding.container.removeAllViews();
        SsShopOnTheFlyView ssShopOnTheFlyView = new SsShopOnTheFlyView(this);
        // TODO: Remove these customizations -- they are just for testing loyalty development
        ssShopOnTheFlyView.setShowLoyaltyAlways(true);
        ssShopOnTheFlyView.setShowLoyaltyAddDelete(true);

        //mBaseView = new SsShopOnTheFlyView(this);
        mBaseView = ssShopOnTheFlyView;

        binding.container.addView(mBaseView);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_left) {
            onBackPressed();
        } else if (v.getId() == R.id.iv_logo) {
            backToHome();
        } else if (v.getId() == R.id.cbv_shop) {
            launchActivity(CartActivity.getActivityIntent(this));
        }
    }

    @Override
    public void onBackPressed() {
        if (mBaseView instanceof SsDealsView) {
            binding.bottomNavigation.setSelectedItemId(R.id.navigation_go_shop);
        } else {
            super.onBackPressed();
        }
    }
}
