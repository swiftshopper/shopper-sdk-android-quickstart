package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;

import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseNavigationActivity;
import com.swiftshopper.sdk.starterapp.databinding.OrderHistoryBinding;
import com.swiftshopper.sdk.starterapp.viewmodel.OrderHistoryListVM;


public class OrderHistoryActivity extends BaseNavigationActivity<OrderHistoryBinding,OrderHistoryListVM> {

    public static final int RC_ORDER_HISTORY = 235;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, OrderHistoryActivity.class);
    }

    @Override
    protected OrderHistoryListVM createViewModel() {
        return new OrderHistoryListVM();
    }

    @Override
    protected void setViewModel(OrderHistoryBinding binding, OrderHistoryListVM viewModel) {
        setToolbar();
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_order_history;
    }

    private void setToolbar() {
        binding.customToolbar.builder()
                .setLeft1(R.drawable.ic_back, v -> back())
                .setLogo(R.drawable.ic_logo_small, v -> backToHome())
                .setTitle(getString(R.string.order_history), null);
    }
}
