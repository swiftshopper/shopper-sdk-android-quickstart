package com.swiftshopper.sdk.starterapp.util;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.swiftshopper.sdk.SwiftShopperSdk;

import androidx.annotation.ColorInt;

public class ColorUtils {

    public static int getStatusBarColor(@ColorInt int color) {
        return darker(color, 0.7f);
    }

    public static int getTabIndicatorColor(@ColorInt int color) {
        return lighter(color, 0.7f);
    }

    public static boolean isLightBackground(View v) {
        boolean retVal = false;
        if (v != null) {
            Drawable background = v.getBackground();
            if (background instanceof ColorDrawable) {
                ColorDrawable colorBackground = (ColorDrawable)background;
                retVal = isLight(colorBackground.getColor());
            }

        }
        return retVal;
    }

    public static boolean isLight(@ColorInt int rawColor) {
        double luminance =
                androidx.core.graphics.ColorUtils.calculateLuminance(rawColor);
        return luminance > 0.79999;
    }

    public static int getConverseThemeColor() {
        int themeColor = SwiftShopperSdk.getThemeColor();
        return getConverseColor(themeColor);
    }

    public static int getConverseColor(@ColorInt int color) {
        if (isLight(color)) {
            return darker(color, 0.3f);
        } else {
            return lighter(color, 07f);
        }
    }


    /**
     * Returns darker version of specified <code>color</code>.
     *
     * @param color  The color to darken
     * @param factor The factor to darken the color. 0 will make the color completely black. 1 will
     *               leave the color unchanged.
     * @return darker version of the specified color.
     */
    private static int darker(@ColorInt int color, float factor) {
        if (factor > 1) {
            factor = 1f;
        }
        if (factor < 0) {
            factor = 0f;
        }
        int a = Color.alpha(color);
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);

        int invertedR = Math.max((int) (r * factor), 0);
        int invertedG = Math.max((int) (g * factor), 0);
        int invertedB = Math.max((int) (b * factor), 0);


        return Color.argb(a, invertedR, invertedG, invertedB);
    }

    public static boolean isThemeColorLight() {
        return isLight(SwiftShopperSdk.getThemeColor());
    }

    /**
     * Lightens a color by a given factor.
     *
     * @param color  The color to lighten
     * @param factor The factor to lighten the color. 0 will make the color unchanged. 1 will make the
     *               color white.
     * @return lighter version of the specified color.
     */
    private static int lighter(@ColorInt int color, float factor) {
        if (factor > 1) {
            factor = 1f;
        }
        if (factor < 0) {
            factor = 0f;
        }
        int red = (int) ((Color.red(color) * (1 - factor) / 255 + factor) * 255);
        int green = (int) ((Color.green(color) * (1 - factor) / 255 + factor) * 255);
        int blue = (int) ((Color.blue(color) * (1 - factor) / 255 + factor) * 255);
        return Color.argb(Color.alpha(color), red, green, blue);
    }

    public static void tintTextView(TextView tv, @ColorInt int tintColor) {
        Drawable[] cartButtonDrawables = tv.getCompoundDrawablesRelative();


        for (Drawable drawable: cartButtonDrawables) {
            if (drawable != null) {
                drawable.mutate().setTint(tintColor);
            }
        }

        tv.setTextColor(tintColor);
    }

    public static Bitmap replaceGreen(Bitmap src, @ColorInt int targetColor) {
        if(src == null) {
            return null;
        }
        // Source image size
        int width = src.getWidth();
        int height = src.getHeight();
        int[] pixels = new int[width * height];
        //get pixels
        src.getPixels(pixels, 0, width, 0, 0, width, height);

        for(int x = 0; x < pixels.length; ++x) {
            int red = Color.red(pixels[x]);
            int green = Color.green(pixels[x]);
            int blue = Color.blue(pixels[x]);
            if (green > red && green > blue) {
                pixels[x] = targetColor;
            }
        }
        // create result bitmap output
        Bitmap result = Bitmap.createBitmap(width, height, src.getConfig());
        //set pixels
        result.setPixels(pixels, 0, width, 0, 0, width, height);

        return result;
    }

    public static void replaceGreen(ImageView imageView, @ColorInt int toColor) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable)imageView.getDrawable();
        Bitmap bitmap = bitmapDrawable.getBitmap();
        Bitmap result = replaceGreen(bitmap, toColor);
        imageView.setImageBitmap(result);
    }

//    public static boolean isDark(Bitmap bitmap){
//        boolean dark=false;
//
//        float darkThreshold = bitmap.getWidth()*bitmap.getHeight()*0.45f;
//        int darkPixels=0;
//
//        int[] pixels = new int[bitmap.getWidth()*bitmap.getHeight()];
//        bitmap.getPixels(pixels,0,bitmap.getWidth(),0,0,bitmap.getWidth(),bitmap.getHeight());
//
//        for(int color : pixels){
//            int r = Color.red(color);
//            int g = Color.green(color);
//            int b = Color.blue(color);
//            double luminance = (0.299*r+0.0f + 0.587*g+0.0f + 0.114*b+0.0f);
//            if (luminance<150) {
//                darkPixels++;
//            }
//        }
//
//        if (darkPixels >= darkThreshold) {
//            dark = true;
//        }
//        return dark;
//    }
//
//    public static Boolean isLightBackground(ViewParent viewParent) {
//        if (viewParent == null || !(viewParent instanceof View)) {
//            return null;
//        } else {
//            return isLightBackground((View)viewParent);
//        }
//    }

//    public static Boolean isLightBackground(View v) {
//        if (v==null) {
//            return null;
//        }
//        v.
//
//        Drawable background = v.getBackground();
//        if (background == null) {
//            return isLightBackground(v.getParent());
//        } else {
//            Boolean isLightBackground = isLightBackground(background);
//            if (isLightBackground == null) {
//                return isLightBackground(v.getParent());
//            } else {
//                return isLightBackground;
//            }
//        }
//    }
//
//    public static Bitmap drawableToBitmap (Drawable drawable) {
//        Bitmap bitmap = null;
//
//        if (drawable instanceof BitmapDrawable) {
//            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
//            if(bitmapDrawable.getBitmap() != null) {
//                return bitmapDrawable.getBitmap();
//            }
//        }
//
//        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
//            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
//        } else {
//            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
//        }
//
//        Canvas canvas = new Canvas(bitmap);
//        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
//        drawable.draw(canvas);
//        return bitmap;
//    }
//
//    public static Boolean isLightBackground(Drawable d) {
//        if (d == null) {
//            return null;
//        } else {
//            Bitmap bm = drawableToBitmap(d);
//            return !isDark(bm);
//        }
//    }
//
//    @ColorInt
//    public static Integer resolveBackgroundColor(View v) {
//        if (v==null) {
//            return null;
//        }
//
//        Drawable background = v.getBackground();
//        if (background == null) {
//            ViewParent viewParent = v.getParent();
//            if (viewParent != null && viewParent instanceof View) {
//                return resolveBackgroundColor((View)viewParent);
//            } else {
//                return null;
//            }
//        } else {
//            if (background instanceof LayerDrawable) {
//                Integer layeredColor = resolveBackgroundColor(background);
//                if (layeredColor == null) {
//                    ViewParent viewParent = v.getParent();
//                    if (viewParent != null && viewParent instanceof View) {
//                        return resolveBackgroundColor((View)viewParent);
//                    } else {
//                        return null;
//                    }
//                } else {
//                    return layeredColor;
//                }
//            } else if (background instanceof ColorDrawable) {
//                return ((ColorDrawable)background).getColor();
//            } else {
//                return null;
//            }
//        }
//    }
//
//    @ColorInt
//    public static Integer resolveBackgroundColor(Drawable d) {
//        if (d == null) {
//            return null;
//        } else if (d instanceof ColorDrawable) {
//            return ((ColorDrawable)d).getColor();
//        } else if (d instanceof LayerDrawable) {
//            LayerDrawable ld = (LayerDrawable)d;
//            int nbrLayers = ld.getNumberOfLayers();
//            for (int i=0; i<nbrLayers; i++) {
//                Drawable innerDrawable = ld.getDrawable(i);
//                Integer innerDrawableBackgroundColor = resolveBackgroundColor(innerDrawable);
//                if (innerDrawableBackgroundColor != null) {
//                    return innerDrawableBackgroundColor;
//                }
//            }
//            return null;
//        }
//        return null;
//    }

}
