package com.swiftshopper.sdk.starterapp.view.dialogfragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.databinding.ExitStoreDialogBinding;
import com.swiftshopper.sdk.starterapp.view.activity.DashboardActivity;
import com.swiftshopper.sdk.starterapp.viewmodel.ExitStoreDialogViewModel;

public class ExitStoreDialogFragment extends BaseDialogFragment<ExitStoreDialogBinding, ExitStoreDialogViewModel>
        implements View.OnClickListener, SwiftShopperSdk.ExitStoreListener {

    public static ExitStoreDialogFragment getDialogFragment() {
        return new ExitStoreDialogFragment();
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_fragment_exit_store;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        Window window = dialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        return dialog;
    }

    @Override
    protected ExitStoreDialogViewModel createViewModel() {
        return new ExitStoreDialogViewModel();
    }

    @Override
    protected void setViewModel(ExitStoreDialogBinding binding, ExitStoreDialogViewModel viewModel) {
        initViews();
    }

    private void initViews() {
        binding.ivExitStore.setImageResource(R.drawable.ic_logo_small);
        binding.btnNo.setOnClickListener(this);
        binding.btnYes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_yes) {
            SwiftShopperSdk.exitStore(this);
        } else if (v.getId() == R.id.btn_no) {
            dismiss();
        }
    }

    @Override
    public void onExitStore() {
        startActivity(DashboardActivity.getActivityIntent(mContext, false));
    }
}