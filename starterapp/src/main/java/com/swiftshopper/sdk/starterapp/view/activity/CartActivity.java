package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.CartActivityBinding;
import com.swiftshopper.sdk.starterapp.view.dialogfragment.ExitStoreDialogFragment;

import androidx.annotation.Nullable;

public class CartActivity extends BaseAppCompatActivity<CartActivityBinding> implements View.OnClickListener {

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, CartActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_cart;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar();
    }

    private void setToolbar() {
        binding.ivLeft.setOnClickListener(this);
        binding.ivLogo.setOnClickListener(this);

        binding.esvDashboard.setVisibility(SwiftShopperSdk.getStore() == null ? View.GONE : View.VISIBLE);
        binding.esvDashboard.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_left) {
            onBackPressed();
        } else if (v.getId() == R.id.iv_logo) {
            backToHome();
        } else if (v.getId() == R.id.esv_dashboard) {
            cancelCartClick();
        }
    }

    private void cancelCartClick() {
        showDialogFragment(ExitStoreDialogFragment.getDialogFragment());
    }
}
