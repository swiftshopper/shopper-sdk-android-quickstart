package com.swiftshopper.sdk.starterapp.util.branchio;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;

import java.util.HashMap;

import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

public class BranchIOUtil {
    public static String feature_Share = "Share";
    public static String tag_Android = "Android";
    public static String campaign_SwiftShopper = "SwiftShopper";
    public static String param_user_id = "user_id";
    public static String param_list_id = "list_id";
    public static String param_link_type = "link_type";
    public static String param_type = "type";

    private static String LINK_TYPE_ANDROID = "1";

    public static void initialize(Application application) {
        // Branch logging for debugging
        Branch.enableLogging();

        // Branch object initialization
        Branch.getAutoInstance(application);
    }

    public static void initSession(AppCompatActivity appCompatActivity, Intent intent, OnBranchListener onBranchListener) {
        Branch.getInstance().initSession((branchUniversalObject, linkProperties, error) -> {
            boolean sharedListImported = false;
            if (error == null) {
                String listId = "";
                String type = "";
                if (branchUniversalObject != null && branchUniversalObject.getContentMetadata() != null) {
                    HashMap<String, String> customMetaData = branchUniversalObject.getContentMetadata().getCustomMetadata();
                    if (customMetaData.containsKey(BranchIOUtil.param_list_id)) {
                        listId = customMetaData.get(BranchIOUtil.param_list_id);
                    }
                    if (customMetaData.containsKey(BranchIOUtil.param_type)) {
                        type = customMetaData.get(BranchIOUtil.param_type);
                    }

                    try {
                        SwiftShopperSdk.importSharedList(listId, type);
                        sharedListImported = true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Branch.getInstance().resetUserSession();
                }
            }
            onBranchListener.onComplete(sharedListImported);
        }, intent.getData(), appCompatActivity);
    }

    public static void generateDeepLink(Context context, String listId, String shareType, OnDeepLinkListener onDeepLinkListener) {
        LinkProperties linkProperties = new LinkProperties()
                .setFeature(BranchIOUtil.feature_Share)
                .addTag(BranchIOUtil.tag_Android)
                .setCampaign(BranchIOUtil.campaign_SwiftShopper)
                .addControlParameter(BranchIOUtil.param_list_id, listId)
                .addControlParameter(BranchIOUtil.param_type, shareType)
                .addControlParameter(BranchIOUtil.param_link_type, LINK_TYPE_ANDROID)
                .addControlParameter("$android_url", "https://play.google.com/store/apps/details?id=" + context.getPackageName())
                .addControlParameter("$ios_url", "custom/path/*")
                .addControlParameter("$desktop_url", "http://swiftshopper.com")
                .addControlParameter("$uri_redirect_mode", LINK_TYPE_ANDROID);

        BranchUniversalObject branchUniversalObject = new BranchUniversalObject();
        branchUniversalObject.generateShortUrl(context, linkProperties, (s, branchError) -> {
            if (branchError == null) {
                onDeepLinkListener.onDeepLinkSuccess(s);
            } else {
                onDeepLinkListener.onDeepLinkError(branchError);
            }
        });
    }

    public static void shareDeepLink(Context context, String link) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, context.getString(R.string.app_name) + " Share List");
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey checkout my list with " + context.getString(R.string.app_name) + "! \n" + link);
        sendIntent.setType("text/plain");
        Intent intent = Intent.createChooser(sendIntent, "Share Via");
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public interface OnBranchListener {
        void onComplete(boolean sharedListImported);
    }

    public interface OnDeepLinkListener {
        void onDeepLinkSuccess(String link);

        void onDeepLinkError(BranchError branchError);
    }
}
