package com.swiftshopper.sdk.starterapp.model;

import androidx.annotation.ArrayRes;
import androidx.annotation.DrawableRes;

public class IntroductionModel {

    private @DrawableRes
    int image;

    private @ArrayRes
    int points;

    public IntroductionModel(@DrawableRes int image, @ArrayRes int points) {
        this.image = image;
        this.points = points;
    }

    public int getImage() {
        return image;
    }

    public int getPoints() {
        return points;
    }
}
