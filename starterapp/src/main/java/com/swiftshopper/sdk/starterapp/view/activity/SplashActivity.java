package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.util.branchio.BranchIOUtil;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class SplashActivity extends BaseAppCompatActivity {

    private static final int SPLASH_TIMEOUT_IN_MILLISECONDS = 2000;
    private Disposable splashDisposable;
    private Disposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();

        BranchIOUtil.initSession(
                SplashActivity.this, getIntent(), (intentHasAppLink) -> {
                    if (intentHasAppLink) {
                        // We need to import the shared list for the app link, however, the
                        // user might not be logged in (Swift Shopper requires that a user
                        // be logged in non-anonymously to access shared lists). Moreover, the user
                        // may have just installed the app.



                    }
                 });
        parseDeepLink(getIntent());
    }

    private void parseDeepLink(Intent intent) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        disposable = createBranchIOObserver(intent)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(sharedListImported -> {
                    goToNextActivity(sharedListImported);
                }, (t)->{
                    Timber.w(t, "Branch Comm Issue During Startup");
                    goToNextActivity(false);
                });
    }

    private Observable<Boolean> createBranchIOObserver(Intent intent) {
        return Observable.create(emitter -> BranchIOUtil.initSession(
                SplashActivity.this, intent, (sharedListImported) -> {
            emitter.onNext(sharedListImported);
            emitter.onComplete();
        }));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        if (splashDisposable != null && !splashDisposable.isDisposed()) {
            splashDisposable.dispose();
        }
    }

    private void goToNextActivity(boolean sharedListImported) {
        if (splashDisposable != null && !splashDisposable.isDisposed()) {
            splashDisposable.dispose();
        }
        splashDisposable = Observable.just("")
                .subscribeOn(Schedulers.io())
                .delay(SPLASH_TIMEOUT_IN_MILLISECONDS, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(delay -> callActivity(sharedListImported), Throwable::printStackTrace);
    }

    private void callActivity(boolean sharedListImported) {
        if (SwiftShopperSdk.getUser() == null) {
            launchActivity(OnBoardingActivity.getActivityIntent(this), true);
        } else {
            launchActivity(DashboardActivity.getActivityIntent(
                    this, sharedListImported), true);
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    private void initViews() {
        setSplashLogo();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.setIntent(intent);
    }

    private void setSplashLogo() {
        ((ImageView)findViewById(R.id.iv_splash)).setImageResource(R.drawable.ic_logo_large);
    }
}
