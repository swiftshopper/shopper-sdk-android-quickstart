package com.swiftshopper.sdk.starterapp;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Base64;

import androidx.core.content.ContextCompat;
import timber.log.Timber;

import com.facebook.FacebookSdk;
import com.swiftshopper.sdk.SsConfig;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.util.branchio.BranchIOUtil;
import com.swiftshopper.sdk.starterapp.view.activity.DashboardActivity;
import com.swiftshopper.sdk.starterapp.view.activity.LoginActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * Created by Simform Solution on 09/07/18.
 */
public class StarterApp extends Application {

    public static final String ACTION_LOGIN_FOR_PROFILE =
            "com.swiftshopper.sdk.starterapp.ACTION_LOGIN_FOR_PROFILE";

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                switch (intent.getAction()) {
                    case SwiftShopperSdk.ACTION_LOGIN_FOR_PUBLIC_SHARE:
                        launchLoginForAction(SwiftShopperSdk.ACTION_LOGIN_FOR_PUBLIC_SHARE);
                        break;
                    case SwiftShopperSdk.ACTION_LOGIN_FOR_PRIVATE_SHARE:
                        launchLoginForAction(SwiftShopperSdk.ACTION_LOGIN_FOR_PRIVATE_SHARE);
                        break;
                    case ACTION_LOGIN_FOR_PROFILE:
                        launchLoginForAction(ACTION_LOGIN_FOR_PROFILE);
                        break;
                    case SwiftShopperSdk.ACTION_LOGIN_TO_VIEW_SHARED_LIST:
                        launchLoginForAction(SwiftShopperSdk.ACTION_LOGIN_TO_VIEW_SHARED_LIST);
                        break;
                    case SwiftShopperSdk.ACTION_CHECKOUT_COMPLETE:
                        launchDashboard();
                        break;
                }
            }
        }
    };

    private void launchDashboard() {
        startActivity(DashboardActivity.getActivityIntent(this, false));
    }

    public void launchLoginForAction(String action) {
        Intent intent = new Intent(action);
        intent.setClass(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initSwiftShopperSDK();
        initializeBranchIO();
        registerListener();
        FacebookSdk.setIsDebugEnabled(true);
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                System.out.println("printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Timber.e(e, "printHashKey()");
        } catch (Exception e) {
            Timber.e(e, "printHashKey()");
        }
    }

    @Override
    public void onTerminate() {
        unregisterReceiver(mBroadcastReceiver);
        super.onTerminate();
    }


    private void initializeBranchIO() {
        BranchIOUtil.initialize(this);
    }

    private void initSwiftShopperSDK() {
        SsConfig config = new SsConfig.Builder(this)
                .setRetailerSdkKey("w9mn{dn7lPIF37Yb#>fh)By^Q|AoA3w8<SGXbd2gmPF2|")
                //.setRetailerSdkKey("2XEL$Qln:6]JEfa:shp%nc#s@bxb;78Zn,L7&u?vM#s:Z")
                .setThemeColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setActivityTheme(R.style.AppTheme_NoActionBar)
                .setLargeLogo(R.drawable.ic_logo_large)
                .setSmallLogo(R.drawable.ic_logo_small)
                .build();

        SwiftShopperSdk.initialize(config);
    }

    private void registerListener() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SwiftShopperSdk.ACTION_LOGIN_FOR_PUBLIC_SHARE);
        intentFilter.addAction(SwiftShopperSdk.ACTION_LOGIN_TO_VIEW_SHARED_LIST);
        intentFilter.addAction(SwiftShopperSdk.ACTION_CHECKOUT_COMPLETE);
        this.registerReceiver(mBroadcastReceiver, intentFilter);
    }
}
