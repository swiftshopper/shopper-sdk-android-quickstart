package com.swiftshopper.sdk.starterapp.base;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by chintan.soni on 20/07/17.
 */

public class BaseRecyclerAdapter<T, U extends BaseViewHolder, V extends BaseViewHolder> extends RecyclerView.Adapter<BaseViewHolder> {
    private static final int VIEW_TYPE_EMPTY = 0;
    private static final int VIEW_TYPE_DATA = 1;

    private List<T> mList = new ArrayList<>();

    @LayoutRes
    private int mEmptyViewLayoutResource;
    private Class<U> mEmptyViewHolder;

    @LayoutRes
    private int mDataLayoutResource;
    private Class<V> mDataViewHolder;
    private Object mViewModel;

    public BaseRecyclerAdapter(Object viewModel, int emptyViewLayoutResource,
                               Class<U> emptyViewHolder, int dataLayoutResource, Class<V> dataViewHolder) {
        this.mViewModel = viewModel;
        this.mEmptyViewLayoutResource = emptyViewLayoutResource;
        this.mEmptyViewHolder = emptyViewHolder;
        this.mDataLayoutResource = dataLayoutResource;
        this.mDataViewHolder = dataViewHolder;
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            BaseViewHolder baseViewHolder;
            ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                    viewType == VIEW_TYPE_EMPTY ? mEmptyViewLayoutResource
                            : mDataLayoutResource, parent, false);
            baseViewHolder = viewType == VIEW_TYPE_EMPTY
                    ? mEmptyViewHolder.getConstructor(ViewDataBinding.class).newInstance(binding)
                    : mDataViewHolder.getConstructor(ViewDataBinding.class).newInstance(binding);
            return baseViewHolder;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if (viewType == VIEW_TYPE_EMPTY) {
            holder.bind(position, mViewModel);
            customEmptyBind(holder, mViewModel);
        } else {
            holder.bind(position, mViewModel, mList.get(position));
            customBind(holder, position, mList.get(position), mViewModel);
        }
    }

    protected void customEmptyBind(BaseViewHolder holder, Object viewModel) {

    }

    protected void customBind(BaseViewHolder holder, int position, T model, Object viewModel) {

    }

    @Override
    public int getItemCount() {
        return mList.size() == 0 ? 1 : mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mList.size() == 0 ? VIEW_TYPE_EMPTY : VIEW_TYPE_DATA;
    }

    public int getListSize() {
        return mList.size();
    }

    public void setList(List<T> itemList) {
        this.mList = itemList;
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        if (position > -1 && position < mList.size()) {
            mList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addItem(T data, int position) {
        if (position <= mList.size()) {
            mList.add(position, data);
            notifyItemInserted(position);
        }
    }

    public void addItem(T data) {
        //addItem(data, mList.size());
        mList = new ArrayList<>();
        mList.add(data);
        notifyDataSetChanged();
    }

    public void clearItems() {
        int size = mList.size();
        if (size > 0) {
            mList.clear();
            notifyItemRangeRemoved(0, size);
        }
    }

    private void addItems(List<T> data, int position) {
        if (position <= mList.size() && data != null && data.size() > 0) {
            mList.addAll(position, data);
            notifyItemRangeChanged(position, data.size());
        }
    }

    public void addItems(List<T> data) {
        addItems(data, mList.size());
    }

    public List<T> getItems() {
        return mList;
    }
}
