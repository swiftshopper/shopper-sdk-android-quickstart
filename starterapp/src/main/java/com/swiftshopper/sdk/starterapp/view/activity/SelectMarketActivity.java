package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;

import com.swiftshopper.sdk.ui.view.SsSelectMarketView;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.SelectMarketActivityBinding;

public class SelectMarketActivity extends BaseAppCompatActivity<SelectMarketActivityBinding> implements SsSelectMarketView.CheckInListener, View.OnClickListener {

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, SelectMarketActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initViews();
    }

    private void initViews() {
        initToolbar();
        binding.selectMarketView.setCheckInListener(this);
    }

    private void initToolbar() {
        binding.ivLeft.setOnClickListener(this);
        binding.ivLogo.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_select_market;
    }

    @Override
    public void onCheckInComplete() {
        launchActivity(DashboardActivity.getActivityIntent(this, false));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_left) {
            onBackPressed();
        } else if (v.getId() == R.id.iv_logo) {
            backToHome();
        }
    }
}
