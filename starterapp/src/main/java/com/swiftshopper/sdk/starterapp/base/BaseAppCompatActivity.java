package com.swiftshopper.sdk.starterapp.base;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.swiftshopper.sdk.starterapp.util.ColorUtils;
import com.swiftshopper.sdk.util.LoadingUtil;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.util.NetworkUtils;
import com.swiftshopper.sdk.starterapp.view.activity.DashboardActivity;

public abstract class BaseAppCompatActivity<DATABINDING extends ViewDataBinding> extends AppCompatActivity {
    private static final int FRAGMENT_TRANSACTION_ADD = 200;
    private static final int FRAGMENT_TRANSACTION_REPLACE = 300;
    public int DEFAULT_REQUEST_CODE = 100;
    protected DATABINDING binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayout());
        LoadingUtil.setLoadingAnimation(this, binding, ContextCompat.getColor(this, R.color.colorPrimary));
    }

    @Override
    protected void onResume() {
        super.onResume();
        setStatusBarAndNavigationBar();
    }

    private void setStatusBarAndNavigationBar() {
        Window window = getWindow();
        if (window != null) {
            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ColorUtils.getStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary)));
            window.setNavigationBarColor(ColorUtils.getStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary)));
        }
    }

    protected abstract int getLayout();

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected void showLoading() {
        View view = findViewById(R.id.relative_layout_progress);
        if (view != null) {
            view.setVisibility(View.VISIBLE);
        }
    }

    protected void hideLoading() {
        View view = findViewById(R.id.relative_layout_progress);
        if (view != null) {
            view.setVisibility(View.GONE);
        }
    }

    public void showToast(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void launchActivity(Intent intent) {
        launchActivity(intent, false);
    }

    public void launchActivity(Intent intent, boolean finishCurrent) {
        launchActivity(intent, DEFAULT_REQUEST_CODE, finishCurrent);
    }

    public void launchActivity(Intent intent, int requestCode) {
        launchActivity(intent, requestCode, false);
    }

    public void launchActivity(Intent intent, int requestCode, boolean finishCurrent) {
        if (requestCode != DEFAULT_REQUEST_CODE) {
            startActivityForResult(intent, requestCode);
        } else {
            if (finishCurrent) {
                finish();
            }
            startActivity(intent);
        }
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment, false);
    }

    public void addFragment(Fragment fragment, boolean addToBackStack) {
        loadFragment(fragment, FRAGMENT_TRANSACTION_ADD, addToBackStack);
    }

    public void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, false);
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        loadFragment(fragment, FRAGMENT_TRANSACTION_REPLACE, addToBackStack);
    }

    private void loadFragment(Fragment fragment, int transactionType) {
        loadFragment(fragment, transactionType, false);
    }

    private void loadFragment(Fragment fragment, int transactionType, boolean addToBackStack) {
        loadFragment(fragment, R.id.frameLayout, transactionType, addToBackStack);
    }

    private void loadFragment(Fragment fragment, int containerId, int transactionType, boolean addToBackStack) {
        String fragmentName = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragmentName);
        }
        if (transactionType == FRAGMENT_TRANSACTION_ADD) {
            fragmentTransaction.add(containerId, fragment, fragmentName);
        } else {
            fragmentTransaction.replace(containerId, fragment, fragmentName);
        }
        fragmentTransaction.commit();
    }

    public void showDialogFragment(AppCompatDialogFragment appCompatDialogFragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(appCompatDialogFragment, appCompatDialogFragment.getClass().getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void showDialogFragment(Fragment targetFragment, AppCompatDialogFragment appCompatDialogFragment, int requestCode) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        appCompatDialogFragment.setTargetFragment(targetFragment, requestCode);
        fragmentTransaction.add(appCompatDialogFragment, appCompatDialogFragment.getClass().getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void showDialogSubFragment(Fragment targetFragment, AppCompatDialogFragment appCompatDialogFragment, int requestCode) {
        FragmentTransaction fragmentTransaction = targetFragment.getParentFragment().getChildFragmentManager().beginTransaction();
        appCompatDialogFragment.setTargetFragment(targetFragment, requestCode);
        fragmentTransaction.add(appCompatDialogFragment, appCompatDialogFragment.getClass().getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void showDialogFragment(BaseDialogFragment targetFragment, AppCompatDialogFragment appCompatDialogFragment, int requestCode) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        appCompatDialogFragment.setTargetFragment(targetFragment, requestCode);
        fragmentTransaction.add(appCompatDialogFragment, appCompatDialogFragment.getClass().getSimpleName());
        fragmentTransaction.commitAllowingStateLoss();
    }

    protected void hideKeyBoard() {
        View view = this.getCurrentFocus();
        hideKeyBoard(view);
    }

    protected void hideKeyBoard(View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected void showKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    public void finishWithResultOk() {
        finishWithResultOk(null);
    }

    protected void finishWithResultOk(Intent intent) {
        hideKeyBoard();
        if (intent == null) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    protected void finishWithResultCancel() {
        finishWithResultCancel(null);
    }

    protected void finishWithResultCancel(Intent intent) {
        hideKeyBoard();
        if (intent == null) {
            setResult(RESULT_CANCELED);
        } else {
            setResult(RESULT_CANCELED, intent);
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public boolean isInternetAvailable() {
        return NetworkUtils.isInternetAvailable(this);
    }

    protected void back() {
        finishWithResultCancel();
    }

    protected void backToHome() {
        launchActivity(DashboardActivity.getActivityIntent(this, false));
    }
}
