package com.swiftshopper.sdk.starterapp.util;

import android.text.TextUtils;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Validations {

    public static boolean isNull(String str) {
        return (str == null || str.equalsIgnoreCase("null") || str.trim().length() == 0);
    }

    public static boolean isNotNull(String str) {
        return !isNull(str);
    }

    public static boolean isNotNull(Object o) {
        return o != null;
    }

    public static boolean isExpectedType(Object o, Class c) {
        return isNotNull(o) && isNotNull(c) && c.isAssignableFrom(o.getClass());
    }

    public static boolean isFloat(String input) {
        Pattern p = Pattern.compile(
                "[\\x00-\\x20]*[+-]?(NaN|Infinity|((((\\p{Digit}+)(\\.)?((\\p{Digit}+)?)"
                        + "([eE][+-]?(\\p{Digit}+))?)|(\\.((\\p{Digit}+))([eE][+-]?(\\p{Digit}+))?)|"
                        + "(((0[xX](\\p{XDigit}+)(\\.)?)|(0[xX](\\p{XDigit}+)?(\\.)(\\p{XDigit}+)))"
                        + "[pP][+-]?(\\p{Digit}+)))[fFdD]?))[\\x00-\\x20]*");
        Matcher m = p.matcher(input);
        return !m.matches();
    }

    public static boolean isServerURL(String input) {
        return isNotNull(input) && (input.startsWith("http://") || input.startsWith("https://"));
    }

    public static boolean isInteger(String input) {
        return !TextUtils.isDigitsOnly(input);
    }

    public static String isNullThenEmpty(String str) {
        return isNull(str) ? "" : str.trim();
    }

    public static final String isNullThenEmpty(double location) {
        if (location != 0.0f) {
            return String.valueOf(location);
        }
        return "";
    }

    public static boolean isNotNullThenImageURL(String str) {
        return isNotNull(str) && isServerURL(str) && checkImageURL(str.substring(str.lastIndexOf("/")));
    }

    public static String isNullThenRemoveCommas(String str) {
        return isNull(str) ? "" : str.replaceAll("^\"|\"$", "");
    }

    public static RequestBody requestBodyOfString(Object v) {
        String value = String.valueOf(v);
        return RequestBody.create(MediaType.parse("text/plain"), value);
    }

    public static MultipartBody.Part requestBodyOfFile(String key, File file) {
        return MultipartBody.Part.createFormData(key, file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
    }

    public static boolean isEqual(String str1, String str2) {
        return ((isNotNull(str1) && isNotNull(str2)) && str1.equalsIgnoreCase(str2));
    }

    public static boolean isNotEqualCheck(String str1, String str2) {
        return ((str1 != null && str2 != null) && (!str1.equalsIgnoreCase(str2)));
    }

    public static boolean isNotEqual(String str1, String str2) {
        return ((isNotNull(str1) && isNotNull(str2)) && (!str1.equalsIgnoreCase(str2)));
    }

    public static boolean isEqual(String barcode1, String barcodeType1, String barcode2, String barcodeType2) {
        return ((isNotNull(barcode1) && isNotNull(barcodeType1) && isNotNull(barcode2) && isNotNull(barcodeType2))
                && (barcode1.contains(barcode2) && barcodeType1.contains(barcodeType2)));
    }

    public static boolean checkEmail(String inputMail) {
        Pattern p = Pattern.compile("^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\\.([a-zA-Z])+([a-zA-Z])+");
        return isNotNull(inputMail) && !(p.matcher(inputMail)).matches();
    }

    public static boolean checkImageURL(String inputMail) {
        Pattern p = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp|jpeg))$)");
        return isNotNull(inputMail) && (p.matcher(inputMail)).matches();
    }

    public static boolean isCreditCard(String str) {
        Pattern patVisa = Pattern.compile("^4[0-9]{12}(?:[0-9]{3})?$");
        Pattern patMaster = Pattern.compile("^5[1-5][0-9]{14}$");
        Pattern patAmericanExpress = Pattern.compile("^3[47][0-9]{13}$");

        if (!patVisa.matcher(str).matches()) {
            if (!patMaster.matcher(str).matches()) {
                return !patAmericanExpress.matcher(str).matches();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isEmpty(String string) {
        if (string == null) {
            return true;
        } else {
            return string.trim().isEmpty();
        }
    }

    public static <T> boolean isEmpty(T[] array) {
        return array == null || array.length==0;
    }

    public static <T> boolean arrayLongerThan(T[] array, int length) {
        return (array != null && array.length > length);
    }
}
