package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.base.BaseView;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.PlanActivityBinding;
import com.swiftshopper.sdk.starterapp.util.branchio.BranchIOUtil;
import com.swiftshopper.sdk.ui.view.SsDealsView;
import com.swiftshopper.sdk.ui.view.SsListView;

import androidx.annotation.Nullable;
import io.branch.referral.BranchError;

public class PlanActivity extends BaseAppCompatActivity<PlanActivityBinding> implements
        SsListView.ShareListDelegate, View.OnClickListener {

    private BaseView mBaseView;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, PlanActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initToolbar();
        initializeBottomTabs();

    }

    private void initToolbar() {
        binding.ivLeft.setOnClickListener(this);
        binding.ivLogo.setOnClickListener(this);
        binding.cbvPlan.setOnClickListener(this);
    }

    private void initializeBottomTabs() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_lists:
                    loadPlanView();
                    return true;
                case R.id.navigation_deals:
                    loadDealsView();
                    return true;
                case R.id.navigation_go_shop:
                    if (SwiftShopperSdk.getStore() == null) {
                        launchActivity(SelectMarketActivity.getActivityIntent(PlanActivity.this));
                        return false;
                    } else {
                        loadPlanView();
                        return true;
                    }
                default:
                    return true;
            }
        });
        setBottomTabsForPlan();
    }

    public void setBottomTabsForPlan() {
        if (SwiftShopperSdk.getStore() == null) {
            binding.bottomNavigation.setSelectedItemId(R.id.navigation_lists);
            binding.bottomNavigation.getMenu().findItem(R.id.navigation_deals).setTitle(R.string.deals);
        } else {
            binding.bottomNavigation.setSelectedItemId(R.id.navigation_go_shop);
            binding.bottomNavigation.getMenu().removeItem(R.id.navigation_lists);
            binding.bottomNavigation.getMenu().findItem(R.id.navigation_deals).setTitle(R.string.tab_in_store_deals);
        }
    }

    private void loadDealsView() {
        binding.container.removeAllViews();
        mBaseView = new SsDealsView(this);
        binding.container.addView(mBaseView);
    }

    private void loadPlanView() {
        binding.container.removeAllViews();
        //mBaseView = new SsListView(this);
        SsListView ssListView = new SsListView(this);
        mBaseView = ssListView;
        // TODO: Remove these customizations -- they are just for testing loyalty development
        ssListView.setShowLoyaltyAlways(true);
        ssListView.setShowLoyaltyAddDelete(true);
        ((SsListView) mBaseView).setShareListDelegate(this);
        binding.container.addView(mBaseView);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_plan;
    }

    @Override
    public void shareList(int listId, int shareTypeId) {
        showLoading();
        BranchIOUtil.generateDeepLink(this, Integer.toString(listId),
                Integer.toString(shareTypeId), new BranchIOUtil.OnDeepLinkListener() {
            @Override
            public void onDeepLinkSuccess(String link) {
                hideLoading();
                BranchIOUtil.shareDeepLink(PlanActivity.this, link);
            }

            @Override
            public void onDeepLinkError(BranchError branchError) {
                hideLoading();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.iv_left) {
            onBackPressed();
        } else if (v.getId() == R.id.iv_logo) {
            backToHome();
        } else if (v.getId() == R.id.cbv_plan) {
            launchActivity(CartActivity.getActivityIntent(this));
        }
    }

    @Override
    public void onBackPressed() {
        if (mBaseView instanceof SsDealsView) {
            binding.bottomNavigation.setSelectedItemId(SwiftShopperSdk.getStore() == null ?
                    R.id.navigation_lists : R.id.navigation_go_shop);
        } else {
            super.onBackPressed();
        }
    }
}
