package com.swiftshopper.sdk.starterapp.util;

import android.os.Build;
import android.view.View;
import android.view.Window;

public class StatusbarUtils {

    public static void hideStatusBar(Window window, boolean shouldHideNavigationBar) {
        int uiOptions = window.getDecorView().getSystemUiVisibility();

        if (shouldHideNavigationBar) {
            uiOptions ^= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        uiOptions ^= View.SYSTEM_UI_FLAG_FULLSCREEN;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            uiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        window.getDecorView().setSystemUiVisibility(uiOptions);
    }
}
