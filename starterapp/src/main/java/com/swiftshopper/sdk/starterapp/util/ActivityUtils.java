package com.swiftshopper.sdk.starterapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

public class ActivityUtils {

    public static void openAppInPlayStore(Context context) {
        final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private static final int FRAGMENT_TRANSACTION_ADD = 200;
    private static final int FRAGMENT_TRANSACTION_REPLACE = 300;
    public static int DEFAULT_REQUEST_CODE = 100;

    protected static void finishWithResultOk(FragmentActivity fragmentActivity, Intent intent) {
        hideKeyBoard(fragmentActivity);
        if (intent == null) {
            fragmentActivity.setResult(Activity.RESULT_OK);
        } else {
            fragmentActivity.setResult(Activity.RESULT_OK, intent);
        }
        fragmentActivity.finish();
    }

    protected static void finishWithResultCancel(FragmentActivity fragmentActivity) {
        finishWithResultCancel(fragmentActivity, null);
    }

    protected static void finishWithResultCancel(FragmentActivity fragmentActivity, Intent intent) {
        hideKeyBoard(fragmentActivity);
        if (intent == null) {
            fragmentActivity.setResult(Activity.RESULT_CANCELED);
        } else {
            fragmentActivity.setResult(Activity.RESULT_CANCELED, intent);
        }
        fragmentActivity.finish();
    }

    protected static void back(FragmentActivity fragmentActivity) {
        finishWithResultCancel(fragmentActivity);
    }

    protected static void backToHome(FragmentActivity fragmentActivity) {
        finishWithResultCancel(fragmentActivity, new Intent());
    }

    public static void launchActivity(FragmentActivity fragmentActivity, Intent intent) {
        launchActivity(fragmentActivity, intent, false);
    }

    public static void launchActivity(FragmentActivity fragmentActivity, Intent intent, boolean finishCurrent) {
        launchActivity(fragmentActivity, intent, DEFAULT_REQUEST_CODE, finishCurrent);
    }

    public static void launchActivity(FragmentActivity fragmentActivity, Intent intent, int requestCode) {
        launchActivity(fragmentActivity, intent, requestCode, false);
    }

    public static void launchActivity(FragmentActivity fragmentActivity, Intent intent, int requestCode, boolean finishCurrent) {
        if (requestCode != DEFAULT_REQUEST_CODE) {
            fragmentActivity.startActivityForResult(intent, requestCode);
        } else {
            if (finishCurrent) {
                fragmentActivity.finish();
            }
            fragmentActivity.startActivity(intent);
        }
    }

    protected static void hideKeyBoard(FragmentActivity fragmentActivity) {
        View view = fragmentActivity.getCurrentFocus();
        hideKeyBoard(fragmentActivity, view);
    }

    protected static void hideKeyBoard(FragmentActivity fragmentActivity, View view) {
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    protected static void showKeyBoard(FragmentActivity fragmentActivity) {
        View view = fragmentActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) fragmentActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, 0);
        }
    }

    public static void finishWithResultOk(FragmentActivity fragmentActivity) {
        finishWithResultOk(fragmentActivity, null);
    }

    public static void addFragment(FragmentActivity fragmentActivity, Fragment fragment, int containerId) {
        addFragment(fragmentActivity, fragment, false, containerId);
    }

    public static void addFragment(FragmentActivity fragmentActivity, Fragment fragment, boolean addToBackStack, int containerId) {
        loadFragment(fragmentActivity, fragment, FRAGMENT_TRANSACTION_ADD, addToBackStack, containerId);
    }

    public static void replaceFragment(FragmentActivity fragmentActivity, Fragment fragment, int containerId) {
        replaceFragment(fragmentActivity, fragment, false, containerId);
    }

    public static void replaceFragment(FragmentActivity fragmentActivity, Fragment fragment, boolean addToBackStack, int containerId) {
        loadFragment(fragmentActivity, fragment, FRAGMENT_TRANSACTION_REPLACE, addToBackStack, containerId);
    }

    private static void loadFragment(FragmentActivity fragmentActivity, Fragment fragment, int transactionType, int containerId) {
        loadFragment(fragmentActivity, fragment, transactionType, false, containerId);
    }

    public static void loadFragment(FragmentActivity fragmentActivity, Fragment fragment, int transactionType, boolean addToBackStack, int containerId) {
        String fragmentName = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragmentName);
        }
        if (transactionType == FRAGMENT_TRANSACTION_ADD) {
            fragmentTransaction.add(containerId, fragment, fragmentName);
        } else {
            fragmentTransaction.replace(containerId, fragment, fragmentName);
        }
        fragmentTransaction.commit();
    }

    public static void showDialogFragment(FragmentActivity fragmentActivity, AppCompatDialogFragment appCompatDialogFragment) {
        appCompatDialogFragment.show(fragmentActivity.getSupportFragmentManager(), appCompatDialogFragment.getClass().getSimpleName());
    }

    public static void showDialogFragment(FragmentActivity fragmentActivity, Fragment targetFragment, AppCompatDialogFragment appCompatDialogFragment, int requestCode) {
        appCompatDialogFragment.setTargetFragment(targetFragment, requestCode);
        appCompatDialogFragment.show(fragmentActivity.getSupportFragmentManager(), appCompatDialogFragment.getClass().getSimpleName());
    }

    public static void showDialogFragment(FragmentActivity fragmentActivity, AppCompatDialogFragment targetFragment, AppCompatDialogFragment appCompatDialogFragment, int requestCode) {
        appCompatDialogFragment.setTargetFragment(targetFragment, requestCode);
        appCompatDialogFragment.show(fragmentActivity.getSupportFragmentManager(), appCompatDialogFragment.getClass().getSimpleName());
    }

    public static Fragment getFragmentFromActivity(FragmentActivity fragmentActivity, Class fragmentClass) {
        return fragmentActivity.getSupportFragmentManager().findFragmentByTag(fragmentClass.getSimpleName());
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void hideKeyboard(Activity activity) {
        Handler handler = new Handler();
        handler.postDelayed(() -> {
            InputMethodManager keyboard = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            keyboard.hideSoftInputFromWindow(activity.findViewById(android.R.id.content).getWindowToken(), 0);
        }, 500);
    }
}
