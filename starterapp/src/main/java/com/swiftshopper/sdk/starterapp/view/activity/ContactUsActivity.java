package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;

import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseNavigationActivity;
import com.swiftshopper.sdk.starterapp.databinding.ContactUsAB;
import com.swiftshopper.sdk.starterapp.viewmodel.ContactUsVM;

/**
 * Created by dipak.vyas on 20/03/18.
 */

public class ContactUsActivity extends BaseNavigationActivity<ContactUsAB,ContactUsVM> {

    public static final int RC_CONTACT_US = 221;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, ContactUsActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected ContactUsVM createViewModel() {
        return new ContactUsVM();
    }

    @Override
    protected void setViewModel(ContactUsAB binding, ContactUsVM viewModel) {
        binding.setViewModel(viewModel);
        setToolbar();
    }

    private void setToolbar() {
        binding.customToolbar.builder()
                .setLeft1(R.drawable.ic_back, v -> back())
                .setLogo(R.drawable.ic_logo_small, v -> backToHome())
                .setTitle(getString(R.string.contact_us), null);
    }
}
