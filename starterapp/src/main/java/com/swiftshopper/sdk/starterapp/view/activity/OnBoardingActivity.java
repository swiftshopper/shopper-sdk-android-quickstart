package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.base.BasePagerAdapter;
import com.swiftshopper.sdk.starterapp.databinding.OnBoardingBinding;
import com.swiftshopper.sdk.starterapp.model.IntroductionModel;
import com.swiftshopper.sdk.starterapp.util.StatusbarUtils;

import java.util.ArrayList;

public class OnBoardingActivity extends BaseAppCompatActivity<OnBoardingBinding> implements View.OnClickListener, ViewPager.OnPageChangeListener {

    public static Intent getActivityIntent(Context context) {
        Intent intent = new Intent(context, OnBoardingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_lets_start:
                callLoginScreen();
                break;
            default:
                break;
        }
    }

    private void initControlListeners() {
        binding.btnLetsStart.setOnClickListener(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_onboarding;
    }

    private void initViews() {
        ArrayList<IntroductionModel> introductionModelArrayList = new ArrayList<>();
        introductionModelArrayList.add(null);
        introductionModelArrayList.add(new IntroductionModel(R.drawable.shot_1, R.array.page_2_points));
        introductionModelArrayList.add(new IntroductionModel(R.drawable.shot_2, R.array.page_3_points));
        introductionModelArrayList.add(new IntroductionModel(R.drawable.shot_3, R.array.page_4_points));
        introductionModelArrayList.add(new IntroductionModel(R.drawable.shot_4, R.array.page_5_points));
        introductionModelArrayList.add(new IntroductionModel(R.drawable.shot_5, R.array.page_6_points));
        introductionModelArrayList.add(new IntroductionModel(R.drawable.shot_6, R.array.page_7_points));

        createPagingIndicator(introductionModelArrayList.size());

        createPager(introductionModelArrayList);
    }

    private void createPager(ArrayList<IntroductionModel> introductionModelArrayList) {
        binding.vpLoginNotification.addOnPageChangeListener(this);
        BasePagerAdapter<IntroductionModel> introductionModelBasePagerAdapter = new BasePagerAdapter<IntroductionModel>(this) {
            @Override
            public int getViewType(int position) {
                return position == 0 ? R.layout.layout_introduction_1 : R.layout.layout_introduction_2;
            }

            @Override
            public View bindDataAndReturnView(ViewGroup container, View view, IntroductionModel model, int viewType, int position) {
                if (model == null) {
                    // Nothing to do, its first page
                } else {
                    // Set Image
                    ImageView imageView = view.findViewById(R.id.iv_introduction_2);
                    imageView.setImageResource(model.getImage());

                    // Set Points
                    LinearLayout linearLayout = view.findViewById(R.id.ll_introduction_2);
                    linearLayout.removeAllViews();

                    TypedArray typedArray = getResources().obtainTypedArray(model.getPoints());
                    for (int count = 0; count < typedArray.length(); count++) {
                        View view1 = getLayoutInflater().inflate(R.layout.layout_introduction_points, null);
                        ((TextView) view1.findViewById(R.id.tv_point_description)).setText(typedArray.getResourceId(count, 0));
                        linearLayout.addView(view1);
                    }
                    typedArray.recycle();
                }
                container.addView(view);
                return view;
            }
        };
        introductionModelBasePagerAdapter.setItems(introductionModelArrayList);
        binding.vpLoginNotification.setAdapter(introductionModelBasePagerAdapter);
    }

    private void createPagingIndicator(int introductionModelArrayList) {
        binding.rgLoginNotification.removeAllViews();
        for (int counter = 0; counter < introductionModelArrayList; counter++) {
            binding.rgLoginNotification.addView(getLayoutInflater().inflate(R.layout.layout_indicator, null));
        }
        ((RadioButton) binding.rgLoginNotification.getChildAt(0)).setChecked(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        StatusbarUtils.hideStatusBar(getWindow(), false);
        super.onCreate(savedInstanceState);
        initViews();
        initControlListeners();
    }

    private void callLoginScreen() {
        launchActivity(LoginActivity.getActivityIntent(this), true);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        binding.rgLoginNotification.clearCheck();
        RadioButton radioButton = (RadioButton) binding.rgLoginNotification.getChildAt(position);
        radioButton.setChecked(true);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
