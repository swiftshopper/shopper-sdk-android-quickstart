package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseAppCompatActivity;
import com.swiftshopper.sdk.starterapp.databinding.ProfileBinding;
import com.swiftshopper.sdk.starterapp.util.DialogUtils;

/**
 * Created by chintan.soni on 26/07/17.
 */

public class ProfileActivity extends BaseAppCompatActivity<ProfileBinding> implements SwiftShopperSdk.LogoutListener {

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbar();
    }

    private void setToolbar() {
        binding.customToolbar.builder()
                .setLeft1(R.drawable.ic_back, view -> back())
                .setLogo(R.drawable.ic_logo_small, v -> backToHome())
                .setRightView("", R.drawable.ic_ico_logout, 0, v -> DialogUtils.confirm(ProfileActivity.this,
                        R.string.dialog_logout_title,
                        R.string.dialog_logout_desc,
                        R.string.btn_logout,
                        (dialogInterface, i) -> logout(),
                        R.string.btn_cancel,
                        (dialogInterface, i) -> {

                        }));
    }

    private void logout() {
        SwiftShopperSdk.logout(this);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_profile;
    }

    @Override
    public void onLogout() {
        launchActivity(OnBoardingActivity.getActivityIntent(this), true);
    }
}
