package com.swiftshopper.sdk.starterapp.base;

import android.content.Context;
import android.content.Intent;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.swiftshopper.sdk.starterapp.R;

public abstract class BaseFragment<DATABINDING extends ViewDataBinding, VIEWMODEL> extends Fragment {
    private static final int FRAGMENT_TRANSACTION_ADD = 200;
    private static final int FRAGMENT_TRANSACTION_REPLACE = 300;
    private static final int DIALOG_FRAGMENT_REQUEST_CODE = 301;
    public BaseAppCompatActivity mBaseNavigationActivity;
    protected Context context;
    protected DATABINDING binding;
    protected VIEWMODEL viewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayout(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = createViewModel();
        setViewModel(binding, viewModel);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.mBaseNavigationActivity = (BaseAppCompatActivity) context;
    }

    protected abstract int getLayout();

    protected abstract VIEWMODEL createViewModel();

    protected abstract void setViewModel(DATABINDING binding, VIEWMODEL viewModel);

    protected void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void showDialogFragment(BaseFragment targetFragment, AppCompatDialogFragment appCompatDialogFragment) {
        appCompatDialogFragment.setTargetFragment(targetFragment, DIALOG_FRAGMENT_REQUEST_CODE);
        appCompatDialogFragment.show(targetFragment.getParentFragment().getChildFragmentManager(), appCompatDialogFragment.getClass().getSimpleName());
    }

    public void launchActivity(Intent intent) {
        launchActivity(intent, mBaseNavigationActivity.DEFAULT_REQUEST_CODE);
    }

    public void launchActivity(Intent intent, int requestCode) {
        if (requestCode == mBaseNavigationActivity.DEFAULT_REQUEST_CODE) {
            mBaseNavigationActivity.launchActivity(intent);
        } else {
            startActivityForResult(intent, requestCode);
        }
    }

    protected void hideKeyBoard() {
        mBaseNavigationActivity.hideKeyBoard();
    }

    protected void hideKeyBoard(View view) {
        mBaseNavigationActivity.hideKeyBoard(view);
    }

    protected boolean isInternetAvailable() {
        return mBaseNavigationActivity.isInternetAvailable();
    }

    protected Fragment getFragmentFromActivity(Class t) {
        return mBaseNavigationActivity.getSupportFragmentManager().findFragmentByTag(t.getSimpleName());
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment, false);
    }

    public void addFragment(Fragment fragment, boolean addToBackStack) {
        loadFragment(fragment, FRAGMENT_TRANSACTION_ADD, addToBackStack);
    }

    public void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, false);
    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        loadFragment(fragment, FRAGMENT_TRANSACTION_REPLACE, addToBackStack);
    }

    private void loadFragment(Fragment fragment, int transactionType) {
        loadFragment(fragment, transactionType, false);
    }

    private void loadFragment(Fragment fragment, int transactionType, boolean addToBackStack) {
        loadFragment(fragment, R.id.frameLayout, transactionType, addToBackStack, mBaseNavigationActivity.getSupportFragmentManager());
    }

    public void addSubFragment(Fragment fragment) {
        addSubFragment(fragment, false);
    }

    public void addSubFragment(Fragment fragment, boolean addToBackStack) {
        loadSubFragment(fragment, FRAGMENT_TRANSACTION_ADD, addToBackStack);
    }

    public void replaceSubFragment(Fragment fragment) {
        replaceSubFragment(fragment, false);
    }

    public void replaceSubFragment(Fragment fragment, boolean addToBackStack) {
        loadSubFragment(fragment, FRAGMENT_TRANSACTION_REPLACE, addToBackStack);
    }

    private void loadSubFragment(Fragment fragment, int transactionType) {
        loadSubFragment(fragment, transactionType, false);
    }

    private void loadSubFragment(Fragment fragment, int transactionType, boolean addToBackStack) {
        loadFragment(fragment, R.id.frameLayoutSub, transactionType, addToBackStack, getChildFragmentManager());
    }

    private void loadFragment(Fragment fragment, int containerId, int transactionType, boolean addToBackStack, FragmentManager fragmentManager) {
        if (fragment != null) {
            String fragmentName = fragment.getClass().getSimpleName();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            if (addToBackStack) {
                fragmentTransaction.addToBackStack(fragmentName);
            }
            if (transactionType == FRAGMENT_TRANSACTION_ADD) {
                fragmentTransaction.add(containerId, fragment, fragmentName);
            } else {
                fragmentTransaction.replace(containerId, fragment, fragmentName);
            }
            fragmentTransaction.commit();
        }
    }
}
