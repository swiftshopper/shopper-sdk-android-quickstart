package com.swiftshopper.sdk.starterapp.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.os.Build;
import android.view.View;

import java.util.List;

import androidx.databinding.ViewDataBinding;
import timber.log.Timber;

public class Utility {

    private static String sCachedUserAgent = null;

    public static int dpToPx(float dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }





    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }



    public static ApplicationInfo getAppInfo(Context context) {
        ApplicationInfo retVal = null;
        if (context == null) {
            Timber.w("Null context passed to Utility.getAppInfo(...). Please have my program fixed.");
        } else {
            PackageManager packageManager = context.getPackageManager();
            try {
                retVal = packageManager.getApplicationInfo(
                        context.getApplicationInfo().packageName, 0);
            } catch (final PackageManager.NameNotFoundException e) {
                Timber.w("Unable to lookup up the app's name using the Android Package Manager");
            }
        }
        return retVal;
    }

    public static String getAppPackage(Context context) {
        String retVal;
        if (context == null) {
            Timber.w("Null context passed to Utility.getAppName(...). Please have my program fixed.");
            retVal = "";
        } else {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = null;
            try {
                applicationInfo = packageManager.getApplicationInfo(
                        context.getApplicationInfo().packageName, 0);
            } catch (final PackageManager.NameNotFoundException e) {
                Timber.w("Unable to lookup up the app's name using the Android Package Manager");
            }

            CharSequence applicationLabel = null;
            if (packageManager == null || applicationInfo == null ||
                    (applicationLabel = packageManager.getApplicationLabel(applicationInfo)) == null) {
                retVal = context.getString(com.swiftshopper.sdk.R.string.app_name);
            } else {
                retVal = applicationLabel.toString();
            }
        }
        return retVal;

    }

    public static String getAppName(Context context) {
        String retVal;
        if (context == null) {
            Timber.w("Null context passed to Utility.getAppName(...). Please have my program fixed.");
            retVal = "";
        } else {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = null;
            try {
                applicationInfo = packageManager.getApplicationInfo(
                        context.getApplicationInfo().packageName, 0);
            } catch (final PackageManager.NameNotFoundException e) {
                Timber.w("Unable to lookup up the app's name using the Android Package Manager");
            }

            CharSequence applicationLabel = null;
            if (packageManager == null || applicationInfo == null ||
                    (applicationLabel = packageManager.getApplicationLabel(applicationInfo)) == null) {
                retVal = context.getString(com.swiftshopper.sdk.R.string.app_name);
            } else {
                retVal = applicationLabel.toString();
            }
        }
        return retVal;
    }

    public static boolean anyActivityInThisAppCanHandleIntent(Context context, String action) {
        return getActivityInThisAppToHandleIntent(context, action) != null;
    }

    public static ComponentName getActivityInThisAppToHandleIntent(Context context, String action) {
        ComponentName retVal = null;
        PackageManager pm;
        ApplicationInfo applicationInfo;

        if (context != null && (pm=context.getPackageManager())!= null &&
                Validations.isNotNull(action) &&
                Validations.isNotNull(applicationInfo=context.getApplicationInfo())) {
            Intent intent = new Intent(action);
            List<ResolveInfo> activitiesForIntent = pm.queryIntentActivities(
                    intent, PackageManager.MATCH_DEFAULT_ONLY);
            if (activitiesForIntent != null && activitiesForIntent.size() > 0) {
                for (ResolveInfo resolveInfo : activitiesForIntent) {
                    if (applicationInfo.packageName.equals(resolveInfo.activityInfo.packageName)) {
                        retVal = new ComponentName(resolveInfo.activityInfo.packageName,
                                resolveInfo.activityInfo.name);
                        break;
                    }
                }
            }
        }
        return retVal;
    }

    public static boolean startDefaultMainActivity(Context context) {
        boolean retVal = false;
        PackageManager pm;
        if (context != null && (pm=context.getPackageManager())!=null) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            ComponentName mainDefaultActivityComponent = intent.resolveActivity(pm);
            if (mainDefaultActivityComponent != null) {
                context.startActivity(intent);
                retVal = true;
            }
        }
        return retVal;
    }

    public static void sendActionBroadcast(Intent intent, ViewDataBinding binding) {
        Context context;
        View bindingRootView;
        if (Validations.isNotNull(binding) &&
                Validations.isNotNull(bindingRootView=binding.getRoot()) &&
                Validations.isNotNull(context=bindingRootView.getContext())) {
            sendActionBroadcast(intent, context);
        }
    }

    public static void sendActionBroadcast(Intent intent, Context context) {
        ApplicationInfo applicationInfo;
        String appPackage;
        if (Validations.isNotNull(context) &&
                Validations.isNotNull(applicationInfo=context.getApplicationInfo()) &&
                Validations.isNotNull(appPackage=applicationInfo.packageName)) {
            intent.setPackage(appPackage);
            context.sendBroadcast(intent);
        }
    }

    public static String capitalizeFirstLetter(String str) {
        String retVal = "";
        if (Validations.isNotNull(str)) {
            if (str.length() > 1) {
                retVal = str.substring(0,1).toUpperCase() + str.substring(1);

            } else {
                // string length is 1 since Validations.isNotNull(...) returns false
                // for empty strings
                retVal = str.substring(0,1).toUpperCase();
            }
        }
        return retVal;
    }


}