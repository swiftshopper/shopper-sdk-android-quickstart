package com.swiftshopper.sdk.starterapp.view.activity;

import android.content.Context;
import android.content.Intent;

import com.swiftshopper.sdk.starterapp.R;
import com.swiftshopper.sdk.starterapp.base.BaseNavigationActivity;
import com.swiftshopper.sdk.starterapp.databinding.AboutAB;
import com.swiftshopper.sdk.starterapp.viewmodel.AboutVM;

/**
 * Created by dipak.vyas on 20/03/18.
 */

public class AboutActivity extends BaseNavigationActivity<AboutAB,AboutVM>{
    public static final int RC_ABOUT = 222;

    public static Intent getActivityIntent(Context context) {
        return new Intent(context, AboutActivity.class);
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_about;
    }

    @Override
    protected AboutVM createViewModel() {
        return new AboutVM();
    }

    @Override
    protected void setViewModel(AboutAB binding, AboutVM viewModel) {
        //binding.setViewModel(viewModel);
        setToolbar();
    }

    private void setToolbar() {
        binding.customToolbar.builder()
                .setLeft1(R.drawable.ic_back, v -> back())
                .setLogo(R.drawable.ic_logo_small, v -> backToHome())
                .setTitle(getString(R.string.about), null);
    }
}
