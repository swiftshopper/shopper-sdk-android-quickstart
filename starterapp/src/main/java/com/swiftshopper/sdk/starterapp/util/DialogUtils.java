package com.swiftshopper.sdk.starterapp.util;

import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

public class DialogUtils {

    public static void alert(Context context, int title, int message, int buttonTitle,
                             DialogInterface.OnClickListener onPositiveButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(buttonTitle, onPositiveButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static AlertDialog alert(Context context, String title, String message, int buttonTitle,
                                    DialogInterface.OnClickListener onPositiveButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(buttonTitle, onPositiveButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    /**
     * This function shows Dialog with Title, Message and
     * two buttons (Positive Button and Negative Button).
     *
     * @param context                       Activity or getContext from
     * @param title                         string resource id to set title
     * @param message                       string resource id to set message
     * @param positiveButtonTitle           string resource id to set Positive Button title
     * @param onPositiveButtonClickListener DialogInterface.OnClickListener to handle click event of Positive Button
     * @param negativeButtonTitle           string resource id to set Negative Button title
     * @param onNegativeButtonClickListener DialogInterface.OnClickListener to handle click event of Negative Button
     */
    public static void confirm(Context context, @StringRes int title, @StringRes int message,
                               @StringRes int positiveButtonTitle, DialogInterface.OnClickListener
                                       onPositiveButtonClickListener, @StringRes int negativeButtonTitle,
                               DialogInterface.OnClickListener onNegativeButtonClickListener) {
        confirm(context, context.getString(title), context.getString(message),
                context.getString(positiveButtonTitle), onPositiveButtonClickListener,
                context.getString(negativeButtonTitle), onNegativeButtonClickListener);
    }

    public static void confirm(Context context, String title, String message, String positiveButtonTitle,
                               DialogInterface.OnClickListener onPositiveButtonClickListener, String negativeButtonTitle,
                               DialogInterface.OnClickListener onNegativeButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title))
            builder.setTitle(title);
        if (!TextUtils.isEmpty(message))
            builder.setMessage(message);
        builder.setPositiveButton(positiveButtonTitle, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeButtonTitle, onNegativeButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void confirm(Context context, int title, String message, int positiveButtonTitle,
                               DialogInterface.OnClickListener onPositiveButtonClickListener, int negativeButtonTitle,
                               DialogInterface.OnClickListener onNegativeButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveButtonTitle, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeButtonTitle, onNegativeButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void confirm(Context context, String title, String message, int positiveButtonTitle,
                               DialogInterface.OnClickListener onPositiveButtonClickListener, int negativeButtonTitle,
                               DialogInterface.OnClickListener onNegativeButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveButtonTitle, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeButtonTitle, onNegativeButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void confirmDeleteCartItem(Context context, String title, int positiveButtonTitle,
                                             DialogInterface.OnClickListener onPositiveButtonClickListener, int negativeButtonTitle,
                                             DialogInterface.OnClickListener onNegativeButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setPositiveButton(positiveButtonTitle, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeButtonTitle, onNegativeButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void confirmDeleteCartItem(Context context, int title, int positiveButtonTitle,
                                             DialogInterface.OnClickListener onPositiveButtonClickListener, int negativeButtonTitle,
                                             DialogInterface.OnClickListener onNegativeButtonClickListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(title));
        builder.setPositiveButton(positiveButtonTitle, onPositiveButtonClickListener);
        builder.setNegativeButton(negativeButtonTitle, onNegativeButtonClickListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public static void confirm(Context context,
                               int title,
                               int message,
                               int positiveButtonTitle,
                               DialogInterface.OnClickListener onPositiveButtonClickListener,
                               int negativeButtonTitle,
                               DialogInterface.OnClickListener onNegativeButtonClickListener,
                               int neutralButtonTitle,
                               DialogInterface.OnClickListener onNeutralButtonClickListener) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(positiveButtonTitle, onPositiveButtonClickListener)
                .setNegativeButton(negativeButtonTitle, onNegativeButtonClickListener)
                .setNeutralButton(neutralButtonTitle, onNeutralButtonClickListener)
                .create().show();
    }
}