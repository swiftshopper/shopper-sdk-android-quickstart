package com.swiftshopper.sdk.quickstart;

public class OrderHistoryActivity extends AppSecondaryActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_order_history;
    }

}
