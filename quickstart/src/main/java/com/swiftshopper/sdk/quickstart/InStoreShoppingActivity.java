package com.swiftshopper.sdk.quickstart;

import android.content.Intent;

import androidx.appcompat.app.AlertDialog;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.ui.view.SsCartButtonView;

public class InStoreShoppingActivity extends AppSecondaryActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_in_store_shopping;
    }

    @Override protected void addSecondaryToolbar() {
        SsCartButtonView ssCartButtonView = new SsCartButtonView(this);
        ssCartButtonView.setOnClickListener((v)->{
            onCartButtonClick();
        });

        getTitle();
        // ssCartButtonView.setCartButtonForegroundColor(Color.GREEN);
        ToolbarHelper.addCustomToolbar(this, getString(R.string.exit_store), (v)->{
                    confirmExitStore();
                }, getMyTitle(),
                ssCartButtonView);
    }

    protected String getMyTitle() {
        return getString(R.string.title_activity_in_store_shopping);
    }

    @Override
    protected int getLowerRightActionButtonDrawable() {
        return R.drawable.ico_tag;
    }

    @Override
    protected int getLowerLeftActionButtonDrawable() {
        return R.drawable.shopping_list_icon;
    }

    private void onCartButtonClick() {
        startActivity(new Intent(this, CartActivity.class));
    }

    private void confirmExitStore() {
        new AlertDialog.Builder(InStoreShoppingActivity.this)
                .setTitle(R.string.exit_store_confirmation_title)
                .setMessage(R.string.exit_store_confirmation_message)
                .setPositiveButton(R.string.exit_store_yes, (dialog, which) -> {
                    SwiftShopperSdk.exitStore(()->{
                        exitedStore();
                    });

                }).setNegativeButton(R.string.exit_store_no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }

    protected void exitedStore() {
        finishAffinity();
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onBackPressed() {
        confirmExitStore();
    }

    @Override
    protected void onLowerRightActionButtonClicked() {
        startActivity(new Intent(this, DealsActivity.class));
    }

    @Override
    protected void onLowerLeftActionButtonClicked() {
        startActivity(new Intent(this, ShoppingListActivity.class));
    }
}
