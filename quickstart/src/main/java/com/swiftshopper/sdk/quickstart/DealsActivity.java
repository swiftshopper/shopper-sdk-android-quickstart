package com.swiftshopper.sdk.quickstart;

import android.content.Intent;

import com.swiftshopper.sdk.SwiftShopperSdk;

public class DealsActivity extends BackIconAndCartActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_deals;
    }

    @Override
    protected void addSecondaryToolbar() {
        super.addSecondaryToolbar();

        if (SwiftShopperSdk.getStore() != null) {
            ToolbarHelper.setTitle(this, R.string.title_activity_in_store_shopping);
            ToolbarHelper.setSubTitle(this, R.string.subtitle_activity_deals_when_shopping);
        }
    }

    @Override
    protected int getLowerLeftActionButtonDrawable() {
        if (SwiftShopperSdk.getStore() == null) {
            return -1;
        } else {
            return R.drawable.shopping_list_icon;
        }
    }

    @Override
    protected void onLowerLeftActionButtonClicked() {
        startActivity(new Intent(this, ShoppingListActivity.class));
    }

    @Override
    protected int getFloatingActionButtonBottomMargin() {
        return getResources().getDimensionPixelSize(R.dimen.deals_fab_margin_bottom);
    }

}
