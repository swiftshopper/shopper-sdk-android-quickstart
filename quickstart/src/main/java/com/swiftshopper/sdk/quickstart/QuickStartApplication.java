package com.swiftshopper.sdk.quickstart;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.core.content.ContextCompat;

import com.swiftshopper.sdk.SsConfig;
import com.swiftshopper.sdk.SwiftShopperSdk;

public class QuickStartApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        SsConfig ssConfig = new SsConfig.Builder(this)
                .setRetailerSdkKey("w9mn{dn7lPIF37Yb#>fh)By^Q|AoA3w8<SGXbd2gmPF2|")
                //.setRetailerSdkKey("2XEL$Qln:6]JEfa:shp%nc#s@bxb;78Zn,L7&u?vM#s:Z")
                .setActivityTheme(R.style.AppTheme_NoActionBar)
                .setThemeColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setLargeLogo(R.mipmap.ic_launcher)
                .setSmallLogo(R.mipmap.ic_launcher).build();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SwiftShopperSdk.ACTION_CHECKOUT_COMPLETE);
        registerReceiver(mSwiftShopperBroadCastReceiver,
                intentFilter);
        SwiftShopperSdk.initialize(ssConfig);
    }

    public BroadcastReceiver mSwiftShopperBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ManualScanExampleActivity.shouldRestoreManualScanExample(context, false);
            Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainActivityIntent);
        }
    };

    @Override
    public void onTerminate() {
        unregisterReceiver(mSwiftShopperBroadCastReceiver);
        super.onTerminate();
    }
}
