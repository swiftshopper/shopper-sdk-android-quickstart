package com.swiftshopper.sdk.quickstart;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;

import com.swiftshopper.sdk.SwiftShopperSdk;

public class ManualScanExampleActivity extends InStoreShoppingActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViewById(R.id.manualScanButtonOnManualScanExampleActivity).setOnClickListener((v)->{
            showLoading();
            SwiftShopperSdk.scanItemToCart(ssScanResult -> {
                dismissLoading();
                String rawBarcode = ssScanResult.getRawBarcode();
                String dlgMsg = ssScanResult.getMessage();
                if (rawBarcode!= null && rawBarcode.length() > 0) {
                    dlgMsg += "\n\nScanned Barcode: " + rawBarcode;
                }
                AlertDialog.Builder dlgAlert = new AlertDialog.Builder(this);
                dlgAlert.setTitle(R.string.manual_scan_result_dialog_title);
                dlgAlert.setMessage(dlgMsg);
                dlgAlert.setPositiveButton("OK", (dlg, id)->{
                });
                dlgAlert.create().show();
            });
        });
    }

    @Override
    protected String getMyTitle() {
        return getString(R.string.title_activity_manual_scan_example);
    }

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_manual_scan_example;
    }

    @Override
    protected int getLowerRightActionButtonDrawable() {
        return -1;
    }

    @Override
    protected int getLowerLeftActionButtonDrawable() {
        return -1;
    }


    @Override
    protected void onLowerRightActionButtonClicked() {
    }

    @Override
    protected void onLowerLeftActionButtonClicked() {
    }

    @Override
    protected void exitedStore() {
        super.exitedStore();
        // In case the user closes the app, this is the app's cue to go directly back to the
        // ManualScanExampleActivity when the app is opened again later
        shouldRestoreManualScanExample(this, false);
    }

    private static final String SHOULD_RESTORE_MANUAL_SCAN_EXAMPLE_SHARED_PREFS_KEY =
            "inManualScanExample";

    public static boolean shouldRestoreManualScanExample(Context cx) {
        SharedPreferences sharedPreferences = cx.getSharedPreferences(
                ManualScanExampleActivity.class.getName(), Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(
                SHOULD_RESTORE_MANUAL_SCAN_EXAMPLE_SHARED_PREFS_KEY,false);
    }

    public static void shouldRestoreManualScanExample(Context cx, boolean shouldRestore) {
        SharedPreferences sharedPreferences = cx.getSharedPreferences(
                ManualScanExampleActivity.class.getName(), Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean(
                SHOULD_RESTORE_MANUAL_SCAN_EXAMPLE_SHARED_PREFS_KEY, shouldRestore);
        sharedPreferencesEditor.commit();
    }

    public static void startActivity(Context cx) {
        // In case the user closes the app, this is the app's cue to go directly back to the
        // ManualScanExampleActivity when the app is opened again later. (See the MainActivity's
        // onCreate(...) method)
        shouldRestoreManualScanExample(cx, true);
        cx.startActivity(new Intent(cx,
                ManualScanExampleActivity.class));

    }
}
