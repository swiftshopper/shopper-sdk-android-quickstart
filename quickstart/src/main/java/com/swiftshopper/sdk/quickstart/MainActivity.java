package com.swiftshopper.sdk.quickstart;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (SwiftShopperSdk.getStore() == null) {
            View contentView = getLayoutInflater().inflate(R.layout.activity_main, null);
            setContentView(contentView);

            Button shopSingleStoreButton = contentView.findViewById(R.id.shopSingleStore);
            shopSingleStoreButton.setOnClickListener((v) -> {
                onShopSingleStoreClicked(v);
            });

            Button shopChooseStoreButton = contentView.findViewById(R.id.shopChooseStore);
            shopChooseStoreButton.setOnClickListener((v) -> {
                onShopChooseStoreClicked(v);
            });

            contentView.findViewById(R.id.loginLogout).setOnClickListener((v) -> {
                onLoginLogoutClicked(v);
            });
            contentView.findViewById(R.id.orderHistory).setOnClickListener((v) -> {
                onOrderHistoryClicked(v);
            });
            contentView.findViewById(R.id.shoppingList).setOnClickListener((v) -> {
                onShoppingListsClicks(v);
            });
            contentView.findViewById(R.id.profileButton).setOnClickListener((v) -> {
                onProfileClicked(v);
            });
            contentView.findViewById(R.id.manualScanExample).setOnClickListener((v) -> {
                onManualScanExampleClicked(v);
            });
        } else if (ManualScanExampleActivity.shouldRestoreManualScanExample(this)) {
            ManualScanExampleActivity.startActivity(this);
        } else {
            startActivity(new Intent(this, InStoreShoppingActivity.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        Button loginLogoutButton = findViewById(R.id.loginLogout);
        if (loginLogoutButton != null) {
            SsUser ssUser = SwiftShopperSdk.getUser();
            if (ssUser == null) {
                loginLogoutButton.setText(R.string.login);
            } else if (ssUser.isAnonymous()) {
                loginLogoutButton.setText(R.string.login_with_social_media);
            } else {
                loginLogoutButton.setText(R.string.logout);
            }

            if (ssUser == null) {
                findViewById(R.id.shopSingleStore).setVisibility(View.INVISIBLE);
                findViewById(R.id.shopChooseStore).setVisibility(View.INVISIBLE);
                findViewById(R.id.orderHistory).setVisibility(View.INVISIBLE);
                findViewById(R.id.shoppingList).setVisibility(View.INVISIBLE);
                findViewById(R.id.profileButton).setVisibility(View.INVISIBLE);
            } else {
                findViewById(R.id.shopSingleStore).setVisibility(View.VISIBLE);
                findViewById(R.id.shopChooseStore).setVisibility(View.VISIBLE);
                findViewById(R.id.orderHistory).setVisibility(View.VISIBLE);
                findViewById(R.id.shoppingList).setVisibility(View.VISIBLE);
                if (ssUser.isAnonymous()) {
                    findViewById(R.id.profileButton).setVisibility(View.INVISIBLE);
                } else {
                    findViewById(R.id.profileButton).setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void onLoginLogoutClicked(View v) {
        System.out.println("On Login/Logout Clicked");
        SsUser ssUser = SwiftShopperSdk.getUser();
        if (ssUser == null || ssUser.isAnonymous()) {
            startActivity(new Intent(MainActivity.this,
                    LoginActivity.class));
        } else {
            SwiftShopperSdk.logout(()->{
                ((Button)findViewById(R.id.loginLogout)).setText(R.string.login);
                findViewById(R.id.shopSingleStore).setVisibility(View.INVISIBLE);
                findViewById(R.id.shopChooseStore).setVisibility(View.INVISIBLE);
                findViewById(R.id.orderHistory).setVisibility(View.INVISIBLE);
                findViewById(R.id.shoppingList).setVisibility(View.INVISIBLE);
                findViewById(R.id.profileButton).setVisibility(View.INVISIBLE);
            });
        }
    }

    public void onShopSingleStoreClicked(View v) {
        System.out.println("On Shop Single Store Clicked");
        SwiftShopperSdk.enterStore(50, new SwiftShopperSdk.EnterStoreListener() {
            public void onSuccess() {
                startActivity(new Intent(MainActivity.this,
                        InStoreShoppingActivity.class));
            }
            public void onFailure() {

            }
        });
    }

    public void onManualScanExampleClicked(View v) {
        System.out.println("On Manual Scan Example Clicked");

        // The ManualScanExampleActivity show-cases how to call into the
        // SwiftShopperSdk.scanItemToCart(...) method. We have to be in a store in order
        // to successfully use the SwiftShopperSdk.scanItemToCart(...) method. Let's enter
        // the test store and then start the ManualScanExampleActivity upon completion of
        // entering the store.
        SwiftShopperSdk.enterStore(50, new SwiftShopperSdk.EnterStoreListener() {
            public void onSuccess() {
                ManualScanExampleActivity.startActivity(MainActivity.this);
            }
            public void onFailure() {

            }
        });

    }

    public void onShopChooseStoreClicked(View v) {
        System.out.println("On Shop Choose Store Clicked");
        // If the user is not checked into a store, show the SelectMarketActivity, which
        // prompts the user to choose a nearby store to shop at. The SelectMarketActivity
        // then starts the InStoreShoppingActivity after the user chooses a nearby store.
        if (SwiftShopperSdk.getStore() == null) {
            startActivity(new Intent(this, SelectMarketActivity.class));
        } else {
            startActivity(new Intent(this, InStoreShoppingActivity.class));
        }
    }

    public void onOrderHistoryClicked(View v) {
        startActivity(new Intent(this, OrderHistoryActivity.class));
    }

    public void onShoppingListsClicks(View v) {
        startActivity(new Intent(this, ShoppingListActivity.class));
    }

    public void onProfileClicked(View v) {
        startActivity(new Intent(this, ProfileActivity.class));
    }
}

