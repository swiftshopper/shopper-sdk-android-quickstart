package com.swiftshopper.sdk.quickstart;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.StringRes;
import androidx.core.app.NavUtils;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;


public class ToolbarHelper {

    public static void setTitle(AppCompatActivity activity, @StringRes int stringResId) {
        ActionBar actionBar = null;
        if (activity != null && (actionBar = activity.getSupportActionBar()) != null) {
            actionBar.setTitle(stringResId);
        }
    }

    public static void setSubTitle(AppCompatActivity activity, @StringRes int stringResId) {
        ActionBar actionBar;
        if (activity != null && (actionBar = activity.getSupportActionBar()) != null) {
            actionBar.setSubtitle(stringResId);
        }
    }


    public static void addCustomToolbar(AppCompatActivity activity, String leftButtonText,
                                        String titleText, View.OnClickListener onLeft) {
        addCustomToolbar(activity, leftButtonText, onLeft, titleText, null);
    }

    public static void addCustomToolbar(AppCompatActivity activity, String leftButtonText,
                                        View.OnClickListener onLeft, String titleText,
                                        View rightView) {
        addCustomToolbar(activity, onLeft);

        View customView = activity.getSupportActionBar().getCustomView();

        TextView leftText = customView.findViewById(R.id.custom_toolbar_left_text);
        leftText.setText(leftButtonText);

        TextView titleTextView = customView.findViewById(R.id.custom_toolbar_title);
        titleTextView.setText(titleText);


        if (rightView != null) {
            FrameLayout frameLayout = customView.findViewById(R.id.custom_toolbar_right_view);
            frameLayout.addView(rightView, FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT);
        }
    }
    public static void addCustomToolbar(AppCompatActivity activity, String leftButtonText,
                                        String titleText) {
        addCustomToolbar(activity, leftButtonText, titleText);

    }
    private static void addCustomToolbar(AppCompatActivity activity, View.OnClickListener onLeft) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.custom_text_on_left_toolbar);
        actionBar.setElevation(0);
        View actionBarCustomView = actionBar.getCustomView();

        if (onLeft == null) {
            onLeft = (v) -> {
                navigateBack(activity);
            };
        }
        TextView leftText = actionBarCustomView.findViewById(R.id.custom_toolbar_left_text);

        leftText.setOnClickListener(onLeft);
    }

    static void navigateBack(Activity activity) {
        Intent upIntent = activity.getParentActivityIntent();
        if (upIntent == null) {
            activity.onBackPressed();
        } else {
            NavUtils.navigateUpTo(activity, upIntent);
        }
    }

    public static void addStandardToolbarWithBack(AppCompatActivity activity) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public static void addStandardToolbarWithBack(AppCompatActivity activity,
                                                  int subtitleStringResourceId) {
        if (activity != null) {
            String subtitle = activity.getString(subtitleStringResourceId);
            addStandardToolbarWithBack(activity, subtitle);
        }
    }

    public static void addStandardToolbarWithBack(AppCompatActivity activity, String subtitle) {
        Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setSubtitle(subtitle);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

}
