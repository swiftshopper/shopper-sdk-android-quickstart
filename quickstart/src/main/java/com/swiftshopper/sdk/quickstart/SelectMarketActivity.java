package com.swiftshopper.sdk.quickstart;

import android.content.Intent;
import androidx.annotation.StringRes;

import com.swiftshopper.sdk.ui.view.SsSelectMarketView;

public class SelectMarketActivity extends AppSecondaryActivity implements
        SsSelectMarketView.CheckInListener {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_select_market;
    }

    @Override
    protected void onCreateSecondaryContent() {
        super.onCreateSecondaryContent();
        SsSelectMarketView ssSelectMarketView = findViewById(R.id.selectMarketView);
        ssSelectMarketView.setCheckInListener(this);
    }


    @Override
    @StringRes
    protected int getSubtitle() {
        return R.string.subtitle_activity_select_market;
    }
    @Override
    public void onCheckInComplete() {
        startActivity(new Intent(this, InStoreShoppingActivity.class));
    }
}
