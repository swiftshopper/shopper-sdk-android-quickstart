package com.swiftshopper.sdk.quickstart;

public class ProfileActivity extends AppSecondaryActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_profile;
    }

}
