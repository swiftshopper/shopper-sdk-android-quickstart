package com.swiftshopper.sdk.quickstart;

import android.content.res.ColorStateList;
import android.os.Bundle;
import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public abstract class AppSecondaryActivity extends AppCompatActivity {

    View mLoadingView = null;

    FloatingActionButton mLowerLeftFloatingActionButton = null;
    FloatingActionButton mLowerRightFloatingActionButton = null;

    protected void showLoading() {
        if (mLoadingView == null) {
            mLoadingView = findViewById(R.id.loading);
        }
        if (mLoadingView == null) {
            Log.w(getClass().getName(), "No loading view in layout.");
        } else {
            mLoadingView.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissLoading() {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);
        addSecondaryToolbar();
        onCreateSecondaryContent();
        onInitFloatingActionButtons();
    }

    protected void onInitFloatingActionButtons() {
        onInitLowerLeftFloatingActionButton();
        onInitLowerRightFloatingActionButton();
    }

    protected void onInitLowerLeftFloatingActionButton() {
        mLowerLeftFloatingActionButton = findViewById(R.id.lower_left_fab);
        int lowerLeftActionButtonDrawable = getLowerLeftActionButtonDrawable();
        if (lowerLeftActionButtonDrawable != -1) {
            mLowerLeftFloatingActionButton.setImageDrawable(getResources().getDrawable(
                    lowerLeftActionButtonDrawable, null));

            int tintColorStateListResId = getLowerLeftActionButtonDrawableColorTintsResId();
            if (tintColorStateListResId != -1) {
                ColorStateList tintColorStateList =
                        getResources().getColorStateList(tintColorStateListResId);
                if (tintColorStateList != null) {
                    mLowerLeftFloatingActionButton.setImageTintList(tintColorStateList);
                }
            }
            mLowerLeftFloatingActionButton.setOnClickListener((v)->{
                onLowerLeftActionButtonClicked();
            });

            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams)
                    mLowerLeftFloatingActionButton.getLayoutParams();

            layoutParams.bottomMargin = getFloatingActionButtonBottomMargin();
            mLowerLeftFloatingActionButton.show();
        }
    }

    protected @DrawableRes int getLowerLeftActionButtonDrawable() {
        return -1;
    }

    protected int getLowerLeftActionButtonDrawableColorTintsResId() {
        return R.color.floating_action_button_image_tint_color;
    }

    protected void onLowerLeftActionButtonClicked() {

    }

    protected void onInitLowerRightFloatingActionButton() {
        mLowerRightFloatingActionButton = findViewById(R.id.lower_right_fab);
        int lowerRightActionButtonDrawable = getLowerRightActionButtonDrawable();
        if (lowerRightActionButtonDrawable != -1) {
            mLowerRightFloatingActionButton.setImageDrawable(getResources().getDrawable(
                    lowerRightActionButtonDrawable, null));

            int tintColorStateListResId = getLowerRightActionButtonDrawableColorTintsResId();
            if (tintColorStateListResId != -1) {
                ColorStateList tintColorStateList =
                        getResources().getColorStateList(tintColorStateListResId);
                if (tintColorStateList != null) {
                    mLowerRightFloatingActionButton.setImageTintList(tintColorStateList);
                }
            }
            mLowerRightFloatingActionButton.setOnClickListener((v)->{
                onLowerRightActionButtonClicked();
            });
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams)
                    mLowerRightFloatingActionButton.getLayoutParams();

            layoutParams.bottomMargin = getFloatingActionButtonBottomMargin();

            mLowerRightFloatingActionButton.show();
        }
    }

    protected int getFloatingActionButtonBottomMargin() {
        return getResources().getDimensionPixelSize(R.dimen.fab_margin_bottom);
    }

    protected @DrawableRes int getLowerRightActionButtonDrawable() {
        return -1;
    }

    protected int getLowerRightActionButtonDrawableColorTintsResId() {
        return R.color.floating_action_button_image_tint_color;
    }

    protected void onLowerRightActionButtonClicked() {

    }

    protected abstract int getContentLayoutId();

    protected void onCreateSecondaryContent() {
        ViewGroup secondaryContentContainer = findViewById(R.id.app_content);
        getLayoutInflater().inflate(getContentLayoutId(), secondaryContentContainer);
    }

    @StringRes
    protected int getSubtitle() {
        return -1;
    }

    protected void addSecondaryToolbar() {
        int subtitleResourceId = getSubtitle();
        if (subtitleResourceId != -1) {
            ToolbarHelper.addStandardToolbarWithBack(this, subtitleResourceId);
        } else {
            ToolbarHelper.addStandardToolbarWithBack(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ToolbarHelper.navigateBack(this);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }
}
