package com.swiftshopper.sdk.quickstart;

import android.content.Intent;
import android.os.Bundle;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.login.LoginListener;
import com.swiftshopper.sdk.ui.view.SsListView;

public class ShoppingListActivity extends BackIconAndCartActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_shopping_list;
    }

    @Override
    protected void addSecondaryToolbar() {
        super.addSecondaryToolbar();

        if (SwiftShopperSdk.getStore() != null) {
            ToolbarHelper.setTitle(this, R.string.title_activity_in_store_shopping);
            ToolbarHelper.setSubTitle(this, R.string.subtitle_acitity_shopping_list_when_shopping);
        }
    }

    @Override
    public void onCreateSecondaryContent() {
        super.onCreateSecondaryContent();
    }

    @Override
    protected int getLowerRightActionButtonDrawable() {
        return R.drawable.ico_tag;
    }

    @Override
    protected void onLowerRightActionButtonClicked() {
        startActivity(new Intent(this, DealsActivity.class));
    }

    @Override
    protected int getFloatingActionButtonBottomMargin() {
        return getResources().getDimensionPixelSize(R.dimen.shopping_list_fab_margin_bottom);
    }

    @Override
    protected void onStart() {
        System.out.println("Shopping List Activity (onStart) intent = " + getIntent());
        SwiftShopperSdk.importSharedList(this);
        super.onStart();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }
}
