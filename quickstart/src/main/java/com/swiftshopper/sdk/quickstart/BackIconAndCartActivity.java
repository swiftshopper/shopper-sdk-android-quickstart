package com.swiftshopper.sdk.quickstart;

import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.ui.view.SsCartButtonView;

public abstract class BackIconAndCartActivity extends AppSecondaryActivity {

    private void onCartButtonClick() {
        startActivity(new Intent(this, CartActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (SwiftShopperSdk.getStore() != null) {
            SsCartButtonView ssCartButtonView = new SsCartButtonView(this);
            MenuItem cartItem = menu.add("cart");
            cartItem.setActionView(ssCartButtonView);
            cartItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            ssCartButtonView.setOnClickListener((v)->{
                onCartButtonClick();
            });
        }
        return super.onCreateOptionsMenu(menu);
    }
}
