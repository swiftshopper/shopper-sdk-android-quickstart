package com.swiftshopper.sdk.quickstart;

import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import android.widget.Toast;

import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.ui.view.SsLoginView;

public class LoginActivity extends AppSecondaryActivity implements SsLoginView.Delegate {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_login;
    }

    @Override
    protected void onCreateSecondaryContent() {
        super.onCreateSecondaryContent();
        SsLoginView ssLoginView = findViewById(R.id.loginView);
        ssLoginView.setLoginDelegate(this);
        // Hide the 'Skip Login' option if the user has previously logged in anonymously (in other
        // words if the user has already explicitly chosen to 'Skip Login')
        SsUser ssUser = SwiftShopperSdk.getUser();
        if (ssUser != null && ssUser.isAnonymous()) {
            ssLoginView.setAllowSkipLogin(false);
        }
    }

    @Override
    public void onLoginSuccess() {
        dismissLoading();
        if (!SwiftShopperSdk.ACTION_LOGIN_FOR_PRIVATE_SHARE.equals(getIntent().getAction())) {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
        dismissLoading();

        if (SwiftShopperSdk.getStore() == null) {
            // The app needs to check into a store...perhaps
            // start your Select Market activity
        } else {
            // The app is already checked into a store...perhaps
            // start your In-Store Shopping activity
        }
    }

    @Override
    public void onLoginFailure(Throwable throwable) {
        Toast.makeText(this, "Login failed:" + throwable, Toast.LENGTH_SHORT).show();
        dismissLoading();
    }

    @Override
    public void proceedAnonymously(Runnable runMeToProceed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getTitle());
        builder.setMessage(R.string.login_skip_confirmation_message);
        // Reverse psychology: "Going Back" or "Cancelling" is illustrated as the positive choice
        builder.setPositiveButton(R.string.login_skip_confirmation_cancel, (d, i)->{

        });
        builder.setNegativeButton(R.string.login_skip_confirmation_yes, (d,i)->{
            showLoading();
            runMeToProceed.run();
        });
        builder.create().show();
    }
}
