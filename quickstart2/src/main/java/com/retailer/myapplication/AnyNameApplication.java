package com.retailer.myapplication;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.core.content.res.ResourcesCompat;

import com.crashlytics.android.Crashlytics;
import com.swiftshopper.sdk.SsConfig;
import com.swiftshopper.sdk.SwiftShopperSdk;

import io.fabric.sdk.android.Fabric;

public class AnyNameApplication extends Application {

    public BroadcastReceiver mSwiftShopperBroadCastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent mainActivityIntent = new Intent(getApplicationContext(),
                    MainActivity.class);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainActivityIntent);
        }
    };

    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        SwiftShopperSdk.initialize(new SsConfig.Builder(this)
                .setRetailerSdkKey("w9mn{dn7lPIF37Yb#>fh)By^Q|AoA3w8<SGXbd2gmPF2|")
                .setSmallLogo(R.drawable.fresh_farm_store_icon)
                .setLargeLogo(R.drawable.fresh_farm_store_icon)
                .setActivityTheme(R.style.AppTheme_NoActionBar)
                .setThemeColor(ResourcesCompat.getColor(
                        getResources(), R.color.colorPrimary, null))
                .build());

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SwiftShopperSdk.ACTION_CHECKOUT_COMPLETE);
        registerReceiver(mSwiftShopperBroadCastReceiver, intentFilter);

        Intent startingActivity;
        if (SwiftShopperSdk.getStore() == null) {
            startingActivity = new Intent(getApplicationContext(), MainActivity.class);
        } else {
            startingActivity = new Intent(getApplicationContext(), ShopListDealsActivity.class);
        }
        startingActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startingActivity);
    }

    @Override
    public void onTerminate() {
        unregisterReceiver(mSwiftShopperBroadCastReceiver);
        super.onTerminate();
    }
}
