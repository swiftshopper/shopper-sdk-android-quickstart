package com.retailer.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.swiftshopper.sdk.ui.view.SsLoginView;

import timber.log.Timber;

public class LoginActivity extends AppCompatActivity implements SsLoginView.Delegate {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        SsLoginView ssLoginView = findViewById(R.id.login_view);
        ssLoginView.setLoginDelegate(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.v("LoginActivity: instance = " + this);
    }


    void showLoading() {
        findViewById(R.id.loading_view).setVisibility(View.VISIBLE);
    }

    void dismissLoading() {
        findViewById(R.id.loading_view).setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onLoginSuccess() {
        dismissLoading();
        finish();
    }

    @Override
    public void onLoginFailure(Throwable throwable) {
        Toast.makeText(this, "Login failed:" + throwable,
                Toast.LENGTH_SHORT).show();
        dismissLoading();
    }

    @Override
    public void proceedAnonymously(Runnable runMeToProceed) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getTitle());
        builder.setMessage(R.string.login_skip_confirmation_message);
        // Reverse psychology: "Going Back" or "Cancelling" is illustrated as
        // the positive choice
        builder.setPositiveButton(R.string.login_skip_confirmation_cancel, (d, i)->{
        });
        builder.setNegativeButton(R.string.login_skip_confirmation_yes, (d,i)->{
            showLoading();
            runMeToProceed.run();
        });
        builder.create().show();
    }
}
