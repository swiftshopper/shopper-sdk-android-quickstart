package com.retailer.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.swiftshopper.sdk.SsStoreDetail;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.ui.view.SsDealsView;
import com.swiftshopper.sdk.ui.view.SsListView;
import com.swiftshopper.sdk.ui.view.SsShopOnTheFlyView;

public class ShopListDealsActivity extends AppCompatActivity {
    FrameLayout mFrameLayout = null;
    SsShopOnTheFlyView mSsShopOnTheFlyView = null;
    SsListView mSsListView = null;
    SsDealsView mSsDealsView = null;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            mFrameLayout.removeAllViews();
            switch (item.getItemId()) {
                case R.id.navigation_self_scan:
                    if (mSsShopOnTheFlyView == null) {
                        mSsShopOnTheFlyView = new SsShopOnTheFlyView(ShopListDealsActivity.this);
                    }
                    mFrameLayout.addView(mSsShopOnTheFlyView);
                    return true;
                case R.id.navigation_shopping_list:
                    if (mSsListView == null) {
                        mSsListView = new SsListView(ShopListDealsActivity.this);
                    }
                    mFrameLayout.addView(mSsListView);
                    return true;
                case R.id.navigation_shopping_deals:
                    if (mSsDealsView == null) {
                        mSsDealsView = new SsDealsView(ShopListDealsActivity.this);
                    }
                    mFrameLayout.addView(mSsDealsView);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_list_deals);

        mFrameLayout = findViewById(R.id.shop_list_deals_content);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_self_scan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                SsStoreDetail ssStoreDetail = SwiftShopperSdk.getStore();
                if (ssStoreDetail != null) {
                    String storeName = ssStoreDetail.getName();
                    if (storeName != null) {
                        storeName = storeName.trim();
                        if (storeName.length() > 0) {
                            getSupportActionBar().setSubtitle(storeName);
                            return;
                        }
                    }
                }
                handler.postDelayed(this, 500);
            }
        }, 500);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.self_scan_menu, menu);
        menu.findItem(R.id.action_cart).getActionView().setOnClickListener((v)->{
            startActivity(new Intent(ShopListDealsActivity.this, CartActivity.class));
        });
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getTitle());
        builder.setMessage(R.string.exit_store_confirm);
        // Reverse psychology: "Going Back" or "Cancelling" is rendered
        // as the positive choice
        builder.setPositiveButton(R.string.exit_store_confirm_no, (d, i)->{
        });
        builder.setNegativeButton(R.string.exit_store_confirm_yes, (d,i)->{
            SwiftShopperSdk.exitStore(()->{
                startActivity(
                        new Intent(ShopListDealsActivity.this,
                                MainActivity.class).addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK));
            });
        });
        builder.create().show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.order_history_menu_item) {
            startActivity(new Intent(this, OrderHistoryActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

}
