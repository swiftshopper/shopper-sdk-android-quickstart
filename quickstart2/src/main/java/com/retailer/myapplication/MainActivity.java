package com.retailer.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.login.LoginListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setSubtitle(R.string.subtitle_activity_main);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.shop_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goShop();
            }
        });

        WebView webView = findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                webView.loadUrl(url);
                return true;
            }
        });
        webView.loadUrl("https://kosherfamily.com");
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        MenuItem loginItem = menu.findItem(R.id.action_login);
        SsUser ssUser = SwiftShopperSdk.getUser();
        String ssUserPhotoUrl = (ssUser == null) ? null : ssUser.getUserPhotoUrl();
        ssUserPhotoUrl = (ssUserPhotoUrl == null) ? null : ssUserPhotoUrl.trim();
        if (ssUserPhotoUrl != null && ssUserPhotoUrl.length() > 0) {
            Glide.with(this).load(ssUserPhotoUrl).asBitmap().fitCenter().atMost().into(new SimpleTarget<Bitmap>(96, 96) {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    loginItem.setIcon(new BitmapDrawable(getResources(), resource));
                }
            });
        }

        return true;
    }

    private void loginAnonymousThenSelectMarket() {
        if (SwiftShopperSdk.getUser() == null) {
            SwiftShopperSdk.loginAnonymous(new LoginListener() {
                @Override
                public void onLoginSuccess() {
                    goShop();
                }

                @Override
                public void onLoginFailure(Throwable throwable) {
                    Toast.makeText(MainActivity.this, "Error logging into store.", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void goShop() {
        if (SwiftShopperSdk.getUser() == null) {
            loginAnonymousThenSelectMarket();
        } else {
            startActivity(new Intent(this, SelectMarketActivity.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.shop_menu_item) {
            goShop();
        } else if (id == R.id.order_history_menu_item) {
            startActivity(new Intent(this, OrderHistoryActivity.class));
        } else if (id == R.id.shopping_list_menu_item) {
            startActivity(new Intent(this, ShoppingListActivity.class));
        } else if (id == R.id.nearby_deals_menu_item) {
            startActivity(new Intent(this, DealsActivity.class));
        }else if (id == R.id.action_login) {
            SsUser ssUser = SwiftShopperSdk.getUser();
            if (ssUser == null || ssUser.isAnonymous()) {
                startActivity(new Intent(this, LoginActivity.class));
            } else {
                startActivity(new Intent(this, ProfileActivity.class));
            }
        }

        return super.onOptionsItemSelected(item);
    }

}
