package com.retailer.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.app.AlertDialog;
import android.view.Menu;
import android.view.View;

import com.swiftshopper.sdk.SsStoreDetail;
import com.swiftshopper.sdk.SwiftShopperSdk;

public class SelfScanShoppingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_scan_shopping);
        findViewById(R.id.cart_widget).setOnClickListener((v)->{
            startActivity(new Intent(SelfScanShoppingActivity.this, CartActivity.class));
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    SsStoreDetail ssStoreDetail = SwiftShopperSdk.getStore();
                    if (ssStoreDetail != null) {
                        String storeName = ssStoreDetail.getName();
                        if (storeName != null) {
                            storeName = storeName.trim();
                            if (storeName.length() > 0) {
                                getSupportActionBar().setSubtitle(storeName);
                                return;
                            }
                        }
                    }
                    handler.postDelayed(this, 500);
                }
            }, 500);



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getTitle());
        builder.setMessage(R.string.exit_store_confirm);
        // Reverse psychology: "Going Back" or "Cancelling" is rendered
        // as the positive choice
        builder.setPositiveButton(R.string.exit_store_confirm_no, (d, i)->{
        });
        builder.setNegativeButton(R.string.exit_store_confirm_yes, (d,i)->{
            SwiftShopperSdk.exitStore(()->{
                startActivity(
                        new Intent(SelfScanShoppingActivity.this,
                                MainActivity.class).addFlags(
                                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                        Intent.FLAG_ACTIVITY_NEW_TASK));
            });
        });
        builder.create().show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.self_scan_menu, menu);
        menu.findItem(R.id.action_cart).getActionView().setOnClickListener((v)->{
            startActivity(new Intent(SelfScanShoppingActivity.this, CartActivity.class));
        });
        return true;
    }
}
