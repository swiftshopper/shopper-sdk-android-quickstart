package com.swiftshopper.sdk.quickstart4;

public class OrderHistoryActivity extends AppSecondaryActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_order_history;
    }

    @Override
    protected boolean showMenuInsteadOfBack() {
        return false;
    }

    @Override
    protected String getSubtitle() {
        return getString(R.string.subtitle_activity_order_history);
    }
}
