package com.swiftshopper.sdk.quickstart4;

public class CartActivity extends AppSecondaryActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_cart;
    }

    @Override
    protected boolean showMenuInsteadOfBack() {
        return false;
    }
}
