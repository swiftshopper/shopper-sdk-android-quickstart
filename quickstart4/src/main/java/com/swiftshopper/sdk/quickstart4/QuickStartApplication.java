package com.swiftshopper.sdk.quickstart4;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.core.content.ContextCompat;

import com.swiftshopper.sdk.SsConfig;
import com.swiftshopper.sdk.SwiftShopperSdk;

public class QuickStartApplication extends Application {

    public BroadcastReceiver mSsCheckoutCompleteListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent mainActivityIntent = new Intent(getApplicationContext(), SelectMarketActivity.class);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(mainActivityIntent);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        initializeSwiftShopperSdk();
        listenForSwiftShopperCheckoutComplete();
    }

    private void listenForSwiftShopperCheckoutComplete() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SwiftShopperSdk.ACTION_CHECKOUT_COMPLETE);

        registerReceiver(mSsCheckoutCompleteListener,
                intentFilter);
    }

    private void initializeSwiftShopperSdk() {
        // Swift Shopper SDK initialization
        SsConfig ssConfig = new SsConfig.Builder(this)
                .setRetailerSdkKey("w9mn{dn7lPIF37Yb#>fh)By^Q|AoA3w8<SGXbd2gmPF2|")
                .setActivityTheme(R.style.AppTheme_NoActionBar)
                .setThemeColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setLargeLogo(R.drawable.layer_list_logo_with_non_transparent_bg)
                .setSmallLogo(R.drawable.layer_list_logo_with_non_transparent_bg).build();
        SwiftShopperSdk.initialize(ssConfig);

    }

    @Override
    public void onTerminate() {
        unregisterReceiver(mSsCheckoutCompleteListener);
        super.onTerminate();
    }
}
