package com.swiftshopper.sdk.quickstart4;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.core.app.NavUtils;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import com.swiftshopper.sdk.ui.view.custom.CustomToolbar;


public class ToolbarHelper {

    static void navigateBack(Activity activity) {
        Intent upIntent = activity.getParentActivityIntent();
        if (upIntent == null) {
            activity.onBackPressed();
        } else {
            NavUtils.navigateUpTo(activity, upIntent);
        }
    }

    public static void addStandardToolbarWithBack(AppCompatActivity activity) {
        CustomToolbar toolbar = activity.findViewById(R.id.custom_toolbar);
        toolbar.setLogo(R.drawable.logo_small, null);
        toolbar.setLeft1(R.drawable.ic_back, (v)->{activity.onBackPressed();});
        configureMenuDrawer(activity, false);
    }

    public static void addStandardToolbarWithMenu(AppCompatActivity activity) {
        CustomToolbar toolbar = activity.findViewById(R.id.custom_toolbar);
        toolbar.setLogo(R.drawable.logo_small, null);
        configureMenuDrawer(activity, true);
    }


    public static void addStandardToolbarWithBack(AppCompatActivity activity,
                                                  int subtitleStringResourceId) {
        if (activity != null) {
            String subtitle = activity.getString(subtitleStringResourceId);
            addStandardToolbarWithBack(activity, subtitle);
        }
    }

    public static void addStandardToolbarWithMenu(AppCompatActivity activity,
                                                  int subtitleStringResourceId) {
        if (activity != null) {
            String subtitle = activity.getString(subtitleStringResourceId);
            addStandardToolbarWithMenu(activity, subtitle);
        }
    }

    public static void addStandardToolbarWithBack(AppCompatActivity activity, String subtitle) {
        CustomToolbar toolbar = activity.findViewById(R.id.custom_toolbar);
        toolbar.setTitle(subtitle, null);
        toolbar.setLogo(R.drawable.logo_small, null);
        toolbar.setLeft1(R.drawable.ic_back, (v)->{navigateBack(activity);});
        configureMenuDrawer(activity, false);
    }

    public static void addStandardToolbarWithMenu(AppCompatActivity activity, String subtitle) {
        CustomToolbar toolbar = activity.findViewById(R.id.custom_toolbar);
        toolbar.setTitle(subtitle, null);
        toolbar.setLogo(R.drawable.logo_small, null);
        configureMenuDrawer(activity, true);
    }

    public static void configureMenuDrawer(AppCompatActivity activity,
                                           boolean changeLeftToolbarIconToMenuIcon) {
        CustomToolbar toolbar = activity.findViewById(R.id.custom_toolbar);

        DrawerLayout menuDrawer = activity.findViewById(R.id.drawer_layout);
        menuDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View view, float v) {
            }

            @Override
            public void onDrawerOpened(@NonNull View view) {
                view.findViewById(R.id.closeMenu).setOnClickListener((v)->{
                    if (menuDrawer.isDrawerOpen(GravityCompat.START)) {
                        menuDrawer.closeDrawer(GravityCompat.START);
                    }
                });
            }

            @Override
            public void onDrawerClosed(@NonNull View view) {

            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });


        if (changeLeftToolbarIconToMenuIcon) {
            toolbar.setLeft1(R.drawable.ic_menu, (v) -> {
                if (menuDrawer.isDrawerOpen(GravityCompat.START)) {
                    menuDrawer.closeDrawer(GravityCompat.START);
                } else {
                    menuDrawer.openDrawer(GravityCompat.START);
                }
            });
        }
    }
}
