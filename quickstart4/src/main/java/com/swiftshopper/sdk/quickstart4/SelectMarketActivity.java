package com.swiftshopper.sdk.quickstart4;

import android.content.Intent;

import androidx.core.app.ActivityOptionsCompat;

import com.swiftshopper.sdk.ui.view.SsSelectMarketView;

public class SelectMarketActivity extends AppSecondaryActivity implements
        SsSelectMarketView.CheckInListener {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_select_market;
    }

    @Override
    protected void createSecondaryContent() {
        super.createSecondaryContent();
        // Listen for when the user selects a store
        SsSelectMarketView ssSelectMarketView = findViewById(R.id.selectMarketView);
        ssSelectMarketView.setCheckInListener(this);
    }

    @Override
    protected String getSubtitle() {
        return getString(R.string.subtitle_activity_select_market);
    }

    @Override
    public void onCheckInComplete() {
        // Start the InStoreShoppingActivity using fade-in/fade-out animation
        ActivityOptionsCompat shoppingActivityOptions =
                ActivityOptionsCompat.makeCustomAnimation(
                        this, android.R.anim.fade_in, android.R.anim.fade_out);

        startActivity(new Intent(this, InStoreShoppingActivity.class),
                shoppingActivityOptions.toBundle());
        finish();
    }
}
