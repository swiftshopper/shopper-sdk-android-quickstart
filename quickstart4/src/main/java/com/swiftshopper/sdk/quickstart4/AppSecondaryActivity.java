package com.swiftshopper.sdk.quickstart4;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.LayoutRes;
import com.google.android.material.navigation.NavigationView;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.swiftshopper.sdk.SsStoreDetail;
import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.nativeLogin.ui.view.SsNativeLoginView;
import com.swiftshopper.sdk.ui.view.SsLoginView;
import com.swiftshopper.sdk.util.GlideUtils;

/**
 * Base class for all Activities, other than the MainActivity.
 */
public abstract class AppSecondaryActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // Reference to the loading overlay that is declared within the layout file. This reference
    // is needed so that the logic in this class can hide or show the loading animation as prudent.
    View mLoadingView = null;

    // Reference to the image view and the two text fields that are rendered at the top of the side
    // menu. These references are needed so that the logic in this class can render the user's
    // profile information into these views if the user is logged in, otherwise some promotional
    // details about the app itself is rendered in these fields.
    ImageView mSideMenuHeaderTopImage = null;
    TextView mSideMenuHeaderTitleText = null;
    TextView mSideMenuHeaderSubtitleText = null;

    protected void showLoading() {
        if (mLoadingView == null) {
            mLoadingView = findViewById(R.id.loading);
        }
        if (mLoadingView == null) {
            Log.w(getClass().getName(), "No loading view in layout.");
        } else {
            mLoadingView.setVisibility(View.VISIBLE);
        }
    }

    protected void dismissLoading() {
        if (mLoadingView != null) {
            mLoadingView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_side_menu);
        configureCustomToolbar();
        createSecondaryContent();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setNavigationBarColor(
                ResourcesCompat.getColor(getResources(),R.color.colorPrimaryDark, null));

        // Update the user data in the top of the menu in case this activity is resuming
        // after the initial global layout occurred for the menu. Without doing this, the
        // top of the side menu will not update in the event that a user logs in or logs
        // out after this activity is displayed and then subsequently redisplayed after the
        // user might have logged in or logged out in between the instances when this activity
        // was in the foreground.
        //
        // Example:
        // (Time 1) This activity appears but user skipped login.
        //          NOTE: The side menu's header does not illustrate the user's profile data since
        //                the user is not logged in.
        // (Time 2) User navigates to the cart
        // (Time 3) From the cart activity, user slides the menu from left onto the screen
        //          NOTE: Even though the cart activity has a back button in the upper left of
        //                the toolbar, instead of a menu button, a savvy user could drag open
        //                the side menu from the left edge of the screen, and then choose the
        //                "Log In" option from the side menu while on the Cart Activity.
        // (Time 4) User logs in via the "Log In" menu option on the cart activity's side menu
        // (Time 5) User navigates back to this activity
        //          RAMIFICATION: Without calling showUserProfileDataInMenu(), this activity's
        //          instance of the side menu would otherwise continue to render the header
        //          as the header was originally configured back in (Time 1) whereby the header
        //          illustrates that the user is not logged in.
        showUserProfileDataInMenu();
        configureSideMenu();
    }

    protected void configureSideMenu() {
        NavigationView sideMenuNavigationView = (NavigationView) findViewById(R.id.nav_view);
        // Listen for when items in the side menu are tapped. Thanks to this listener, the
        // onNavigationItemSelected(MenuItem item) method will be called when an item is tapped
        // within the side-menu.
        sideMenuNavigationView.setNavigationItemSelectedListener(this);

        Menu sideMenu = sideMenuNavigationView.getMenu();
        if (sideMenu != null) { // Sanity Check
            // Hide either the "Log Out" or the "Log In" menu option based on whether the user
            // is currently logged in
            SsUser ssUser = SwiftShopperSdk.getUser();
            if (ssUser == null || ssUser.isAnonymous()) {
                // User is not logged in. hide "Log Out" menu option
                sideMenu.findItem(R.id.side_menu_logout).setVisible(false);
                sideMenu.findItem(R.id.side_menu_login).setVisible(true);
            } else {
                // User is logged in. Hide "Log In" menu option
                sideMenu.findItem(R.id.side_menu_logout).setVisible(true);
                sideMenu.findItem(R.id.side_menu_login).setVisible(false);
            }

            // Hide the "Exit Store" menu option if the user is not currently shopping at a
            // particular store.
            SsStoreDetail ssStoreDetail = SwiftShopperSdk.getStore();
            if (ssStoreDetail == null) {
                // User is not checked into a store. Hide "Exit Store" menu option
                sideMenu.findItem(R.id.side_menu_exit_store).setVisible(false);
            } else {
                sideMenu.findItem(R.id.side_menu_exit_store).setVisible(true);
            }

            // Get a reference to the header views within the side menu. Note that Android lazily
            // adds these to the view hierarchy. Hence we cannot access then until after Android
            // decides to lay them out. Hence, listen for the side menu to be globally lain out
            // so that we can then grab references to the image view and the two text views that
            // are drawn at the top of the side menu. The user's profile information will be
            // illustrated in these views if the user is logged in, otherwise, some promotional
            // information about the app itself is rendered in these views.
            sideMenuNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            mSideMenuHeaderTopImage =
                                    sideMenuNavigationView.findViewById(R.id.side_menu_top_image);
                            mSideMenuHeaderTitleText =
                                    sideMenuNavigationView.findViewById(R.id.side_menu_top_text_1);
                            mSideMenuHeaderSubtitleText =
                                    sideMenuNavigationView.findViewById(R.id.side_menu_top_text_2);
                            sideMenuNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                            showUserProfileDataInMenu();
                        }
                    });
        }
    }


    private void showUserProfileDataInMenu() {
        SsUser ssUser = SwiftShopperSdk.getUser();
        String ssUserProfilePhotoUrl;

        // If there is an ssUser object and the ssUser object is not an anonymous user (anonymous
        // users are users that have elected to "Skip" login). Hence if the user has not logged
        // in, and if the user has not yet elected to "skip" login, the ssUser object will be null.
        // Then if the ssUser object is not null yet the ssUser object indicates that the user is
        // anonymous, that means that user has elected to "Skip" login.
        if (ssUser != null && !ssUser.isAnonymous() &&
                mSideMenuHeaderTitleText != null && // <- Making sure global layout has happened
                mSideMenuHeaderTopImage != null && // <- Making sure global layout has happened
                mSideMenuHeaderSubtitleText != null) { // <- Making sure global layout has happened

            // Render the user's profile picture in the image view that is located at the top of
            // the menu.
            if ((ssUserProfilePhotoUrl=ssUser.getUserPhotoUrl()) != null) {
                GlideUtils.loadImageUrl(mSideMenuHeaderTopImage,
                        ssUserProfilePhotoUrl, R.drawable.logo_small);
            }
            // Render the user's display name in the first text field that is located immediately
            // below the image view in the top of the menu.
            mSideMenuHeaderTitleText.setText(ssUser.getUserDisplayName());
            // There is really no other interesting information about the user to display
            // (hypothetically speaking, the logic herein could display the user's email address
            // if the user hasn't elected via their social media account to block third party
            // apps from accessing the user's email). However, let's merely display the app's
            // name in the second text view that is located below the image view in the side menu.
            mSideMenuHeaderSubtitleText.setText(R.string.nav_header_title);
        }
    }


    /**
     * Subclasses must provide the implementation for this method and return the layout resource
     * that represents the particular activity's inner-most content. (This base class handles the
     * "shell" that surrounds the content, which includes the toolbar and sidemenu.)
     *
     * @return the int that corresponds to the subclass's content layout.
     */
    protected abstract @LayoutRes int getContentLayoutId();

    protected void createSecondaryContent() {
        // Inflates the activity's inner content into the reserved located within the view hierarchy
        ViewGroup secondaryContentContainer = findViewById(R.id.content_for_activity_with_side_menu);
        getLayoutInflater().inflate(getContentLayoutId(), secondaryContentContainer);
    }

    /**
     * Subclasses may override this method and return a string if any subclasses wish to display
     * text below the icon that is drawn in the center of the toolbar.
     *
     * @return the string text to be displayed below the icon that is drawn in the center
     * of the toolbar
     */
    protected String getSubtitle() {
        return null;
    }

    protected void configureCustomToolbar() {
        String subtitle = getSubtitle();
        if (subtitle == null) {
            if (showMenuInsteadOfBack()) {
                ToolbarHelper.addStandardToolbarWithMenu(this);
            } else {
                ToolbarHelper.addStandardToolbarWithBack(this);
            }
        } else {
            if (showMenuInsteadOfBack()) {
                ToolbarHelper.addStandardToolbarWithMenu(this, subtitle);
            } else {
                ToolbarHelper.addStandardToolbarWithBack(this, subtitle);
            }
        }
    }

    /**
     * Indicates whether to show a menu icon versus a back button icon in the upper left hand
     * side of the custom toolbar.
     *
     * @return true to show a menu icon instead of a back button icon in the upper left hand side
     * of the custom toolbar; or false to show a back button in the upper left hand side of the
     * custom toolbar.
     */
    protected boolean showMenuInsteadOfBack() {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            ToolbarHelper.navigateBack(this);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            // If the side menu is currently displayed, simply hide the side menu when the back
            // button is pressed.
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // Otherwise navigate back
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // This method is called when a menu item is tapped in the side menu.
        int id = item.getItemId();

        if (id == R.id.side_menu_order_history) {
            // ORDER HISTORY
            startActivity(new Intent(this, OrderHistoryActivity.class));
        } else if (id == R.id.side_menu_exit_store) {
            // EXIT STORE
            new AlertDialog.Builder(this)
                    .setTitle(R.string.exit_store_confirmation_title)
                    .setMessage(R.string.exit_store_confirmation_message)
                    .setPositiveButton(R.string.exit_store_yes, (dialog, which) -> {
                        SwiftShopperSdk.exitStore(this::onExitStore);
                    }).setNegativeButton(R.string.exit_store_no, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else if (id == R.id.side_menu_login) {
            // LOG IN

            // Create an SsNativeLoginView to display within an Alert Dialog.
            SsNativeLoginView ssLoginView = new SsNativeLoginView(this);
            ssLoginView.setAllowSkipLogin(false);

            // Create an alert dialog that presents the SsNativeLoginView
            AlertDialog alertDialog =
                    new AlertDialog.Builder(this).setView(ssLoginView).create();

            // Sets the login delegate listener upon the SsNativeLoginView. The login delegate
            // receives callbacks when the user successfully logs in and upon login failure.
            ssLoginView.setLoginDelegate(new SsLoginView.Delegate() {
                @Override
                public void onLoginSuccess() {
                    // LOG IN: Success
                    //
                    // Hide the "Log In" menu option in the side menu
                    // Show the "Log Out" menu option in the side menu.
                    // Render the user's profile photo and display name at the top of the side menu
                    NavigationView sideMenuNavigationView = findViewById(R.id.nav_view);
                    if (sideMenuNavigationView != null) { // Sanity Check
                        Menu sideMenu = sideMenuNavigationView.getMenu();
                        if (sideMenu != null) { // Sanity Check
                            // User is now logged in, show the "Log Out" menu option and hide the
                            // "Log In" menu option
                            sideMenu.findItem(R.id.side_menu_logout).setVisible(true);
                            sideMenu.findItem(R.id.side_menu_login).setVisible(false);
                        }
                        showUserProfileDataInMenu();
                    }
                    alertDialog.dismiss();
                }

                @Override
                public void onLoginFailure(Throwable throwable) {
                    Toast.makeText(AppSecondaryActivity.this,
                            R.string.toast_message_login_failed, Toast.LENGTH_LONG).show();
                    alertDialog.dismiss();
                }

                @Override
                public void proceedAnonymously(Runnable runnable) {
                    // This method would be called if the user was allowed to skip login and
                    // elected to skip login, however, as configured herein, the SsNativeLoginView
                    // will not render the option for the user to skip login.
                    alertDialog.dismiss();
                }
            });

            alertDialog.show();

            // Set the background to a semi-white, rounded rectangle. Note that due to Android
            // nuisances, this has to be done after the alert dialog is presented.
            alertDialog.getWindow().
                    setBackgroundDrawableResource(R.drawable.dialog_rounded_rectangle_bg);

        } else if (id == R.id.side_menu_logout) {
            // LOG OUT
            new AlertDialog.Builder(this)
                    .setTitle(R.string.logout_confirmation_title)
                    .setMessage(
                            SwiftShopperSdk.getStore()==null ?
                                    R.string.logout_confirmation_message_not_in_store :
                                    R.string.logout_confirmation_message_in_store)
                    .setPositiveButton(R.string.logout_confirmation_yes, (dialog, which) -> {
                        SwiftShopperSdk.logout(() -> {
                            // Redirect to the LoginActivity after the Swift Shopper SDK completely logs
                            // out the user, and close this activity.
                            startActivity(new Intent(this, LoginActivity.class).setFlags(
                                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        });
                    })
                    .setNegativeButton(R.string.login_confirmation_canel, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        // Closes the side-menu after processing the user's selection from the menu
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    protected void onExitStore() {
        ActivityOptionsCompat selectMarketActivityOptions =
                ActivityOptionsCompat.makeCustomAnimation(
                        this, R.anim.anim_slide_up, R.anim.anim_slide_down);
        startActivity(new Intent(this, SelectMarketActivity.class).setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        finish();
    }
}
