package com.swiftshopper.sdk.quickstart4;

import android.content.Intent;

import com.swiftshopper.sdk.SsStoreDetail;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.ui.view.SsCartButtonView;
import com.swiftshopper.sdk.ui.view.custom.CustomToolbar;

public class InStoreShoppingActivity extends AppSecondaryActivity {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_in_store_shopping;
    }

    @Override protected void configureCustomToolbar() {
        super.configureCustomToolbar();

        // Add the cart widget to the upper right hand corner in the custom toolbar
        SsCartButtonView ssCartButtonView = new SsCartButtonView(this);
        ssCartButtonView.setOnClickListener((v)->{
            onCartButtonClick();
        });

        CustomToolbar customToolbar = findViewById(R.id.custom_toolbar);
        customToolbar.setRightView(ssCartButtonView);
    }

    protected String getMyTitle() {
        return getString(R.string.title_activity_in_store_shopping);
    }

    @Override
    protected String getSubtitle() {
        String retVal;
        SsStoreDetail ssStoreDetail = SwiftShopperSdk.getStore();
        String retailerName = ssStoreDetail == null ? null : ssStoreDetail.getRetailerName();
        if (retailerName == null) {
            // Sanity check -- this should never be the case since by design, the
            // InStoreShoppingActivity is only displayed if the user is shopping at a store AND
            // the swift shopper infrastructure should not allow a retailer's name to be null.
            // Nevertheless, this is "software" and "software" can hypothetically behave in a
            // manner that is contrary to the software's design. Hence, in case a "bug" ever
            // occurs, let's at least avoid making the subtitle look ugly to the user by using
            // the activity's "title" instead, which does not expect any formatter argument for
            // the retailer name
            retVal = getString(R.string.title_activity_in_store_shopping);
        } else {
            retVal = getString(R.string.subtitle_activity_in_store_shopping, retailerName);
        }
        return retVal;
    }

    private void onCartButtonClick() {
        startActivity(new Intent(this, CartActivity.class));
    }
}
