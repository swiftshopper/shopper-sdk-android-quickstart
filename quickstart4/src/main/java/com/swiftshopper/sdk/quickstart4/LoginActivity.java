package com.swiftshopper.sdk.quickstart4;

import android.content.Intent;
import androidx.core.app.ActivityOptionsCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;
import com.swiftshopper.sdk.ui.view.SsLoginView;

/**
 * This is the LoginActivity that appears after the MainActivity and also appears again later if
 * the user logs out.
 */
public class LoginActivity extends AppSecondaryActivity implements SsLoginView.Delegate {

    @Override
    protected int getContentLayoutId() {
        return R.layout.content_login;
    }

    @Override
    protected void createSecondaryContent() {
        super.createSecondaryContent();

        // Listen for successful, failed, and anonymous login (user opting to skip login)
        SsLoginView ssLoginView = findViewById(R.id.loginView);
        ssLoginView.setLoginDelegate(this);

        // Hide the 'Skip Login' option if the user has previously logged in anonymously (in other
        // words if the user has already explicitly chosen to 'Skip Login')
        SsUser ssUser = SwiftShopperSdk.getUser();
        if (ssUser != null && ssUser.isAnonymous()) {
            ssLoginView.setAllowSkipLogin(false);
        }
        // Hides the custom toolbar: the LoginActivity is a fullscreen activity without a toolbar.
        findViewById(R.id.custom_toolbar).setVisibility(View.GONE);
        // Disables the side menu
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    protected void configureSideMenu() {
        // Overrides the base class implementation and does nothing, on purpose, to prevent a side
        // menu from being able to appear on this activity.
    }

    // BEGIN: SsLoginView.Delegate Implementation
    @Override
    public void onLoginSuccess() {
        dismissLoading();

        if (SwiftShopperSdk.getStore() == null) {
            // The user is not checked into a store, proceed next to ask the user to select a store
            ActivityOptionsCompat selectMarketActivityOptions =
                    ActivityOptionsCompat.makeCustomAnimation(
                            this, R.anim.anim_slide_up, android.R.anim.fade_out);
            startActivity(new Intent(this, SelectMarketActivity.class).setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK),
                    selectMarketActivityOptions.toBundle());

        } else {
            // The user is checked into a store, proceed next to the InStoreShoppingActivity to
            // allow the user to go shopping at the store.
            startActivity(new Intent(this, InStoreShoppingActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));

        }
    }

    @Override
    public void onLoginFailure(Throwable throwable) {
        if (throwable != null) {
            Toast.makeText(this, getString(R.string.toast_message_login_failed), Toast.LENGTH_SHORT).show();
        }
        dismissLoading();
    }

    @Override
    public void proceedAnonymously(Runnable runMeToProceed) {
        // Try to gently convince the user not to skip login
        //
        // Create an alert dialog that explains what features will be missing if the user skips
        // login and asks the user if they are sure.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getTitle());
        builder.setMessage(R.string.login_skip_confirmation_message);
        // Reverse psychology: "Going Back" or "Cancelling" is illustrated as the positive choice
        builder.setPositiveButton(R.string.login_skip_confirmation_cancel, (d, i)->{

        });
        builder.setNegativeButton(R.string.login_skip_confirmation_yes, (d,i)->{
            // Proceed to skip the login since the user insists
            showLoading();
            runMeToProceed.run();
        });
        // Show the alert dialog
        builder.create().show();
    }
    // END: SsLoginView.Delegate Implementation
}
