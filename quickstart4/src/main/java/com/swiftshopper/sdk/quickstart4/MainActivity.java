package com.swiftshopper.sdk.quickstart4;

import android.content.Intent;
import android.os.Handler;
import androidx.core.app.ActivityOptionsCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.swiftshopper.sdk.SsUser;
import com.swiftshopper.sdk.SwiftShopperSdk;

/**
 * The MainActivity serves as a splash screen which then launches the appropriate next screen
 * based on whether the user has logged in and based on whether the user is in the middle of
 * shopping at a store.
 */
public class MainActivity extends AppCompatActivity {

    // This handler is used to execute the afterSplash() method one second after this
    // activity appears
    private Handler mHandler;

    /**
     * This method executes one second after this activity appears.
     */
    private void afterSplash() {
        SsUser ssUser = SwiftShopperSdk.getUser();
        if (ssUser == null) {
            // User is not logged in, and the user has not yet elected even to skip login.
            // Show the LoginActivity!
            Intent startLoginActivity = new Intent(this, LoginActivity.class);
            // Smoothly animate the splash logo moving from its size & location on this activity to
            // the same graphical element's location on the login activity.
            ActivityOptionsCompat startLoginActivityOptions =
                    ActivityOptionsCompat.makeSceneTransitionAnimation(
                            this, findViewById(R.id.splash_slogan), "slogan");
            startActivity(startLoginActivity, startLoginActivityOptions.toBundle());

            // Close this splash screen after the LoginActivity appears. Note that this logic does
            // not add FLAG_ACTIVITY_NEW_TASK | FLAG_ACTIVITY_CLEAR_TASK. Reason = we have a
            // special animation that will make the store's logo appear to morph from this screen
            // onto the LoginActivity screen. Simply calling finish() without a delay would cause
            // the user to momentarily see their home screen or previous app in the middle of the
            // transition. Simply declaring those flags would defeat the animation. The solution
            // is to have this activity finish after a brief amount of time. Note that from prior
            // experience, it can take Android up to 1/2 of a second to present an Activity or
            // longer if Android decides to invoke a garbage collection cycle. A value 1000 seems
            // long enough to generally cause this activity to be closed just after the next
            // activity appears. (We need to either call finish() or use those flags, otherwise the
            // user would be able to navigate back to this activity via the Android back button).
            mHandler.postDelayed(this::finish, 1000);
        } else if (SwiftShopperSdk.getStore() == null) {
            // User is either logged in, or has elected to skip login (aka: user is logged in
            // anonymously). User is not in the middle of shopping at any particular store.
            // Show the SelectMarketActivity!
            ActivityOptionsCompat selectMarketActivityOptions =
                    ActivityOptionsCompat.makeCustomAnimation(
                            this, R.anim.anim_slide_up, android.R.anim.fade_out);
            startActivity(new Intent(this, SelectMarketActivity.class).setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK),
                    selectMarketActivityOptions.toBundle());
        } else {
            // User is either logged in, or has elected to skip login (aka: user is logged in
            // anonymously). User is in the middle of shopping at any particular store. (Maybe
            // the user closed the app and relaunched the app -- that might explain how
            // circumstances could have caused the logic to take this path through the code.
            // Show the InStoreShoppingActivity so that the user can continue shopping.
            ActivityOptionsCompat shoppingActivityOptions =
                    ActivityOptionsCompat.makeCustomAnimation(
                            this, android.R.anim.fade_in, android.R.anim.fade_out);
            startActivity(new Intent(this, InStoreShoppingActivity.class).setFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK),
                    shoppingActivityOptions.toBundle());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View contentView = getLayoutInflater().inflate(R.layout.activity_main, null);
        setContentView(contentView);
        mHandler = new Handler();
        mHandler.postDelayed(this::afterSplash, 1000);

    }
}

