**Android Quickstart Apps Using the Swift Shopper Native SDK**

## Clone and open in Android Studio
1. Clone this repository onto your development computer's file system
2. Open the resulting directory as a project in Android Studio

---

## View and Run the Sample Apps

1. You should see 4 apps within the project in Android Studio (quickstart, quickstart2, quickstart3, and quickstart4)
2. Feel free to browse the source and borrow from the source as you implement your own app that uses the Swift Shopper Native SDK for Android.
3. Choose one of these apps from the "run configurations" drop down in Android studio and press the green triangle button (the run button).

### NEW:
* The quickstart4 app is a near-release quality application that performs a basic self-scan / scan-n-go checkout for a retail store chain. All a potential retailer would need to do is replace the demo SDK key with their own Swift Shopper SDK key and replace the colors, logo png files, and font files with their own that matches their branding. The result would be a brand new self-scan shopping app ready for release and deployment to the Google Play store.

---

## Any Problems?

1. See the Bitbucket Wiki for this repository. We intend to keep it updated with instructions.
2. See the Bitbucket Issues tracker for this repository. Feel free to post an issue.
3. Feel free to create and push a branch with any suggestions or fixes that you may have. Create a pull request and we'll get your changes incorporated back into the master branch.
3. See the [Swift Shopper Developers Website](https://developer.swiftshopper.com)
